package controllers

import (
	"bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/model/entity"
	"bitbucket.org/keekee-proj/service-observer/internal/model/event_queue"
	"bitbucket.org/keekee-proj/service-observer/internal/modules"
	"bitbucket.org/keekee-proj/service-observer/internal/modules/pump"
	"bitbucket.org/keekee-proj/service-observer/internal/queue"
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"
)

type asyncCh struct {
	sync.Mutex
	queue.Channel
}

var amqpCh asyncCh

func amqpChannel(qp queue.Provider) (*asyncCh, error) {
	if amqpCh.Channel != nil {
		return &amqpCh, nil
	}

	amqpCh.Lock()
	defer amqpCh.Unlock()

	if amqpCh.Channel != nil {
		return &amqpCh, nil
	}
	ch, err := qp.MakeChannel()
	if err != nil {
		return nil, err
	}

	amqpCh.Channel = ch
	return &amqpCh, nil
}

func AddEvent(ctx context.Context, response http.ResponseWriter, request *http.Request) error {
	ap := app.MustFromContext(ctx)
	e, err := ap.EntityRepository().OneCached(ctx.Value("entity").(string))
	if err != nil {
		return err
	}
	event := ctx.Value("event").(string)
	if !e.HasEvent(event) {
		return fmt.Errorf("event [%s] not in [%v]", event, e.Events)
	}

	data, err := ioutil.ReadAll(request.Body)
	if err != nil {
		return err
	}
	request.Body.Close()

	if len(data) == 0 {
		return fmt.Errorf("request empty")
	}

	switch ap.Config().App.CreatingEventsType {
	case "async":
		err = asyncMakeEvents(ap, e, event, data)
	default:
		err = syncMakeEvents(ap, e, event, data)
	}

	if err != nil {
		return err
	}

	response.Write(success)
	return nil
}

func syncMakeEvents(ap *app.App, entity *entity.Entity, event string, data []byte) error {
	return event_queue.MakeEvents(event, entity, ap.SubjectRepository(), ap.EventQueueRepository(), data)
}

func asyncMakeEvents(ap *app.App, entity *entity.Entity, event string, data []byte) error {
	ch, err := amqpChannel(ap.QueueProvider())
	if err != nil {
		return err
	}

	mi, err := modules.ModuleByName(pump.Name)
	if err != nil {
		return err
	}

	module := mi.(*pump.Module)
	return module.AddToQueue(ch, ap.Config().Amqp, entity, event, data)
}
