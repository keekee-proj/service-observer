###Plugins
Call `GET /modules` to get information about available modules 
```json
[
  {
    "name": "templater",
    "description": "make result using GO templates",
    "scheme": {
      "templateRaw": {
        "type": "string",
        "required": true,
        "default": null,
        "Description": "template body. For example:\nId: \"{{ .id }}\"\nParamX: {{ .params.X }}"
      }
    }
  },
  {
    "name": "sender_http",
    "description": "Send response by http",
    "scheme": {
      "headers": {
        "type": "map",
        "required": false,
        "default": null,
        "Description": "request headers (key: value)"
      },
      "method": {
        "type": "string",
        "required": false,
        "default": "POST",
        "Description": "request method"
      },
      "url": {
        "type": "string",
        "required": true,
        "default": null,
        "Description": "url of resource"
      }
    }
  }
]
```

###Installing new plugins
In config-file you have part for configuring path for plugins:
```yaml
plugins:
  path: ./plugins
```

Save the `.so` file in this path and restart application (all parts).
To verify the installation of the module, you can Call `GET /modules` via http.

For starting your module, you should execute:
```bash
./pipeline --config=<config.path> --module=<module.name>
``` 

For example `./docker-compose.yaml`:
```yaml
pipeline-sender:
  image: service-observer
  command: ./pipeline --config=/app/etc/compose.yaml --module=sender_http
  volumes:
  - etc:/app/etc
  links:
  - mongodb
  - rabbit
  depends_on:
  - mongodb
  - rabbit
  restart: on-failure

```

###Making new module
See [example](https://bitbucket.org/keekee-proj/service-observer-plugin-sender/src/master/).

The first, you should implements `PipeExtension` (`bitbucket.org/keekee-proj/service-observer/pkg/plugin`) in your code.
```go
type PipeExtension interface {
	Exec(entityType string, entityId string, event string, subject string,
		data []byte, params map[string]interface{}) ([]byte, error)

	Name() string
	Description() string
	Scheme() map[string]*SchemeAttribute

	PluginVersion() int
	MinAppVersion() int
	MaxAppVersion() int
	MinPluginApiVersion() int
	MaxPluginApiVersion() int
}

```
And build your module with plugin mode:
```bash
go build -buildmode=plugin -o ./plugin/myNewPlugin.so main.go
``` 

