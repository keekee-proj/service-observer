##API
###Entity create
```text
POST /entity
---------- request body ----------
{
  "type": "test_entity",
  "events": [
    "add", "update"
  ]
}
```
###Entity update
```text
PUT /entity/test_entity
---------- request body ----------
{
  "events": [
      "add", "update", "delete"
  ]
}
```
###Entity delete
```text
DELETE /entity/test_entity
```
###Get entity
```text
GET /entity/test_entity # for get one item
GET /entities           # for get all item
```

###Stage create
```text
POST /stage
---------- request body ----------
{
  "name": "stage_tpl_test",
  "module": "templater",
  "attributes": {
    "templateRaw": "ID: \"{{ .id }}\", PARAM1: {{ .param1 }}"
  }
}
```
or
```text
POST /stage
---------- request body ----------
{
  "name": "send_http_test",
  "module": "sender_http",
  "attributes": {
    "url": "http://localhost:8199",
    "method": "POST",
    "headers": {
      "Authorization": "Bearer **********"
    }
  }
}
```

###Stage delete
```text
DELETE /stage/stage_tpl_test
```
###Get stages
```text
GET /stage/stage_tpl_test       # for get one item
GET /stages                     # for get all item
GET /stages?module=templater    # get items by filter
```

###Pipeline create
```text
POST /pipe
---------- request body ----------
{
  "name": "pipe_test",
  "stages": [
    "stage_tpl_test",
    "send_http_test"
  ]
}
```
###Pipeline delete
```text
DELETE /pipe/pipe_test
```
###Get pipelines
```text
GET /pipe/pipe_test            # for get one item
GET /pipes                     # for get all item
GET /pipes?name=pipe_test      # get items by filter
```

###Subject create
```text
POST /subject
---------- request body ----------
{
  "name": "subj_0",
  "entity": "test_entity",
  "events": [
    "add"
  ],
  "filter": {
    "prop": {"@in": [1, 2, 3]}
  },
  "pipeline": "pipe_test"
}
```
###Subject update
```text
PUT /subject/subj_0
---------- request body ----------
{
  "entity": "test_entity",
  "events": [
    "add"
  ],
  "pipeline": "pipe_test"
}
```
###Subject delete
```text
DELETE /subject/subj_0
```
###Get subject
```text
GET /subject/test_entity                            # for get one item
GET /subjects                                       # for get all item
GET /subjects?entity=test_entity&events=add,create  # get items by filter
```
###Event create
```text
POST /entity/<entity>/event/<event>
---------- request body ----------
{
  "id": "some_id",
  "param1": "some value"
  //Other data
}
```
###Get modules
```text
GET /modules
```