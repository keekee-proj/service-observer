package command_bus

import (
	"fmt"
	"log"
	"net"
	"sync"
)

type busHandle interface {
	script(b *Bus, ch <-chan *Command)
	closeBus(b *Bus)
}
type CloseHandler func(err error)

type Bus struct {
	*sync.RWMutex
	buf         []byte
	cn          *net.TCPConn
	closeHandls []CloseHandler
	closed      bool
}

func NewBus(cn *net.TCPConn, bufSize int) *Bus {
	buf := make([]byte, bufSize)
	closeHandls := make([]CloseHandler, 0)

	return &Bus{new(sync.RWMutex), buf, cn, closeHandls, false}
}

func (b *Bus) String() string {
	return b.cn.LocalAddr().String()
}
func (b *Bus) Alive() bool {
	return !b.closed
}
func (b *Bus) Call(cmd *Command) error {
	b.RLock()
	defer b.RUnlock()
	if b.closed {
		return fmt.Errorf("bus is closed")
	}

	_, err := b.cn.Write(makeMessage(cmd))
	return err
}

func (b *Bus) NotifyOnClose(fn CloseHandler) {
	b.closeHandls = append(b.closeHandls, fn)
}

func (b *Bus) Close(err error) {
	b.closeWithErr(err)
}
func (b *Bus) closeWithErr(err error) {
	b.Lock()
	defer b.Unlock()
	if b.closed {
		return
	}

	b.cn.Close()
	for _, fn := range b.closeHandls {
		fn(err)
	}
	b.closed = true
	log.Printf("bus closeWithErr: [%s]\n", err)
}

func (b *Bus) ReceiveNext(cmd *Command) bool {
	n, err := b.cn.Read(b.buf)

	if err != nil {
		b.closeWithErr(err)
		return false
	}
	res, err := readMessage(b.buf[:n])
	if err != nil {
		*cmd = Command{}
		return true
	}
	*cmd = *res

	return true
}

func listenFunc(bh busHandle, b *Bus) {
	ch := make(chan *Command)
	go func(bus *Bus, cmdCh chan<- *Command) {
		cmd := &Command{}
		for bus.ReceiveNext(cmd) {
			if cmd.Name == "" {
				continue
			}
			ch <- cmd
		}
		close(ch)
	}(b, ch)

	bh.script(b, ch)
	log.Println("listenFunc finish")
	bh.closeBus(b)
}
