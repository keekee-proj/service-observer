package subject

import (
	"bitbucket.org/keekee-proj/service-observer/internal/model/pipeline"
	"bitbucket.org/keekee-proj/service-observer/internal/storage"
	"bitbucket.org/keekee-proj/service-observer/internal/utils"
	"fmt"
	"gopkg.in/mgo.v2/bson"
)

type Repository struct {
	storage    storage.Storage
	collection string
	cache      *utils.Cache
}

func queryEntityEvent(entity string, event string) bson.M {
	return bson.M{
		"entity": entity,
		"events": event,
	}
}

func NewRepository(s storage.Storage, collection string, cache *utils.Cache) *Repository {
	return &Repository{s, collection, cache}
}

func (r *Repository) One(t string) (*Subject, error) {
	e := &Subject{}
	if err := r.storage.OneId(r.collection, t, e); err != nil {
		return nil, err
	}

	return e, nil
}

func (r *Repository) OneCached(t string) (subj *Subject, err error) {
	subj, _ = r.cache.Get(t, func() interface{} {
		res, er := r.One(t)
		if er != nil {
			err = er
			return err
		}
		return res
	}).(*Subject)
	return
}

func (r *Repository) All() ([]*Subject, error) {
	var res []*Subject
	if err := r.storage.All(r.collection, &res); err != nil {
		return nil, err
	}

	return res, nil
}

func (r *Repository) Count() (int, error) {
	return r.storage.Count(r.collection, nil)
}

func (r *Repository) Find(q interface{}, limit int, offset int) ([]*Subject, error) {
	var res []*Subject
	if err := r.storage.Find(r.collection, q, limit, offset, &res); err != nil {
		return nil, err
	}

	return res, nil
}

func (r *Repository) Update(t string, e *Subject) error {
	if err := r.storage.UpdateId(r.collection, t, e); err != nil {
		return err
	}

	return nil
}

func (r *Repository) Delete(t string) error {
	if err := r.storage.RemoveId(r.collection, t); err != nil {
		return err
	}

	return nil
}

func (r *Repository) Add(e *Subject) error {
	if err := r.storage.Create(r.collection, e); err != nil {
		return err
	}

	return nil
}

func (r *Repository) ValidateEdit(s *Subject, pR *pipeline.PipeRepository) error {
	_, err := pR.OneCached(s.Pipeline)
	if err != nil {
		return fmt.Errorf("incorrect piplene: %s", err)
	}

	return nil
}

func (r *Repository) ValidateAdd(s *Subject, pR *pipeline.PipeRepository) error {
	if s.Name == "" {
		return fmt.Errorf("name is empty")
	}

	if _, err := r.One(s.Name); err == nil {
		return fmt.Errorf("alredy exists")
	}

	return r.ValidateEdit(s, pR)
}

func (r *Repository) IterByEntityEvent(entity string, event string) storage.Iterator {
	return r.storage.Iter(r.collection, queryEntityEvent(entity, event))
}
func (r *Repository) IterAll() storage.Iterator {
	return r.storage.Iter(r.collection, nil)
}
