package program

import (
	"bitbucket.org/keekee-proj/service-observer/internal/config"
	"bitbucket.org/keekee-proj/service-observer/internal/model/event_queue"
	"bitbucket.org/keekee-proj/service-observer/internal/model/pipeline"
	amqpMock "bitbucket.org/keekee-proj/service-observer/tests/mock/amqp"
	appMock "bitbucket.org/keekee-proj/service-observer/tests/mock/app"
	"bitbucket.org/keekee-proj/service-observer/tests/mock/plugin"
	"bitbucket.org/keekee-proj/service-observer/tests/mock/queue"
	"bitbucket.org/keekee-proj/service-observer/tests/mock/storage"
	"context"
	"errors"
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/streadway/amqp"
	"gopkg.in/mgo.v2/bson"
	"sync"
	"testing"
	"time"
)

func TestMainPipeline(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	cfg := &config.Config{Amqp: &config.Amqp{RouteKey: "rk", QueueName: "qn", Exchange: "ex"}}
	storageM := storage.NewMockStorage(ctrl)
	qProvM := queue.NewMockProvider(ctrl)
	pluginM := plugin.NewMockPipeExtension(ctrl)

	app := appMock.NewMockApp(cfg, storageM, qProvM)
	plugins := app.PipePlugins()
	testModule := "testModule"
	nextStageModule := "x2Module"
	plugins[testModule] = pluginM

	ctx, cnlFn := context.WithCancel(context.Background())
	ctx = NewPipelineContext(ctx, testModule)

	acknowledger := amqpMock.NewMockAcknowledger(ctrl)
	// ------------------------------------------------------------

	// ToDo config channel
	chMsg := make(chan amqp.Delivery, 10)
	channel := queue.NewMockChannel(ctrl)
	wgPublishTo := sync.Mutex{}
	counterPublishTo := 0
	channel.EXPECT().
		PublishTo(gomock.Any(), gomock.Eq("ex"), gomock.Eq("rk_"+nextStageModule), gomock.Eq(int16(0))).
		AnyTimes().
		DoAndReturn(func(body []byte, exchange, routeKey string, attempt int16) error {
			wgPublishTo.Lock()
			counterPublishTo++
			wgPublishTo.Unlock()
			return nil
		})

	q := queue.NewMockQueue(ctrl)
	q.EXPECT().
		Consume(gomock.Any()).
		AnyTimes().
		Return(chMsg)

	q.EXPECT().
		Close().
		Times(1).
		Return(nil)

	qProvM.EXPECT().
		MakeChannel().
		Times(1).
		Return(channel, nil)

	qProvM.EXPECT().
		MakeQueue(gomock.Eq("ex"), gomock.Eq("rk_"+testModule), gomock.Eq("qn_"+testModule), gomock.Any()).
		Times(1).
		Return(q, nil)

	storageM.EXPECT().
		OneId(gomock.Eq("event_queue"), gomock.Any(), gomock.Any()).
		AnyTimes().
		DoAndReturn(func(_ string, id interface{}, result interface{}) error {
			res := result.(*event_queue.EventQueue)
			*res = event_queue.EventQueue{
				Id:         id.(bson.ObjectId),
				EntityType: "yyy",
			}
			return nil
		})
	storageM.EXPECT().
		OneId(gomock.Eq("stage"), gomock.Eq("x2"), gomock.Any()).
		AnyTimes().
		DoAndReturn(func(_ string, id interface{}, result interface{}) error {
			res := result.(*pipeline.Stage)
			*res = pipeline.Stage{
				Name:   id.(string),
				Module: "x2Module",
			}
			return nil
		})
	storageM.EXPECT().
		Find(gomock.Eq("stage"), gomock.Any(), gomock.Eq(1), gomock.Eq(0), gomock.Any()).
		AnyTimes().
		DoAndReturn(func(table string, query interface{}, limit int, offset int, result interface{}) error {
			res := result.(*[]*pipeline.Stage)
			*res = []*pipeline.Stage{
				{
					Name:       "x1",
					Module:     testModule,
					Attributes: map[string]interface{}{}},
			}
			return nil
		})

	pluginM.EXPECT().Name().AnyTimes().Return(testModule)
	pluginM.EXPECT().
		Exec(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
		AnyTimes().
		DoAndReturn(func(entityType string, entityId string, event string, subject string,
			data []byte, params map[string]interface{}) ([]byte, error) {
			return data, nil
		})

	wgAck := sync.Mutex{}
	counterAck := 0
	acknowledger.EXPECT().
		Ack(gomock.Any(), gomock.Any()).
		AnyTimes().
		Do(func(_ uint64, _ bool) {
			wgAck.Lock()
			counterAck++
			wgAck.Unlock()
		}).
		Return(nil)

	done := make(chan struct{})
	go func(ch chan<- struct{}) {
		app.Run(Pipeline, ctx)
		ch <- struct{}{}
	}(done)

	msgCtn := 88
	for i := 0; i < msgCtn; i++ {
		json := fmt.Sprintf(`{"event_queue_id":"507f1f77bcf86cd7994390%02d", "entity_id": "%d", "subject":"xxx","event": "add","data":"","pipeline":{"name":"abc", "stages":["x1", "x2"]}}`, i, i)
		chMsg <- amqp.Delivery{Body: []byte(json), Acknowledger: acknowledger}
	}

	moment := time.Now()
	for counterAck < msgCtn {
		if time.Since(moment) > time.Second {
			break
		}
		time.Sleep(time.Microsecond)
	}
	cnlFn()
	<-done

	if counterAck != msgCtn {
		t.Fatalf("ack %d msg, expect %d", counterAck, msgCtn)
	}
	if counterPublishTo != msgCtn {
		t.Fatalf("recieve %d msg, expect %d", counterPublishTo, msgCtn)
	}
}

func TestProcessErrorPipeline(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	cfg := &config.Config{Amqp: &config.Amqp{RouteKey: "rk", QueueName: "qn", Exchange: "ex"}}
	storageM := storage.NewMockStorage(ctrl)
	qProvM := queue.NewMockProvider(ctrl)
	pluginM := plugin.NewMockPipeExtension(ctrl)

	app := appMock.NewMockApp(cfg, storageM, qProvM)
	plugins := app.PipePlugins()
	testModule := "testModule"
	plugins[testModule] = pluginM

	ctx, cnlFn := context.WithCancel(context.Background())
	ctx = NewPipelineContext(ctx, testModule)

	acknowledger := amqpMock.NewMockAcknowledger(ctrl)
	// ------------------------------------------------------------

	// ToDo config channel
	chMsg := make(chan amqp.Delivery, 10)
	channel := queue.NewMockChannel(ctrl)

	q := queue.NewMockQueue(ctrl)
	q.EXPECT().
		Consume(gomock.Any()).
		AnyTimes().
		Return(chMsg)

	q.EXPECT().
		Close().
		Times(1).
		Return(nil)

	wgDefer := sync.Mutex{}
	counterDefer := 0
	q.EXPECT().
		Defer(gomock.Any()).
		AnyTimes().
		Do(func(d *amqp.Delivery) {
			wgDefer.Lock()
			counterDefer++
			wgDefer.Unlock()
		}).
		Return(nil)

	qProvM.EXPECT().
		MakeChannel().
		Times(1).
		Return(channel, nil)

	qProvM.EXPECT().
		MakeQueue(gomock.Eq("ex"), gomock.Eq("rk_"+testModule), gomock.Eq("qn_"+testModule), gomock.Any()).
		Times(1).
		Return(q, nil)

	storageM.EXPECT().
		OneId(gomock.Eq("event_queue"), gomock.Any(), gomock.Any()).
		AnyTimes().
		DoAndReturn(func(_ string, id interface{}, result interface{}) error {
			res := result.(*event_queue.EventQueue)
			*res = event_queue.EventQueue{
				Id:         id.(bson.ObjectId),
				EntityType: "yyy",
			}
			return nil
		})
	storageM.EXPECT().
		OneId(gomock.Eq("stage"), gomock.Eq("x2"), gomock.Any()).
		AnyTimes().
		DoAndReturn(func(_ string, id interface{}, result interface{}) error {
			res := result.(*pipeline.Stage)
			*res = pipeline.Stage{
				Name:   id.(string),
				Module: "x2Module",
			}
			return nil
		})
	storageM.EXPECT().
		Find(gomock.Eq("stage"), gomock.Any(), gomock.Eq(1), gomock.Eq(0), gomock.Any()).
		AnyTimes().
		DoAndReturn(func(table string, query interface{}, limit int, offset int, result interface{}) error {
			res := result.(*[]*pipeline.Stage)
			*res = []*pipeline.Stage{
				{
					Name:       "x1",
					Module:     testModule,
					Attributes: map[string]interface{}{}},
			}
			return nil
		})

	pluginM.EXPECT().Name().AnyTimes().Return(testModule)
	pluginM.EXPECT().
		Exec(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
		AnyTimes().
		DoAndReturn(func(entityType string, entityId string, event string, subject string,
			data []byte, params map[string]interface{}) ([]byte, error) {
			return data, errors.New("test error")
		})

	wgAck := sync.Mutex{}
	counterAck := 0
	acknowledger.EXPECT().
		Ack(gomock.Any(), gomock.Any()).
		AnyTimes().
		Do(func(_ uint64, _ bool) {
			wgAck.Lock()
			counterAck++
			wgAck.Unlock()
		}).
		Return(nil)

	done := make(chan struct{})
	go func(ch chan<- struct{}) {
		app.Run(Pipeline, ctx)
		ch <- struct{}{}
	}(done)

	msgCtn := 88
	for i := 0; i < msgCtn; i++ {
		json := fmt.Sprintf(`{"event_queue_id":"507f1f77bcf86cd7994390%02d", "entity_id": "%d", "subject":"xxx","event": "add","data":"","pipeline":{"name":"abc", "stages":["x1", "x2"]}}`, i, i)
		chMsg <- amqp.Delivery{Body: []byte(json), Acknowledger: acknowledger}
	}

	moment := time.Now()
	for counterAck < msgCtn {
		if time.Since(moment) > time.Second {
			break
		}
		time.Sleep(time.Microsecond)
	}
	cnlFn()
	<-done

	if counterAck != msgCtn {
		t.Fatalf("ack %d msg, expect %d", counterAck, msgCtn)
	}
	if counterDefer != msgCtn {
		t.Fatalf("defer %d msg, expect %d", counterDefer, msgCtn)
	}
}
