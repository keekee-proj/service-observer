package app

import (
	"bitbucket.org/keekee-proj/service-observer/internal/config"
	"github.com/pkg/profile"
	"os"
	"path/filepath"
)

func StartProfiling(cfg *config.Config) (interface{ Stop() }, error) {
	mode := profile.MemProfile
	switch cfg.Pprof.Mode {
	case "cpu":
		mode = profile.CPUProfile
	case "mx":
		mode = profile.MutexProfile
	case "block":
		mode = profile.BlockProfile
	case "trace":
		mode = profile.TraceProfile
	case "treads":
		mode = profile.ThreadcreationProfile
	}
	path, err := filepath.Abs(cfg.Pprof.Path)
	if err != nil {
		return nil, err
	}
	if cfg.App.Name != "" {
		if _, err := os.Stat(path); err != nil {
			return nil, err
		}
		path += "/" + cfg.App.Name
		if _, err := os.Stat(path); os.IsNotExist(err) {
			err := os.Mkdir(path, 0777)
			if err != nil {
				return nil, err
			}
		} else if err != nil {
			return nil, err
		}
	}
	return profile.Start(mode, profile.ProfilePath(path)), nil
}
