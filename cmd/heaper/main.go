package main

import (
	application "bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/heaper"
	"bitbucket.org/keekee-proj/service-observer/internal/program"
	"context"
	"flag"
	"log"
	"syscall"
)

var (
	filterEntity  = flag.String("entity", "", "filter of entity type")
	filterEvent   = flag.String("event", "", "filter of events")
	filterSubject = flag.String("subject", "", "filter of subscribers")
)

func ctxWithFilters(ctx context.Context) context.Context {
	if *filterEntity != "" {
		ctx = context.WithValue(ctx, heaper.CtxFilterKey(heaper.FilterEntity), *filterEntity)
	}
	if *filterEvent != "" {
		ctx = context.WithValue(ctx, heaper.CtxFilterKey(heaper.FilterEvent), *filterEvent)
	}
	if *filterSubject != "" {
		ctx = context.WithValue(ctx, heaper.CtxFilterKey(heaper.FilterSubject), *filterSubject)
	}

	return ctx
}

func fatalIfErr(format string, err error) {
	if err != nil {
		log.Fatalf(format, err)
	}
}

func main() {
	cfg, err := application.ConfigLoad()
	if err != nil {
		log.Fatalf("failed loading config: [%s]", err)
	}

	if cfg.Pprof != nil && cfg.Pprof.Enabled {
		pprof, err := application.StartProfiling(cfg)
		fatalIfErr("failed starting profiling: [%s]", err)
		defer pprof.Stop()
	}

	app, err := application.Make(cfg)
	if err != nil {
		log.Fatalf("failed run app: [%s]", err)
	}
	ctx := ctxWithFilters(context.Background())

	go application.CancelOnSignals(ctx, app, syscall.SIGINT, syscall.SIGTERM)
	err = app.Run(program.Heaper, ctx)
	app.Close()
	log.Printf("application finish: [%v]", err)

	if err != nil {
		syscall.Exit(1)
	}
}
