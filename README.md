# service-observer
Publishing-subscribing golang application, using on RabbitMQ and MongoDB.
This application ensure order of firing events.

##Table of contents
0. [Configuration](https://bitbucket.org/keekee-proj/service-observer/src/master/docs/configuration.md)
0. [Starting application](https://bitbucket.org/keekee-proj/service-observer/src/master/docs/building.md)
0. [Data models](https://bitbucket.org/keekee-proj/service-observer/src/master/docs/models.md)
0. [Http API](https://bitbucket.org/keekee-proj/service-observer/src/master/docs/api.md)
0. [Metrics](https://bitbucket.org/keekee-proj/service-observer/src/master/docs/metrics.md)
0. [Other](https://bitbucket.org/keekee-proj/service-observer/src/master/docs/others.md)
    - [Filters for subscriptions](https://bitbucket.org/keekee-proj/service-observer/src/master/docs/others.md#markdown-header-filters)
    - [HTTP authorization](https://bitbucket.org/keekee-proj/service-observer/src/master/docs/others.md#markdown-header-authorization)
    - [Duplicate filter](https://bitbucket.org/keekee-proj/service-observer/src/master/docs/others.md#markdown-header-duplicate-filter)
0. [Modules](https://bitbucket.org/keekee-proj/service-observer/src/master/docs/plugins.md)
0. [Example](https://bitbucket.org/keekee-proj/service-observer/src/master/examples)


###Quick start via Docker
Call `make build` to build docker images. After finishing building you should have next images:
```bash
REPOSITORY                    TAG                 IMAGE ID            CREATED             SIZE
service-observer              latest              0fd23728181d        2 hours ago         87.9MB
service-observer              build               43c698da0941        2 hours ago         624MB
service-observer              base                700cca9c04b0        2 hours ago         480MB
```

For start any part of application you can use next commands:
```bash
make run.http
make run.heaper
make run.pipeline MODULE=[module_name] # for example: `templater` (module, which make message by template)
```
or you can use manual docker command:
```bash
docker run --rm -it -e config=$(CONFIG_PATH) -e app=$(APP_PART) -p $(PORT):8883 -v `pwd`/etc:/app/etc service-observer:latest
```
where `APP_PART` is one of [`http`, `heaper`, `pipeline`]