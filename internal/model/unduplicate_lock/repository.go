package unduplicate_lock

import (
	"bitbucket.org/keekee-proj/service-observer/internal/storage"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Repository struct {
	storage    storage.Storage
	collection string
}

const (
	OpAdd = iota
	OpUpdate
)

func NewRepository(s storage.Storage, collection string) *Repository {
	return &Repository{
		s,
		collection,
	}
}

func (r *Repository) One(t string) (*UnduplicateLock, error) {
	e := &UnduplicateLock{}
	if err := r.storage.OneId(r.collection, t, e); err != nil {
		return nil, err
	}

	return e, nil
}

func (r *Repository) AddOrTouch(e *UnduplicateLock) (int, error) {
	q := &bson.M{"_id": e.Sum}
	u := &bson.M{"$inc": &bson.M{"touch_ctn": 1}, "$set": &bson.M{"touched_at": time.Now()}, "$setOnInsert": e}

	_, upd, err := r.storage.Upsert(r.collection, q, u)
	if err != nil {
		return 0, err
	}
	if upd > 0 {
		return OpUpdate, nil
	}
	return OpAdd, nil
}
