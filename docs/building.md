##Building and running application
This application contains of 3 patrs:
 - http api
 - module of manage handling of events (heaper)
 - pipeline module 
 - test http server (for local testing and for trying some examples)

Call next command for build Docker images
```bash
make build
```
or you can use
```bash
make build.test
```
for build all Docker images including test part.

###Starting via Docker
For start any part of application you can use next commands:
```bash
make run.http
make run.heaper
make run.pipeline MODULE=[module_name] # for example: `templater` (module, which make message by template)
```
or you can start full application in a single instance, but it's mode desirable only for test:
```bash
make run.single PORT=8885
```
You can set argument `PORT` for setting any listening post, default 8884

###Starting via docker-compose
Simple starting:
```bash
make up
```
This command run main parts of application and test http server.

You can manage run configuration by docker-compose files.
There are 3 docker-compose files:
 - `docker-compose.yaml` - main compose file
 - `docker-compose-testServ.yaml` - configuration of test server (included by default)
 - `docker-compose-grafana.yaml` - configuration of services for collecting metrics (graphite, statsd, grafana)

For including (or excluding) any compose files, set environment variable `COMPOSE_FILES`, for example:
```bash
export COMPOSE_FILES="-f docker-compose.yaml -f docker-compose-testServ.yaml -f docker-compose-grafana.yaml"
```

For stopping application execute:
```bash
make down
```

For viewing others commands, execute:
```bash
make help
``` 