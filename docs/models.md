##Data models
###Entity
```json
{
  "type": "test_entity",
  "events": [
    "add", "update", "delete"
  ]
}
```

###Subject
```json
{
  "name": "subj_0",
  "entity": "test_entity",
  "events": [
    "add"
  ],
  "filter": {
      "prop_0": "xyz"
    },
  "pipeline": "pipeline_name"
}
```

###Event
```text
{
  "id": "some_id"
  //Some other data
}

```
###Pipeline
```text
{
  "name": "pipe_name",
  "stages": [
    "stage_1",
    "stage_2"
  ]
}
```
###Stage
```text
{
  "name": "stage_1",
  "module": "templater",
  "attributes": {
    "module_attribute_1": "value1",
    "module_attribute_2": "value2"
  }
}
```