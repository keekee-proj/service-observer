package program

import (
	application "bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/heaper"
	"context"
)

func Heaper(app *application.App, ctx context.Context) error {
	h, err := heaper.New(
		app.EventQueueRepository(),
		app.PipeRepository(),
		app.StageRepository(),
		app.Metric(),
	)
	if err != nil {
		return err
	}

	ctxCnl, cnlFn := context.WithCancel(ctx)
	app.RegisterCancelFn(cnlFn)
	h.Run(ctxCnl, app.QueueProvider(), app.Config().Amqp)
	return nil
}
