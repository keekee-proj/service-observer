package subject

type Subject struct {
	Name     string   `bson:"_id"`
	Entity   string   `bson:"entity"`
	Events   []string `bson:"events"`
	Pipeline string   `bson:"pipeline"`
	Filter   Filter   `bson:"filter"`
}
