package pump

import (
	application "bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/config"
	"bitbucket.org/keekee-proj/service-observer/internal/model/entity"
	"bitbucket.org/keekee-proj/service-observer/internal/model/event_queue"
	"bitbucket.org/keekee-proj/service-observer/internal/queue"
	"encoding/json"
	"fmt"
)

const Name = "pump"

type Module struct{}

type Msg struct {
	Entity    string
	Event     string
	EvendData []byte
}

func (m *Module) Name() string {
	return Name
}

func (m *Module) AddToQueue(ch queue.Channel, cfg *config.Amqp, e *entity.Entity, ev string, data []byte) error {
	msg := Msg{Event: ev, Entity: e.Type, EvendData: data}
	msgData, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	return ch.PublishTo(msgData, cfg.Exchange, m.RouteKey(cfg), 0)
}

func (m *Module) RouteKey(cfg *config.Amqp) string {
	return fmt.Sprintf("%s_system_%s", cfg.RouteKey, Name)
}

func (m *Module) QueueName(cfg *config.Amqp) string {
	return fmt.Sprintf("%s_system_%s", cfg.QueueName, Name)
}

func (m *Module) Exec(app *application.App, data []byte) error {
	var msg Msg
	err := json.Unmarshal(data, &msg)
	if err != nil {
		return err
	}

	e, err := app.EntityRepository().OneCached(msg.Entity)
	if err != nil {
		return err
	}

	if !e.HasEvent(msg.Event) {
		return fmt.Errorf("event [%s] not in [%v]", msg.Event, e.Events)
	}

	if err := event_queue.MakeEvents(msg.Event, e, app.SubjectRepository(), app.EventQueueRepository(), msg.EvendData); err != nil {
		return err
	}

	return nil
}
