package metric

import "time"

type Metric interface {
	Incr(metricName string)
	Add(metricName string, count int)
	Value(metricName string, val int)
	Timing(metricName string, duration time.Duration)
	Close()
}
