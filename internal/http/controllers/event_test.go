package controllers

import (
	application "bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/config"
	"bitbucket.org/keekee-proj/service-observer/internal/model/entity"
	"bitbucket.org/keekee-proj/service-observer/internal/model/event_queue"
	"bitbucket.org/keekee-proj/service-observer/internal/model/subject"
	storageInterface "bitbucket.org/keekee-proj/service-observer/internal/storage"
	appMock "bitbucket.org/keekee-proj/service-observer/tests/mock/app"
	"bitbucket.org/keekee-proj/service-observer/tests/mock/storage"
	"bytes"
	"context"
	"github.com/golang/mock/gomock"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestAddEvent_SimpleTest(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	expectEvent := "xxx"
	expectType := "test_entity"

	cfg := &config.Config{}
	storageM := storage.NewMockStorage(ctrl)
	storageM.EXPECT().
		OneId(application.EntityCollection, expectType, gomock.Any()).
		DoAndReturn(func(_, _ string, x interface{}) func(_, _ string, x interface{}) error {
			*(x.(*entity.Entity)) = entity.Entity{expectType, []string{expectEvent, "yyy"}}

			return nil
		})
	itersSubj := 0
	subjs := []*subject.Subject{
		{Name: "s1", Entity: expectType, Events: []string{expectEvent}, Filter: make(subject.Filter)},
		{Name: "s2", Entity: expectType, Events: []string{expectEvent}, Filter: make(subject.Filter)},
		{Name: "s3", Entity: expectType, Events: []string{expectEvent}, Filter: make(subject.Filter)},
	}
	storageM.EXPECT().
		Iter(
			application.SubjectCollection,
			bson.M{"entity": expectType, "events": expectEvent,}).
		DoAndReturn(func(_ string, _ interface{}) storageInterface.Iterator {
			res := storage.NewMockIterator(ctrl)
			res.EXPECT().
				Next(gomock.Any()).
				Times(4).
				DoAndReturn(func(x interface{}) bool {
					if itersSubj >= len(subjs) {
						return false
					}
					*(x.(*subject.Subject)) = *subjs[itersSubj]
					itersSubj++
					return true
				})
			res.EXPECT().Close().Return(nil)
			return res
		})

	resultedEvents := make([]*event_queue.EventQueue, 0, 3)
	storageM.EXPECT().
		Create(application.EventQueueCollection, gomock.Any(), gomock.Any(), gomock.Any()).
		DoAndReturn(func(_ string, docs ...*event_queue.EventQueue) error {
			resultedEvents = append(resultedEvents, docs...)
			return nil
		})
	app := appMock.NewMockApp(cfg, storageM, nil)
	ctx := application.MakeContext(context.Background(), app)
	ctx = context.WithValue(ctx, "entity", expectType)
	ctx = context.WithValue(ctx, "event", expectEvent)

	reqData := `{"id": "event0", "data": {"a": 1, "b": 2}}`
	reqReader := bytes.NewReader([]byte(reqData))
	req, err := http.NewRequest("", "", reqReader)
	if err != nil {
		t.Fatalf("err on preparing test: [%s]", err)
	}

	resp := httptest.NewRecorder()
	err = AddEvent(ctx, resp, req)
	if err != nil {
		t.Fatalf("receive error: [%s]", err)
	}

	respData := resp.Body.Bytes()
	if !reflect.DeepEqual(respData, success) {
		t.Fatalf("incorrect answer: [%s]", respData)
	}

	if len(resultedEvents) != 3 {
		t.Errorf("incorrect numbers of result events: current %d, expected %d", len(resultedEvents), 3)
	}

	for i, e := range resultedEvents {
		if e.EntityType != expectType {
			t.Errorf("incorrect type in event: current %s, expected %s", e.EntityType, "test")
		}
		if reflect.DeepEqual(e.Data, reqData) {
			t.Errorf("incorrect data in event: current %s, expected %s", e.Data, reqData)
		}
		if e.Event != expectEvent {
			t.Errorf("incorrect event: current %s, expected %s", e.Event, expectEvent)
		}
		if e.Status != event_queue.NewEvent {
			t.Errorf("incorrect status: current %s, expected %s", e.Status, event_queue.NewEvent)
		}
		if e.EntityId != "event0" {
			t.Errorf("incorrect event id: current %s, expected %s", e.EntityId, "event0")
		}
		if e.Subject != subjs[i].Name {
			t.Errorf("incorrect subject: current %s, expected %s", e.Subject, subjs[i].Name)
		}
	}
}
