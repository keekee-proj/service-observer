package app

import (
	"bitbucket.org/keekee-proj/service-observer/internal/config"
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"syscall"
	"time"
)

var confPath = flag.String("config", "", "Config file path")

const envServiceName = "SERVICE_NAME"

func ConfigLoad() (*config.Config, error) {
	flag.Parse()
	if *confPath == "" {
		return nil, fmt.Errorf("config not set")
	}
	fData, err := ioutil.ReadFile(*confPath)
	if err != nil {
		return nil, fmt.Errorf("can not open file by path: [%s]: %s", *confPath, err)
	}
	cfg, err := config.ReadFromFile(fData)
	if err != nil {
		return nil, fmt.Errorf("can not read file by path: [%s]: %s", *confPath, err)
	}

	// ToDo: move to other place
	cfg.App.Name, _ = syscall.Getenv(envServiceName)

	return cfg, nil
}

func (app *App) Run(fn Program, ctx context.Context) error {
	log.Println("running program")
	app.working = true
	if !app.testMod {
		go app.gc.Run()
		go RunCollectingRuntimeMetrics(app, 500*time.Millisecond)
	}

	return fn(app, ctx)
}
