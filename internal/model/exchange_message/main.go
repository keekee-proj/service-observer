package exchange_message

import (
	"bitbucket.org/keekee-proj/service-observer/internal/model/event_queue"
	"bitbucket.org/keekee-proj/service-observer/internal/model/pipeline"
	"encoding/json"
	"gopkg.in/mgo.v2/bson"
)

type PipelineMessage struct {
	Name   string   `json:"name"`
	Stages []string `json:"stages"`
}

func (pm *PipelineMessage) OriginalPipe() *pipeline.Pipe {
	return &pipeline.Pipe{
		Name:   pm.Name,
		Stages: pm.Stages,
	}
}

type ExchangeMessage struct {
	EventQueueID bson.ObjectId    `json:"event_queue_id"`
	EntityId     string           `json:"entity_id"`
	Subject      string           `json:"subject"`
	Event        string           `json:"event"`
	Data         []byte           `json:"data"`
	Pipeline     *PipelineMessage `json:"pipeline"`
}

func New(eq *event_queue.EventQueue, pipe *pipeline.Pipe) *ExchangeMessage {
	if pipe == nil {
		pipe = pipeline.DefaultPipe
	}
	return &ExchangeMessage{
		EventQueueID: eq.Id,
		EntityId:     eq.EntityId,
		Subject:      eq.Subject,
		Event:        eq.Event,
		Data:         eq.Data,
		Pipeline: &PipelineMessage{
			Name:   pipe.Name,
			Stages: pipe.Stages,
		},
	}
}

func NewJson(eq *event_queue.EventQueue, pipe *pipeline.Pipe) ([]byte, error) {
	return json.Marshal(New(eq, pipe))
}

func MakeFromJson(jsonMessage []byte) (*ExchangeMessage, error) {
	result := &ExchangeMessage{}

	if err := json.Unmarshal(jsonMessage, result); err != nil {
		return result, err
	}
	return result, nil
}
