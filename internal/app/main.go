package app

import (
	"context"
	"fmt"
	"time"

	"bitbucket.org/keekee-proj/service-observer/internal/config"
	"bitbucket.org/keekee-proj/service-observer/internal/http/duplicate_filter"
	"bitbucket.org/keekee-proj/service-observer/internal/metric"
	"bitbucket.org/keekee-proj/service-observer/internal/metric/statsd"
	"bitbucket.org/keekee-proj/service-observer/internal/metric/stdout"
	"bitbucket.org/keekee-proj/service-observer/internal/model/entity"
	"bitbucket.org/keekee-proj/service-observer/internal/model/event_queue"
	"bitbucket.org/keekee-proj/service-observer/internal/model/pipeline"
	"bitbucket.org/keekee-proj/service-observer/internal/model/subject"
	"bitbucket.org/keekee-proj/service-observer/internal/model/unduplicate_lock"
	"bitbucket.org/keekee-proj/service-observer/internal/queue"
	qProvider "bitbucket.org/keekee-proj/service-observer/internal/queue/provider"
	"bitbucket.org/keekee-proj/service-observer/internal/storage"
	"bitbucket.org/keekee-proj/service-observer/internal/storage/mongodb"
	"bitbucket.org/keekee-proj/service-observer/internal/utils"
	"bitbucket.org/keekee-proj/service-observer/pkg/plugin"
)

type App struct {
	cfg              *config.Config
	db               storage.Storage
	qProv            queue.Provider
	metric           metric.Metric
	eRepo            *entity.Repository
	sRepo            *subject.Repository
	stageRepo        *pipeline.StageRepository
	pipeRepo         *pipeline.PipeRepository
	eqRepo           *event_queue.Repository
	dupServ          duplicate_filter.DuplicateFilterService
	working          bool
	cancelFns        []context.CancelFunc
	version          int
	pluginApiVersion int
	pipePlugins      map[string]plugin.PipeExtension
	gc               *utils.GC
	testMod          bool
}

const (
	EntityCollection     = "entity"
	SubjectCollection    = "subject"
	StageCollection      = "stage"
	PipelineCollection   = "pipe"
	EventQueueCollection = "event_queue"
	DuplicateCollection  = "duplicate"
)

// setting by ldflugs
var (
	AppVersion       = 0
	PluginApiVersion = 0
)

type Program func(*App, context.Context) error

func Make(cfg *config.Config) (*App, error) {
	metricCli, err := createMetric(cfg)
	if err != nil {
		return nil, fmt.Errorf("metric error: [%s]", err)
	}

	db, err := mongodb.New(cfg.Db.Dsn, cfg.Db.Name, true, metricCli)
	if err != nil {
		return nil, fmt.Errorf("storage error: [%s]", err)
	}

	qProv := qProvider.NewRabbitProvider(cfg.Amqp)
	is, err := duplicate_filter.DuplicateService(cfg.App.DuplicateFilter, unduplicate_lock.NewRepository(db, DuplicateCollection))
	if err != nil {
		return nil, fmt.Errorf("duplicate service: [%s]", err)
	}

	var pl map[string]plugin.PipeExtension
	if cfg.Plugins.Path != "" {
		pl, err = setupPlugins(cfg.Plugins)
		if err != nil {
			return nil, fmt.Errorf("setup plugins err: [%s]", err)
		}
	}

	gc := utils.NewGC(time.Millisecond, 10*time.Second, metricCli)
	entityCache := utils.NewCache(cfg.Cache.Ttl, metricCli)
	subjectCache := utils.NewCache(cfg.Cache.Ttl, metricCli)
	pipelineStageCache := utils.NewCache(cfg.Cache.Ttl, metricCli)
	pipeCache := utils.NewCache(cfg.Cache.Ttl, metricCli)
	gc.AddCache(entityCache, subjectCache, pipelineStageCache, pipeCache)

	return New(
		cfg,
		db,
		qProv,
		metricCli,
		entity.NewRepository(db, EntityCollection, entityCache),
		subject.NewRepository(db, SubjectCollection, subjectCache),
		pipeline.NewStageRepository(db, StageCollection, pipelineStageCache),
		pipeline.NewPipeRepository(db, PipelineCollection, pipeCache),
		event_queue.NewRepository(db, EventQueueCollection),
		is,
		false,
		make([]context.CancelFunc, 0),
		AppVersion,
		PluginApiVersion,
		pl,
		gc,
		false,
	), nil
}

func New(cfg *config.Config, db storage.Storage, qProv queue.Provider, metric metric.Metric, eRepo *entity.Repository,
	sRepo *subject.Repository, stageRepo *pipeline.StageRepository, pipeRepo *pipeline.PipeRepository, eqRepo *event_queue.Repository,
	dupServ duplicate_filter.DuplicateFilterService, working bool, cancelFns []context.CancelFunc, version int,
	pluginApiVersion int, pipePlugins map[string]plugin.PipeExtension, gc *utils.GC, testMod bool) *App {
	return &App{
		cfg:              cfg,
		db:               db,
		qProv:            qProv,
		metric:           metric,
		eRepo:            eRepo,
		sRepo:            sRepo,
		stageRepo:        stageRepo,
		pipeRepo:         pipeRepo,
		eqRepo:           eqRepo,
		dupServ:          dupServ,
		working:          working,
		cancelFns:        cancelFns,
		version:          version,
		pluginApiVersion: pluginApiVersion,
		pipePlugins:      pipePlugins,
		gc:               gc,
		testMod:          testMod,
	}
}
func (app *App) Close() {
	app.db.Close()
	app.metric.Close()
	app.gc.Disable()
}

func createMetric(cfg *config.Config) (metric.Metric, error) {
	if cfg.Metric == nil {
		return stdout.New(""), nil
	}

	return statsd.New(cfg.Metric.Address, cfg.Metric.Prefix)
}

func (app *App) Config() config.Config {
	return *app.cfg
}

func (app *App) IsWorking() bool {
	return app.working
}

func (app *App) EntityRepository() *entity.Repository {
	return app.eRepo
}

func (app *App) SubjectRepository() *subject.Repository {
	return app.sRepo
}

func (app *App) StageRepository() *pipeline.StageRepository {
	return app.stageRepo
}

func (app *App) PipeRepository() *pipeline.PipeRepository {
	return app.pipeRepo
}

func (app *App) EventQueueRepository() *event_queue.Repository {
	return app.eqRepo
}

func (app *App) RegisterCancelFn(cnl context.CancelFunc) {
	app.cancelFns = append(app.cancelFns, cnl)
}

func (app *App) QueueProvider() queue.Provider {
	return app.qProv
}

func (app *App) Metric() metric.Metric {
	return app.metric
}

func (app *App) DuplicateService() duplicate_filter.DuplicateFilterService {
	return app.dupServ
}

func (app *App) Version() int {
	return app.version
}

func (app *App) PluginApiVersion() int {
	return app.pluginApiVersion
}
func (app *App) PipePlugins() map[string]plugin.PipeExtension {
	return app.pipePlugins
}

func (app *App) PipePlugin(name string) (p plugin.PipeExtension, ok bool) {
	p, ok = app.pipePlugins[name]
	return
}
