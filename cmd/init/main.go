package main

import (
	application "bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/metric/stdout"
	"bitbucket.org/keekee-proj/service-observer/internal/model/event_queue"
	"bitbucket.org/keekee-proj/service-observer/internal/storage/mongodb"
	"flag"
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"time"
)

var (
	createTimeout    = flag.Duration("duplicate collection timeout", time.Minute*2, "")
	processedTimeout = flag.Duration("processed events timeout", 24*14*time.Hour, "")
)

func main() {
	flag.Parse()
	cfg, err := application.ConfigLoad()
	if err != nil {
		log.Fatalf("failed loading config: [%s]", err)
	}

	m := stdout.New("")
	storage, err := mongodb.New(cfg.Db.Dsn, cfg.Db.Name, false, m)
	if err != nil {
		log.Fatalf("error on connection db: [%s]", err)
	}

	db := storage.DB()
	if err := ic(db, *createTimeout); err != nil {
		log.Println(err)
	} else {
		log.Printf("successful creating index on [%s] collection\n", application.DuplicateCollection)
	}
	if err := eq(db, *processedTimeout); err != nil {
		log.Println(err)
	} else {
		log.Printf("successful creating index on [%s] collection\n", application.EventQueueCollection)
	}
}

func ic(db *mgo.Database, createTimeout time.Duration) error {
	ic := db.C(application.DuplicateCollection)

	cExist, err := isCollectionExists(application.DuplicateCollection, db)
	if err != nil {
		return err
	}
	var idxs []mgo.Index
	if cExist {
		idxs, err = ic.Indexes()
		if err != nil {
			return fmt.Errorf("error on getting indexes: [%s]", err)
		}

		if len(idxs) != 0 {
			return fmt.Errorf("collection [%s] already have some indexes", application.DuplicateCollection)
		}
	}

	err = ic.EnsureIndex(mgo.Index{Key: []string{"touched_at"}, ExpireAfter: createTimeout})
	if err != nil {
		return fmt.Errorf("error on creating index: [%s]", err)
	}

	return nil
}

type indexSpec struct {
	Name, NS         string
	Key              bson.D
	Unique           bool    `,omitempty`
	DropDups         bool    `dropDups,omitempty`
	Background       bool    `,omitempty`
	Sparse           bool    `,omitempty`
	Bits             int     ",omitempty"
	Min              float64 ",omitempty"
	Max              float64 ",omitempty"
	BucketSize       float64 "bucketSize,omitempty"
	ExpireAfter      int     "expireAfterSeconds,omitempty"
	Weights          bson.D  ",omitempty"
	DefaultLanguage  string  "default_language,omitempty"
	LanguageOverride string  "language_override,omitempty"
	TextIndexVersion int     "textIndexVersion,omitempty"
	PartialFilterEx  bson.M  "partialFilterExpression,omitempty"
}

func eq(db *mgo.Database, processedTimeout time.Duration) error {
	cExist, err := isCollectionExists(application.EventQueueCollection, db)
	if err != nil {
		return err
	}
	eqc := db.C(application.EventQueueCollection)

	if cExist {
		idxs, err := eqc.Indexes()
		if err != nil {
			return fmt.Errorf("error on getting indexes: [%s]", err)
		}
		if len(idxs) != 0 {
			return fmt.Errorf("collection [%s] already have some indexes", application.EventQueueCollection)
		}
	}

	spec := indexSpec{
		Name:             "processed_at_1",
		NS:               eqc.FullName,
		Key:              bson.D{{"processed_at", 1}},
		Unique:           false,
		DropDups:         false,
		Background:       false,
		Sparse:           false,
		Bits:             0,
		Min:              0,
		Max:              0,
		BucketSize:       0,
		ExpireAfter:      int(processedTimeout / time.Second),
		Weights:          nil,
		DefaultLanguage:  "",
		LanguageOverride: "",
		PartialFilterEx:  bson.M{"status": event_queue.ProcessedEvent},
	}
	err = db.Run(bson.D{{"createIndexes", application.EventQueueCollection}, {"indexes", []indexSpec{spec}}}, nil)
	if err != nil {
		return fmt.Errorf("error on creating index: [%s]", err)
	}

	return nil
}

func isCollectionExists(cNmae string, db *mgo.Database) (bool, error) {
	cols, err := db.CollectionNames()
	if err != nil {
		return false, err
	}

	cExist := false
	for _, n := range cols {
		if n == cNmae {
			cExist = true
			break
		}
	}

	return cExist, nil
}
