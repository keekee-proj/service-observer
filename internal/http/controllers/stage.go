package controllers

import (
	"bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/model/pipeline"
	"context"
	"errors"
	"io/ioutil"
	"net/http"
)

func UpdateStage(ctx context.Context, response http.ResponseWriter, request *http.Request) error {
	sName := ctx.Value("stage").(string)
	data, err := ioutil.ReadAll(request.Body)
	if err != nil {
		return err
	}
	request.Body.Close()

	s, err := pipeline.ReadJsonStage(data)
	if err != nil {
		return err
	}
	repo := app.MustFromContext(ctx).StageRepository()
	s.Name = sName
	if err = repo.ValidateEdit(s); err != nil {
		return err
	}
	if err = repo.Update(sName, s); err != nil {
		return err
	}
	response.Write(success)
	return nil
}

func Stage(ctx context.Context, response http.ResponseWriter, _ *http.Request) error {
	eName := ctx.Value("stage").(string)

	s, err := app.MustFromContext(ctx).StageRepository().OneCached(eName)
	if err != nil {
		return err
	}
	return WriteResponse(response, pipeline.StageToHttpStruct(s))
}

func Stages(ctx context.Context, response http.ResponseWriter, request *http.Request) error {
	if err := request.ParseForm(); err != nil {
		return err
	}
	q, limit, offset := MakeHttpFilterQuery(request.Form, pipeline.StageFilterQuery)
	s, err := app.MustFromContext(ctx).StageRepository().Find(q, limit, offset)
	if err != nil {
		return err
	}

	return WriteResponse(response, pipeline.StageListToHttpStruct(s))
}

func AddStage(ctx context.Context, response http.ResponseWriter, request *http.Request) error {
	data, err := ioutil.ReadAll(request.Body)
	if err != nil {
		return err
	}
	request.Body.Close()

	if len(data) == 0 {
		return errors.New("request empty")
	}
	s, err := pipeline.ReadJsonStage(data)
	if err != nil {
		return err
	}
	repo := app.MustFromContext(ctx).StageRepository()
	if err = repo.ValidateAdd(s); err != nil {
		return err
	}
	if err = repo.Add(s); err != nil {
		return err
	}
	response.Write(success)
	return nil
}

func DeleteStage(ctx context.Context, response http.ResponseWriter, _ *http.Request) error {
	sName := ctx.Value("stage").(string)
	err := app.MustFromContext(ctx).StageRepository().Delete(sName)
	if err != nil {
		return err
	}
	response.Write(success)
	return nil
}
