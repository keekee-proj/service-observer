package app

import (
	"fmt"
	"runtime"
	"time"
)

func RunCollectingRuntimeMetrics(app *App, sleepTimeout time.Duration) {
	m := &runtime.MemStats{}

	goroutinesPath := "runtime.goroutines"
	memAllocPath := "runtime.mem_alloc"
	memSysPath := "runtime.mem_sys"

	if app.cfg.App.Name != "" {
		goroutinesPath = fmt.Sprintf("%s.%s", app.cfg.App.Name, goroutinesPath)
		memAllocPath = fmt.Sprintf("%s.%s", app.cfg.App.Name, memAllocPath)
		memSysPath = fmt.Sprintf("%s.%s", app.cfg.App.Name, memSysPath)
	}

	for app.working {
		runtime.ReadMemStats(m)
		app.metric.Value(goroutinesPath, runtime.NumGoroutine())
		app.metric.Value(memAllocPath, int(m.Alloc))
		app.metric.Value(memSysPath, int(m.Sys))
		time.Sleep(sleepTimeout)
	}
}
