package md5

import (
	"bitbucket.org/keekee-proj/service-observer/internal/model/unduplicate_lock"
	"crypto/md5"
	"fmt"
	"io/ioutil"
	"net/http"
)

func makeSumStr(md5Data [16]byte) string {
	return fmt.Sprintf("%x", md5Data)
}

type Service struct {
	r *unduplicate_lock.Repository
}

func NewService(r *unduplicate_lock.Repository) *Service {
	return &Service{r}
}

func (i *Service) checkSum(sum string) error {
	model := unduplicate_lock.New(sum)
	op, err := i.r.AddOrTouch(model)
	if err != nil {
		return err
	}

	if op == unduplicate_lock.OpUpdate {
		return fmt.Errorf("sum [%s] already exists", sum)
	}

	return nil
}

func (i *Service) CheckRequest(r *http.Request) error {
	if r.Method != http.MethodPost || r.ContentLength == 0 {
		return nil
	}
	ioBody, err := r.GetBody()
	if err != nil {
		return err
	}

	b, err := ioutil.ReadAll(ioBody)
	if err != nil {
		return err
	}

	sum := md5.Sum(b)
	return i.checkSum(makeSumStr(sum))
}
