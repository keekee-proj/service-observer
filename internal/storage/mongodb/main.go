package mongodb

import (
	"bitbucket.org/keekee-proj/service-observer/internal/metric"
	"bitbucket.org/keekee-proj/service-observer/internal/storage"
	"bitbucket.org/keekee-proj/service-observer/internal/utils"
	"fmt"
	"gopkg.in/mgo.v2"
	"log"
	"time"
)

const (
	maxConcurrentThreads = uint32(100)
	lockWaitTimeout      = 100 * time.Millisecond
)

type Storage struct {
	l       *utils.RWLock
	session *mgo.Session
	db      *mgo.Database
	dsn     string
	dbName  string
	metric  metric.Metric
}

func New(dsn string, dbName string, keepAlive bool, m metric.Metric) (*Storage, error) {
	ses, err := mgo.Dial(dsn)
	if err != nil {
		return nil, err
	}
	db := ses.DB(dbName)
	s := &Storage{
		utils.NewLock(maxConcurrentThreads),
		ses,
		db,
		dsn,
		dbName,
		m,
	}
	if keepAlive {
		go s.keepAlive()
	}
	return s, nil
}

func (s *Storage) Close() {
	s.session.Close()
}

func (s *Storage) DB() *mgo.Database {
	return s.db
}

func (s *Storage) keepAlive() {
	var err error
	for {
		err = s.session.Ping()
		if err != nil {
			s.metric.Incr("storage.connection.error")
			s.l.Lock()
			ses := reconnect(s.dsn)
			s.session = ses
			s.db = ses.DB(s.dbName)
			s.l.Unlock()
			log.Printf("successful reconnect db")
			s.metric.Incr("storage.connection.reconnect")
		}
		time.Sleep(100 * time.Millisecond)
	}
}

func reconnect(dsn string) *mgo.Session {
	var (
		ses *mgo.Session
		err error
	)
	for {
		ses, err = mgo.Dial(dsn)
		if err != nil {
			log.Print("failed reconnect to db")
			continue
		}

		return ses
	}
}

func (s *Storage) All(collection string, result interface{}) error {
	err := s.l.TimeoutRLock(lockWaitTimeout)
	if err != nil {
		s.metric.Incr(fmt.Sprintf("storage.lock.timeout.%s", collection))
		return fmt.Errorf("waiting lock err: [%s]", err)
	}
	defer s.l.RUnlock()
	s.metric.Incr(fmt.Sprintf("storage.query.%s.all", collection))
	return s.db.C(collection).Find(nil).All(result)
}

func (s *Storage) OneId(collection string, id interface{}, result interface{}) error {
	err := s.l.TimeoutRLock(lockWaitTimeout)
	if err != nil {
		s.metric.Incr(fmt.Sprintf("storage.lock.timeout.%s", collection))
		return fmt.Errorf("waiting lock err: [%s]", err)
	}
	defer s.l.RUnlock()
	s.metric.Incr(fmt.Sprintf("storage.query.%s.oneById", collection))
	return s.db.C(collection).FindId(id).One(result)
}

func (s *Storage) Create(collection string, doc ...interface{}) error {
	err := s.l.TimeoutRLock(lockWaitTimeout)
	if err != nil {
		s.metric.Incr(fmt.Sprintf("storage.lock.timeout.%s", collection))
		return fmt.Errorf("waiting lock err: [%s]", err)
	}
	defer s.l.RUnlock()
	s.metric.Incr(fmt.Sprintf("storage.query.%s.create", collection))
	return s.db.C(collection).Insert(doc...)
}

func (s *Storage) RemoveId(collection string, id interface{}) error {
	err := s.l.TimeoutRLock(lockWaitTimeout)
	if err != nil {
		s.metric.Incr(fmt.Sprintf("storage.lock.timeout.%s", collection))
		return fmt.Errorf("waiting lock err: [%s]", err)
	}
	defer s.l.RUnlock()
	s.metric.Incr(fmt.Sprintf("storage.query.%s.removeById", collection))
	return s.db.C(collection).RemoveId(id)
}

func (s *Storage) UpdateId(collection string, id interface{}, doc interface{}) error {
	err := s.l.TimeoutRLock(lockWaitTimeout)
	if err != nil {
		s.metric.Incr(fmt.Sprintf("storage.lock.timeout.%s", collection))
		return fmt.Errorf("waiting lock err: [%s]", err)
	}
	defer s.l.RUnlock()
	s.metric.Incr(fmt.Sprintf("storage.query.%s.updateById", collection))
	return s.db.C(collection).UpdateId(id, doc)
}

func (s *Storage) Iter(collection string, query interface{}, sortF ...string) storage.Iterator {
	s.l.RLock()
	defer s.l.RUnlock()

	s.metric.Incr(fmt.Sprintf("storage.query.%s.iteration", collection))
	return s.db.C(collection).Find(query).Sort(sortF...).Iter()
}

func (s *Storage) Find(collection string, query interface{}, limit int, offset int, result interface{}) error {
	err := s.l.TimeoutRLock(lockWaitTimeout)
	if err != nil {
		s.metric.Incr(fmt.Sprintf("storage.lock.timeout.%s", collection))
		return fmt.Errorf("waiting lock err: [%s]", err)
	}
	defer s.l.RUnlock()
	q := s.db.C(collection).Find(query)
	if limit > 0 {
		q.Limit(limit)
	}
	if offset > 0 {
		q.Skip(offset)
	}
	s.metric.Incr(fmt.Sprintf("storage.query.%s.find", collection))
	return q.All(result)
}

func (s *Storage) Count(collection string, query interface{}) (int, error) {
	err := s.l.TimeoutRLock(lockWaitTimeout)
	if err != nil {
		s.metric.Incr(fmt.Sprintf("storage.lock.timeout.%s", collection))
		return 0, fmt.Errorf("waiting lock err: [%s]", err)
	}
	defer s.l.RUnlock()
	s.metric.Incr(fmt.Sprintf("storage.query.%s.count", collection))
	return s.db.C(collection).Find(query).Count()
}
func (s *Storage) Upsert(collection string, selector, update interface{}) (id interface{}, updated int, err error) {
	er := s.l.TimeoutRLock(lockWaitTimeout)
	if er != nil {
		s.metric.Incr(fmt.Sprintf("storage.lock.timeout.%s", collection))
		return nil, 0, fmt.Errorf("waiting lock err: [%s]", er)
	}
	defer s.l.RUnlock()
	s.metric.Incr(fmt.Sprintf("storage.query.%s.upsert", collection))
	res, err := s.db.C(collection).Upsert(selector, update)

	if err != nil {
		return
	}

	return res.UpsertedId, res.Updated, nil
}
