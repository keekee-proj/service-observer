package modules

import (
	"bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/config"
	"bitbucket.org/keekee-proj/service-observer/internal/modules/pump"
	"errors"
)

type Module interface {
	Exec(*app.App, []byte) error
	Name() string
	RouteKey(*config.Amqp) string
	QueueName(*config.Amqp) string
}

func ModuleByName(name string) (Module, error) {
	switch name {
	case pump.Name:
		return new(pump.Module), nil
	default:
		return nil, errors.New("undefined system module")
	}
}
