
##Other
###Filters
You can specify filters for selecting the desired events:
```json
{
  "name": "subj_0",
  "entity": "test_entity",
  "events": [
    "add"
  ],
  "filter": {
      "prop_0": {
        "@in": [1, 2, 3]
      },
      "prop_1": "xyz",
      "@or": {
        "prop_2": 1,
        "prop_3": 1
      },
      "@exists": "prop_4"
    },
  "pipeline": "pipeline_name"
}
```
Available operators: `@or`, `@and`, `@in`, `@nin`, `@ne`, `@exists`, `@notExists`

Benchmark:
```text
go test -bench=Filter -cpu=8 -benchmem -v -parallel=8  tests/filter_benchmark_test.go 
goos: darwin
goarch: amd64
BenchmarkFilter-8       50000000                27.4 ns/op             0 B/op          0 allocs/op
PASS
ok      command-line-arguments  1.412s

```

###Authorization
If `authKey` setted, you should use header with key `authKey` and value from configuration:
```
curl http://localhost:8884/entities -H "authKey: J0h12oeuoII**yiu12" | jq
---------- conf.yaml ----------
app:
  listen_address: :8884
  authKey: J0h12oeuoII**yiu12
  ...
``` 

###Duplicate filter
When you create an event and something goes wrong - you can use one of the duplicate filters: `md5` or `idempotency-key`.
Algorithm `md5` make checksum and check this for uniqueness.
Algorithm `idempotency-key` get request header `idempotency-key` and use this key for check uniqueness. 