package pipeline

import (
	"encoding/json"
)

var PipeFilterQuery = map[string]string{"_id": "name", "stages": "stages"}

type PipeHttpAdapter struct {
	Name   string   `json:"name"`
	Stages []string `json:"stages"`
}

func PipeToHttpStruct(p *Pipe) *PipeHttpAdapter {
	return convertPipe(p)
}

func convertPipe(p *Pipe) *PipeHttpAdapter {
	return &PipeHttpAdapter{
		Name:   p.Name,
		Stages: p.Stages,
	}
}

func PipeListToHttpStruct(ps []*Pipe) []*PipeHttpAdapter {
	result := make([]*PipeHttpAdapter, 0, len(ps))
	for _, p := range ps {
		result = append(result, convertPipe(p))
	}
	return result
}

func ReadJsonPipe(rowBody []byte) (*Pipe, error) {
	adapter := &PipeHttpAdapter{}
	if err := json.Unmarshal(rowBody, adapter); err != nil {
		return nil, err
	}
	return &Pipe{
		Name:   adapter.Name,
		Stages: adapter.Stages,
	}, nil
}
