package command_bus

var (
	PingCmd  = NewCmd("ping")
	PongCmd  = NewCmd("pong")
	OkCmd    = NewCmd("ok")
	HelloCmd = NewCmd("hello")
	WaitCmd  = NewCmd("wait")
	RunCmd   = NewCmd("run")
)

type Command struct {
	Name string
	Opts []string
}

func NewCmd(name string, args ...string) *Command {
	return &Command{name, args}
}

func NewRunCmd(args ...string) *Command {
	return &Command{RunCmd.Name, args}
}

func makeMessage(cmd *Command) []byte {
	messageBytes := []byte(cmd.Name)
	message := make([]byte, 1, len(messageBytes)+1)
	message[0] = byte(len(messageBytes))
	message = append(message, messageBytes...)

	for _, opt := range cmd.Opts {
		bOpt := []byte(opt)
		message = append(message, byte(len(bOpt)))
		message = append(message, bOpt...)
	}

	return message
}

func readMessage(msg []byte) (cmd *Command, err error) {
	defer func() {
		if er := recover(); er != nil {
			err = er.(error)
		}
	}()

	lc := msg[0] + 1
	command := string(msg[1:lc])

	opts := make([]string, 0, 0)
	i := lc
	for int(i) < len(msg) {
		lo := msg[i]
		j := i + 1
		opts = append(opts, string(msg[j:j+lo]))
		i = j + lo
	}
	return &Command{command, opts}, nil
}
