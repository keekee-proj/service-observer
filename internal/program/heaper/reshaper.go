package heaper

import (
	application "bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/heaper"
	"bitbucket.org/keekee-proj/service-observer/internal/model/entity"
	"bitbucket.org/keekee-proj/service-observer/internal/model/event_queue"
	"bitbucket.org/keekee-proj/service-observer/internal/storage"
	"fmt"
	"time"
)

type reshaper interface {
	reshape(*slList) error
	needReshape(*slList) bool
}

type EntityReshaper struct {
	eR  *entity.Repository
	eqR *event_queue.Repository
}

func reshaperByType(instType string, app *application.App) reshaper {
	switch instType {
	case "subject":
		return NewSubjectReshaper(app.SubjectRepository())
	case "entity":
		fallthrough
	default:
		return newEntityReshaper(app.EntityRepository(), app.EventQueueRepository())
	}
}

func newEntityReshaper(eR *entity.Repository, eqR *event_queue.Repository) *EntityReshaper {
	return &EntityReshaper{eR: eR, eqR: eqR}
}

func (er *EntityReshaper) needReshape(sls *slList) bool {
	if len(sls.lst) == 0 {
		return false
	}

	entities, err := er.eR.All()
	if err != nil || len(entities) == 0 {
		return false
	}

	if len(entities) != len(sls.wt) {
		return true
	}

	for _, ent := range entities {
		if _, ok := sls.wt[ent.Type]; !ok {
			return true
		}
	}

	return sls.dirty
}

func (er *EntityReshaper) reshape(sls *slList) error {
	sls.Lock()
	defer sls.Unlock()

	if time.Since(sls.shapeTime) < time.Minute {
		return sls.shapeResult
	}

	if len(sls.lst) == 0 {
		sls.shapeResult = fmt.Errorf("no slaves")
		return sls.shapeResult
	}

	wt := make(weightTable)
	entities, err := er.eR.All()
	if err != nil {
		sls.shapeResult = err
		return sls.shapeResult
	}

	if len(entities) == 0 {
		return ErrNoEntities
	}

	for _, ent := range entities {
		ctn, _ := er.eqR.Count(&storage.QueryOption{Key: heaper.FilterEntity, Value: ent.Type})
		ctn++
		wt[ent.Type] = ctn
		sls.eqCnt += ctn
	}

	for _, slv := range sls.lst {
		slv.weight = 0
		slv.commandArgs = make([]string, 3)
	}

	for ent := range wt {
		entW := float64(wt[ent]) / float64(sls.eqCnt)
		s := getMinimalSlave(sls)
		if s == nil {
			sls.shapeResult = fmt.Errorf("no slaves")
			return sls.shapeResult
		}
		format := "%s,%s"
		if s.commandArgs[0] == "" {
			format = "%s%s"
		}
		s.commandArgs[0] = fmt.Sprintf(format, s.commandArgs[0], ent)
		s.weight += entW
	}

	sls.wt = wt
	sls.dirty = false

	return nil
}
