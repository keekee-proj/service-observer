package utils

import (
	"bitbucket.org/keekee-proj/service-observer/internal/metric"
	"fmt"
	"sync"
	"time"
)

type Cache struct {
	sync.RWMutex
	ttl    time.Duration
	data   map[interface{}]*cacheValue
	metric metric.Metric
}

func NewCache(ttl time.Duration, m metric.Metric) *Cache {
	return &Cache{
		ttl:    ttl,
		data:   make(map[interface{}]*cacheValue),
		metric: m,
	}
}

type cacheValue struct {
	v      interface{}
	expire time.Time
}

func (c *Cache) remove(key interface{}) {
	c.Lock()
	defer c.Unlock()
	delete(c.data, key)
	c.metric.Incr(fmt.Sprintf("cache.remove.%s", key))
}

func (c *Cache) add(key interface{}, data interface{}, ttl time.Duration) {
	c.Lock()
	defer c.Unlock()
	c.data[key] = &cacheValue{v: data, expire: time.Now().Add(ttl)}
	c.metric.Incr(fmt.Sprintf("cache.add.%s", key))
}

func (c *Cache) get(key interface{}) interface{} {
	c.RLock()
	defer c.RUnlock()
	c.metric.Incr(fmt.Sprintf("cache.get.%s", key))
	val, ok := c.data[key]
	if !ok || val.expire.Before(time.Now()) {
		return nil
	}

	return val.v
}

func (c *Cache) Get(key interface{}, fn func() interface{}) interface{} {
	data := c.get(key)
	if data != nil {
		return data
	}
	data = fn()

	if _, ok := data.(error); ok {
		data = nil
	}
	if data != nil {
		c.add(key, data, c.ttl)
	}
	return data
}
