package controllers

import (
	application "bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/config"
	appMock "bitbucket.org/keekee-proj/service-observer/tests/mock/app"
	"bitbucket.org/keekee-proj/service-observer/tests/mock/storage"
	"bytes"
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestAddEntity_SimpleTest(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	cfg := &config.Config{}
	storageM := storage.NewMockStorage(ctrl)
	storageM.EXPECT().
		OneId(application.EntityCollection, "test_entity", gomock.Any()).
		Return(errors.New("exists"))
	storageM.EXPECT().
		Create(application.EntityCollection, gomock.Any()).
		Return(nil)
	app := appMock.NewMockApp(cfg, storageM, nil)
	ctx := application.MakeContext(context.Background(), app)

	reqData := `{"type": "test_entity", "events": ["add"]}`
	reqReader := bytes.NewReader([]byte(reqData))
	req, err := http.NewRequest("POST", "/entity", reqReader)
	if err != nil {
		t.Fatalf("err on preparing test: [%s]", err)
	}

	resp := httptest.NewRecorder()
	err = AddEntity(ctx, resp, req)
	if err != nil {
		t.Fatalf("receive error: [%s]", err)
	}

	respData := resp.Body.Bytes()
	if !reflect.DeepEqual(respData, success) {
		t.Fatalf("incorrect answer: [%s]", respData)
	}
}

func TestAddEntity_EmptyRequest(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	cfg := &config.Config{}
	storageM := storage.NewMockStorage(ctrl)
	app := appMock.NewMockApp(cfg, storageM, nil)
	ctx := application.MakeContext(context.Background(), app)

	reqData := ``
	reqReader := bytes.NewReader([]byte(reqData))
	req, err := http.NewRequest("POST", "/entity", reqReader)
	if err != nil {
		t.Fatalf("err on preparing test: [%s]", err)
	}

	resp := httptest.NewRecorder()
	err = AddEntity(ctx, resp, req)
	if err == nil {
		t.Fatal("didn't receive error")
	}

	if err.Error() != "request empty" {
		t.Fatalf("incorrect error in answear: [%s]", err)
	}
}

func TestAddEntity_DuplicateEnt(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	cfg := &config.Config{}
	storageM := storage.NewMockStorage(ctrl)

	storageM.EXPECT().
		OneId(application.EntityCollection, "test_entity", gomock.Any()).
		Return(nil)

	app := appMock.NewMockApp(cfg, storageM, nil)
	ctx := application.MakeContext(context.Background(), app)

	reqData := `{"type": "test_entity", "events": ["add"]}`
	reqReader := bytes.NewReader([]byte(reqData))
	req, err := http.NewRequest("POST", "/entity", reqReader)
	if err != nil {
		t.Fatalf("err on preparing test: [%s]", err)
	}

	resp := httptest.NewRecorder()
	err = AddEntity(ctx, resp, req)
	if err == nil {
		t.Fatal("didn't receive error")
	}

	if err.Error() != "alredy exists" {
		t.Fatalf("incorrect error in answear: [%s]", err)
	}
}
