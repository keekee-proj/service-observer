package pipeline

import (
	"bitbucket.org/keekee-proj/service-observer/internal/config"
	"fmt"
)

var DefaultPipe = &Pipe{
	"DEFAULT",
	[]string{},
}

type Pipe struct {
	Name   string   `bson:"_id"`
	Stages []string `bson:"stages"`
}

type Stage struct {
	Name       string                 `bson:"_id"`
	Module     string                 `bson:"module"`
	Attributes map[string]interface{} `bson:"attributes"`
}

type StageQueueConfig struct {
	Exchange  string
	RouteKey  string
	QueueName string
}

type ProcessPipe struct {
	Pipe
	CurrentStage string
}

func ModuleConfigQueue(moduleName string, conf *config.Amqp) *StageQueueConfig {
	return &StageQueueConfig{
		conf.Exchange,
		fmt.Sprintf("%s_%s", conf.RouteKey, moduleName),
		fmt.Sprintf("%s_%s", conf.QueueName, moduleName),
	}
}

func NewProcessPipe(p *Pipe, currentStage string) *ProcessPipe {
	return &ProcessPipe{
		*p,
		currentStage,
	}
}

func (p *ProcessPipe) NextStageName() (string, error) {
	var found bool

	for _, st := range p.Stages {
		if found {
			return st, nil
		}

		if st == p.CurrentStage {
			found = true
		}
	}

	if !found {
		return "", fmt.Errorf(
			"not found current stage [%s] in pipe [%s]",
			p.CurrentStage,
			p.Name,
		)
	}

	return "", nil
}

func (p *ProcessPipe) Stage(repo *StageRepository) (*Stage, error) {
	return repo.OneCached(p.CurrentStage)
}
