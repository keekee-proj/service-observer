#!/usr/bin/env bash
curl -X DELETE http://localhost:8884/entity/test_entity -H "authKey: J0h12oeuoII**yiu12" | jq
curl -X DELETE http://localhost:8884/stage/send_http_test -H "authKey: J0h12oeuoII**yiu12" | jq
curl -X DELETE http://localhost:8884/stage/stage_tpl_test -H "authKey: J0h12oeuoII**yiu12" | jq
curl -X DELETE http://localhost:8884/pipe/pipe_test -H "authKey: J0h12oeuoII**yiu12" | jq
curl -X DELETE http://localhost:8884/subject/subj_0 -H "authKey: J0h12oeuoII**yiu12" | jq

curl http://localhost:8884/entities -H "authKey: J0h12oeuoII**yiu12" | jq
curl http://localhost:8884/stages -H "authKey: J0h12oeuoII**yiu12" | jq
curl http://localhost:8884/pipes -H "authKey: J0h12oeuoII**yiu12" | jq
curl http://localhost:8884/subjects -H "authKey: J0h12oeuoII**yiu12" | jq
