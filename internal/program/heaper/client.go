package heaper

import (
	application "bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/heaper"
	"bitbucket.org/keekee-proj/service-observer/internal/heaper/command_bus"
	"context"
	"log"
	"time"
)

func Client(app *application.App, ctx context.Context) error {
	script := func(bus *command_bus.Bus, commands <-chan *command_bus.Command) {
		if err := bus.Call(command_bus.HelloCmd); err != nil {
			return
		}
		<-commands // ok

	waitRunCmd:
		runCmd, ok := <-commands

		if !ok {
			return
		}

		if runCmd.Name == command_bus.WaitCmd.Name {
			log.Println("have to wait")
			goto waitRunCmd
		}

		if runCmd.Name != command_bus.RunCmd.Name {
			log.Println("incorrect cmd:", runCmd.Name)
			return
		}
		log.Printf("%v", runCmd.Opts)
		scriptCtx, cnlCtx := context.WithCancel(ctx)
		for i, filter := range heaper.AvailableFilters {
			if i >= len(runCmd.Opts) {
				break
			}

			if v := runCmd.Opts[i]; v != "" && v != "*" {
				scriptCtx = context.WithValue(scriptCtx, heaper.CtxFilterKey(filter), v)
			}
		}
		go func() {
			for app.IsWorking() {
				err := bus.Call(command_bus.PingCmd)
				if err != nil {
					break
				}
				time.Sleep(time.Second)
			}
			cnlCtx()
		}()
		lastPong := new(time.Time)
		*lastPong = time.Now()
		tik := time.NewTicker(time.Second * 5)
		go func(tik *time.Ticker, lp *time.Time) {
			for range tik.C {
				if time.Since(*lp) > time.Second*5 {
					log.Printf("last pong was a long time ago: [%s]", *lp)
					cnlCtx()
				}
			}
		}(tik, lastPong)

		go func(cmds <-chan *command_bus.Command, lp *time.Time) {
			for cmd := range cmds {
				if cmd.Name == command_bus.PongCmd.Name {
					*lp = time.Now()
					continue
				}
			}
		}(commands, lastPong)

		h, err := heaper.New(
			app.EventQueueRepository(),
			app.PipeRepository(),
			app.StageRepository(),
			app.Metric(),
		)
		if err != nil {
			log.Println(err)
			return
		}
		h.Run(scriptCtx, app.QueueProvider(), app.Config().Amqp)
		tik.Stop()
	}
	cliCtx, cnlFn := context.WithCancel(ctx)
	app.RegisterCancelFn(cnlFn)
	for app.IsWorking() {
		cli := command_bus.NewCliBus(cliCtx, script)
		cli.Dial(app.Config().HCluster.Address)
	}
	return nil
}
