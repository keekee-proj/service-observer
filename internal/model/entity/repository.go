package entity

import (
	"bitbucket.org/keekee-proj/service-observer/internal/storage"
	"bitbucket.org/keekee-proj/service-observer/internal/utils"
	"fmt"
)

type Repository struct {
	storage    storage.Storage
	collection string
	cache      *utils.Cache
}

func NewRepository(s storage.Storage, collection string, cache *utils.Cache) *Repository {
	return &Repository{s, collection, cache}
}

func (r *Repository) One(t string) (*Entity, error) {
	e := &Entity{}
	if err := r.storage.OneId(r.collection, t, e); err != nil {
		return nil, err
	}

	return e, nil
}

func (r *Repository) OneCached(t string) (subj *Entity, err error) {
	subj, _ = r.cache.Get(t, func() interface{} {
		var res *Entity
		res, err = r.One(t)
		if err != nil {
			return err
		}
		return res
	}).(*Entity)
	return
}

func (r *Repository) All() ([]*Entity, error) {
	var res []*Entity
	if err := r.storage.All(r.collection, &res); err != nil {
		return nil, err
	}

	return res, nil
}

func (r *Repository) Update(t string, e *Entity) error {
	if err := r.storage.UpdateId(r.collection, t, e); err != nil {
		return err
	}

	return nil
}

func (r *Repository) Delete(t string) error {
	if err := r.storage.RemoveId(r.collection, t); err != nil {
		return err
	}

	return nil
}

func (r *Repository) Add(e *Entity) error {
	if err := r.storage.Create(r.collection, e); err != nil {
		return err
	}

	return nil
}

func (r *Repository) ValidateAdd(e *Entity) error {
	if err := r.ValidateEdit(e); err != nil {
		return err
	}
	if _, err := r.One(e.Type); err == nil {
		return fmt.Errorf("alredy exists")
	}
	return nil
}

func (r *Repository) ValidateEdit(e *Entity) error {
	if e.Type == "" {
		return fmt.Errorf("type not set")
	}
	if e.Events == nil || len(e.Events) == 0 {
		return fmt.Errorf("events not set")
	}
	return nil
}

func (r *Repository) Find(q interface{}, limit int, offset int) ([]*Entity, error) {
	var res []*Entity
	if err := r.storage.Find(r.collection, q, limit, offset, &res); err != nil {
		return nil, err
	}

	return res, nil
}
