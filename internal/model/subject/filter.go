package subject

import (
	"reflect"
)

type Filter map[string]interface{}

const (
	in        = "@in"
	and       = "@and"
	or        = "@or"
	ne        = "@ne"
	nin       = "@nin"
	exists    = "@exists"
	notExists = "@notExists"
)

func (f Filter) IsSuited(i map[string]interface{}) bool {
	return opAnd(f, i)
}

func isOp(key string) bool {
	return key == in ||
		   key == and ||
		   key == or ||
		   key == ne ||
		   key == nin ||
		   key == exists ||
		   key == notExists
}

func check(exc interface{}, inpt interface{}) bool {
	f, ok := exc.(Filter)
	if !ok {
		return opEq(exc, inpt)
	}
	return opAnd(f, inpt)
}

func checkOp(op string, expected interface{}, input interface{}) bool {
	if op == in {
		return opIn(expected, input)
	}
	if op == and {
		exc, ok := expected.(Filter)
		if !ok {
			return false
		}
		return opAnd(exc, input)
	}
	if op == or {
		exc, ok := expected.(Filter)
		if !ok {
			return false
		}
		return opOr(exc, input)
	}
	if op == ne {
		return !check(expected, input)
	}
	if op == nin {
		return !opIn(expected, input)
	}
	if op == exists {
		return opExists(expected, input)
	}
	if op == notExists {
		return !opExists(expected, input)
	}
	return false
}
func opExists(expected interface{}, inpt interface{}) bool {
	key, ok := expected.(string)
	if !ok {
		return false
	}
	model, ok := inpt.(map[string]interface{})
	if !ok {
		return false
	}

	if _, ok := model[key]; ok {
		return true
	}
	return false
}

func opEq(expected interface{}, input interface{}) bool {
	return reflect.DeepEqual(expected, input)
}

func opIn(expected interface{}, inpt interface{}) bool {
	if reflect.TypeOf(expected).Kind() != reflect.Slice {
		return false
	}
	sl := reflect.ValueOf(expected)
	for i := 0; i < sl.Len(); i++ {
		if opEq(sl.Index(i).Interface(), inpt) {
			return true
		}
	}

	return false
}

func opAnd(f Filter, input interface{}) bool {
	for key, fData := range f {
		if isOp(key) {
			if ok := checkOp(key, fData, input); !ok {
				return false
			}
			continue
		}
		if inMap, ok := input.(map[string]interface{}); ok {
			raw, ok := inMap[key]
			if !ok {
				return false
			}
			if !check(fData, raw) {
				return false
			}
			continue
		}
		return false
	}
	return true
}

func opOr(f Filter, input interface{}) bool {
	for key, fData := range f {
		if isOp(key) {
			if ok := checkOp(key, fData, input); ok {
				return true
			}
			continue
		}
		if inMap, ok := input.(map[string]interface{}); ok {
			raw, ok := inMap[key]
			if !ok {
				continue
			}
			if check(fData, raw) {
				return true
			}
		}
	}
	return false
}
