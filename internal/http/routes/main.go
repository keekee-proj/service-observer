package routes

import (
	router "bitbucket.org/keekee-proj/router/v2"
	"bitbucket.org/keekee-proj/service-observer/internal/http/controllers"
	"bitbucket.org/keekee-proj/service-observer/internal/http/middlewares"
)

var List = router.NewRouteList()

func init() {
	_ = List.AddGroup(
		router.NewGroup("/entity", middlewares.CheckKey, middlewares.CheckEntity).AddRoutes(
			router.NewRoute(router.PUT, "/entity:string", controllers.UpdateEntity),
			router.NewRoute(router.GET, "/entity:string", controllers.Entity),
			router.NewRoute(router.DELETE, "/entity:string", controllers.DeleteEntity),
		),
	)
	_ = List.AddGroup(
		router.NewGroup("", middlewares.CheckKey, middlewares.CheckEntity, middlewares.DuplicateFilter).AddRoutes(
			router.NewRoute(router.POST, "/entity/entity:string/event/event:string", controllers.AddEvent),
		),
	)
	_ = List.AddGroup(
		router.NewGroup("/", middlewares.CheckKey).AddRoutes(
			router.NewRoute(router.POST, "/entity", controllers.AddEntity),
			router.NewRoute(router.GET, "/entities", controllers.Entities),
		),
	)
	_ = List.AddGroup(
		router.NewGroup("/subject", middlewares.CheckKey, middlewares.CheckSubject).AddRoutes(
			router.NewRoute(router.PUT, "/subject:string", controllers.UpdateSubject),
			router.NewRoute(router.GET, "/subject:string", controllers.Subject),
			router.NewRoute(router.DELETE, "/subject:string", controllers.DeleteSubject),
		),
	)
	_ = List.AddGroup(
		router.NewGroup("/", middlewares.CheckKey).AddRoutes(
			router.NewRoute(router.POST, "/subject", controllers.AddSubject),
			router.NewRoute(router.GET, "/subjects", controllers.Subjects),
		),
	)
	_ = List.AddGroup(
		router.NewGroup("/stage", middlewares.CheckKey, middlewares.CheckStage).AddRoutes(
			router.NewRoute(router.PUT, "/stage:string", controllers.UpdateStage),
			router.NewRoute(router.GET, "/stage:string", controllers.Stage),
			router.NewRoute(router.DELETE, "/stage:string", controllers.DeleteStage),
		),
	)
	_ = List.AddGroup(
		router.NewGroup("/", middlewares.CheckKey).AddRoutes(
			router.NewRoute(router.POST, "/stage", controllers.AddStage),
			router.NewRoute(router.GET, "/stages", controllers.Stages),
		),
	)
	_ = List.AddGroup(
		router.NewGroup("/pipe", middlewares.CheckKey, middlewares.CheckPipe).AddRoutes(
			router.NewRoute(router.PUT, "/pipe:string", controllers.UpdatePipe),
			router.NewRoute(router.GET, "/pipe:string", controllers.Pipe),
			router.NewRoute(router.DELETE, "/pipe:string", controllers.DeletePipe),
		),
	)
	_ = List.AddGroup(
		router.NewGroup("/", middlewares.CheckKey).AddRoutes(
			router.NewRoute(router.POST, "/pipe", controllers.AddPipe),
			router.NewRoute(router.GET, "/pipes", controllers.Pipes),
		),
	)
	_ = List.AddGroup(
		router.NewGroup("/", middlewares.CheckKey).AddRoutes(
			router.NewRoute(router.GET, "/modules", controllers.Modules),
		),
	)
}
