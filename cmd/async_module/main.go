package main

import (
	application "bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/program/system_module"
	"context"
	"flag"
	"log"
	"syscall"
)

var module = flag.String("module", "", "system async module")

func fatalIfErr(format string, err error) {
	if err != nil {
		log.Fatalf(format, err)
	}
}

func main() {
	flag.Parse()
	cfg, err := application.ConfigLoad()
	fatalIfErr("failed loading config: [%s]", err)

	if cfg.Pprof != nil && cfg.Pprof.Enabled {
		pprof, err := application.StartProfiling(cfg)
		fatalIfErr("failed starting profiling: [%s]", err)
		defer pprof.Stop()
	}

	app, err := application.Make(cfg)
	fatalIfErr("failed run app: [%s]", err)

	ctx := context.Background()
	go application.CancelOnSignals(ctx, app, syscall.SIGINT, syscall.SIGTERM)
	err = app.Run(system_module.AsyncSystemModule, system_module.NewSystemModuleContext(ctx, *module))
	app.Close()
	if err != nil {
		log.Fatalf("application finish: [%v]", err)
	}

	log.Println("application successful finish")
}
