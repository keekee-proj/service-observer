package command_bus

import (
	"context"
	"log"
	"net"
	"time"
)

type CliBus struct {
	ctx      context.Context
	bs       *Bus
	scriptFn ScriptFn
}

func NewCliBus(ctx context.Context, scFn ScriptFn) *CliBus {
	return &CliBus{ctx, nil, scFn}
}

func (b *CliBus) Dial(addr string) error {
	log.Printf("starting dial %v\n", addr)
	tik := time.NewTicker(time.Second)
tikCycle:
	for {
		select {
		case <-b.ctx.Done():
			tik.Stop()
			return b.ctx.Err()
		case <-tik.C:
			address, err := net.ResolveTCPAddr("tcp", addr)
			if err != nil {
				log.Printf("err on ResolveTCPAddr: [%s]", err)
				continue
			}

			con, err := net.DialTCP("tcp", nil, address)
			if err == nil {
				b.bs = NewBus(con, 1024)
				break tikCycle
			}
			log.Printf("err on DialTCP: [%s]", err)
		}
	}
	tik.Stop()
	go func() {
		<-b.ctx.Done()
		b.bs.Close(nil)
	}()
	listenFunc(b, b.bs)

	return nil
}

func (b *CliBus) Call(cmd *Command) error {
	return b.bs.Call(cmd)
}
func (b *CliBus) script(_ *Bus, ch <-chan *Command) {
	b.scriptFn(b.bs, ch)
}
func (b *CliBus) closeBus(_ *Bus) {
	b.bs.Close(nil)
}
