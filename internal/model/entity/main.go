package entity

type Entity struct {
	Type   string   `bson:"_id"`
	Events []string `bson:"events"`
}

func (e *Entity) HasEvent(event string) bool {
	for _, ev := range e.Events {
		if ev == event {
			return true
		}
	}
	return false
}
