package controllers

import (
	"bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/model/subject"
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)

func UpdateSubject(ctx context.Context, response http.ResponseWriter, request *http.Request) error {
	sName := ctx.Value("subject").(string)
	data, err := ioutil.ReadAll(request.Body)
	if err != nil {
		return err
	}
	request.Body.Close()

	s, err := subject.ReadJson(data)
	if err != nil {
		return err
	}
	application := app.MustFromContext(ctx)
	repo := application.SubjectRepository()
	if err = repo.ValidateEdit(s, application.PipeRepository()); err != nil {
		return err
	}
	s.Name = sName
	if err = repo.Update(sName, s); err != nil {
		return err
	}
	response.Write(success)
	return nil
}

func Subject(ctx context.Context, response http.ResponseWriter, _ *http.Request) error {
	eName := ctx.Value("subject").(string)

	s, err := app.MustFromContext(ctx).SubjectRepository().OneCached(eName)
	if err != nil {
		return err
	}
	return WriteResponse(response, subject.ToHttpStruct(s))
}

func Subjects(ctx context.Context, response http.ResponseWriter, request *http.Request) error {
	if err := request.ParseForm(); err != nil {
		return err
	}
	q, limit, offset := MakeHttpFilterQuery(request.Form, subject.FilterQuery)
	s, err := app.MustFromContext(ctx).SubjectRepository().Find(q, limit, offset)
	if err != nil {
		return err
	}

	return WriteResponse(response, subject.ListToHttpStruct(s))
}

func AddSubject(ctx context.Context, response http.ResponseWriter, request *http.Request) error {
	data, err := ioutil.ReadAll(request.Body)
	if err != nil {
		return err
	}
	request.Body.Close()

	if len(data) == 0 {
		return errors.New("request empty")
	}
	s, err := subject.ReadJson(data)
	if err != nil {
		return fmt.Errorf("error on readin json: %s", err)
	}
	application := app.MustFromContext(ctx)
	repo := application.SubjectRepository()
	if err = repo.ValidateAdd(s, application.PipeRepository()); err != nil {
		return err
	}
	if err = repo.Add(s); err != nil {
		return err
	}
	response.Write(success)
	return nil
}

func DeleteSubject(ctx context.Context, response http.ResponseWriter, _ *http.Request) error {
	sName := ctx.Value("subject").(string)
	err := app.MustFromContext(ctx).SubjectRepository().Delete(sName)
	if err != nil {
		return err
	}
	response.Write(success)
	return nil
}
