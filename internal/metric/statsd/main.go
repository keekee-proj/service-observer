package statsd

import (
	"bitbucket.org/keekee-proj/service-observer/internal/metric"
	"github.com/alexcesaro/statsd"
	"math"
	"time"
)

type Metric struct {
	cli *statsd.Client
}

func New(addr, prefix string) (metric.Metric, error) {
	cli, err := statsd.New(statsd.Address(addr), statsd.Prefix(prefix))
	if err != nil {
		return nil, err
	}
	return &Metric{cli: cli}, nil
}

func (s *Metric) Close() {
	s.cli.Close()
}

func (s *Metric) Incr(metricName string) {
	s.cli.Increment(metricName)
}

func (s *Metric) Add(metricName string, count int) {
	s.cli.Count(metricName, count)
}

func (s *Metric) Value(metricName string, val int) {
	s.cli.Gauge(metricName, val)
}

func (s *Metric) Timing(metricName string, duration time.Duration) {
	s.cli.Timing(metricName, durationToMills(duration))
}
func durationToMills(duration time.Duration) int {
	return int(math.Round(float64(duration / time.Millisecond)))
}
