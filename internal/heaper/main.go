package heaper

import (
	"bitbucket.org/keekee-proj/service-observer/internal/config"
	"bitbucket.org/keekee-proj/service-observer/internal/metric"
	"bitbucket.org/keekee-proj/service-observer/internal/model/event_queue"
	"bitbucket.org/keekee-proj/service-observer/internal/model/exchange_message"
	"bitbucket.org/keekee-proj/service-observer/internal/model/pipeline"
	"bitbucket.org/keekee-proj/service-observer/internal/queue"
	"bitbucket.org/keekee-proj/service-observer/internal/storage"
	"context"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"log"
	"strings"
	"sync"
	"time"
	"unicode/utf8"
)

const (
	FilterEntity  = "entity_type"
	FilterEvent   = "event"
	FilterSubject = "subject"
)

var AvailableFilters = []string{FilterEntity, FilterEvent, FilterSubject}

type lockList struct {
	sync.RWMutex
	l map[string]bool
}

type Heaper struct {
	lockList *lockList
	eqR      *event_queue.Repository
	pR       *pipeline.PipeRepository
	stR      *pipeline.StageRepository
	metric   metric.Metric
	working  bool
}

func New(eqR *event_queue.Repository, pR *pipeline.PipeRepository, stR *pipeline.StageRepository, m metric.Metric) (*Heaper, error) {
	h := &Heaper{
		&lockList{},
		eqR,
		pR,
		stR,
		m,
		true,
	}
	return h, nil
}

func CtxFilterKey(fName string) string {
	return "filter_" + fName
}

func loadPipeline(name string, h *Heaper) (*pipeline.Pipe, error) {
	p, err := h.pR.OneCached(name)

	if err != nil {
		return nil, err
	}

	return p, nil
}

func loadStage(name string, h *Heaper) (*pipeline.Stage, error) {
	s, err := h.stR.OneCached(name)

	if err != nil {
		return nil, err
	}

	return s, nil
}

func makeKey(eq *event_queue.EventQueue) string {
	return fmt.Sprintf("subject:%s_type:%s_id:%s", eq.Subject, eq.EntityType, eq.EntityId)
}

func (pl *lockList) isLock(eq *event_queue.EventQueue) bool {
	pl.RLock()
	defer pl.RUnlock()
	return pl.l[makeKey(eq)]
}

func (h *Heaper) loadLockList(opts []*storage.QueryOption) {
	h.lockList.Lock()
	defer h.lockList.Unlock()
	err := fmt.Errorf("")
	for err != nil {
		maybeBroken := make([]*brokeRow, 0)
		now := time.Now()
		h.lockList.l = make(map[string]bool)
		iter := h.eqR.IterByStatus(event_queue.ProcessEvent, opts...)
		res := &event_queue.EventQueue{}
		for iter.Next(res) {
			if res.StartProcessAt.After(res.CreatedAt) && now.Sub(res.StartProcessAt) > 3*time.Hour {
				maybeBroken = append(maybeBroken, &brokeRow{
					res.Id.Hex(),
					res.EntityType,
					res.EntityId,
					res.Event,
					now.Sub(res.StartProcessAt),
				})
			}
			h.lockList.add(res)
		}
		iter.Close()
		err = iter.Err()

		if err != nil {
			log.Printf("error on loadLockList: [%s]", err)
			notSoFast(time.Now())
		}

		if len(maybeBroken) > 0 {
			printBrokenTable(maybeBroken)
		}
	}
}

func (pl *lockList) add(eq *event_queue.EventQueue) {
	pl.l[makeKey(eq)] = true
}

func (h *Heaper) lock(eq *event_queue.EventQueue) {
	h.lockList.Lock()
	defer h.lockList.Unlock()
	h.lockList.add(eq)
}

func watchStop(h *Heaper, ctx context.Context) {
	<-ctx.Done()
	h.working = false
}
func parseFilterValue(name, filterValue string) *storage.QueryOption {
	filterValue = strings.Trim(filterValue, "")
	firstLetter, _ := utf8.DecodeRuneInString(filterValue)
	negative := firstLetter == '-'
	if negative {
		filterValue = strings.Trim(strings.TrimLeft(filterValue, "-"), " ")
	}

	if strings.IndexRune(filterValue, ',') > 0 {
		dirtyVals := strings.Split(filterValue, ",")
		vals := make([]string, 0, len(dirtyVals))
		for _, val := range dirtyVals {
			vals = append(vals, strings.Trim(val, " "))
		}

		if negative {
			return &storage.QueryOption{Key: name, Value: bson.M{"$nin": vals}}
		}

		return &storage.QueryOption{Key: name, Value: bson.M{"$in": vals}}
	}

	if negative {
		return &storage.QueryOption{Key: name, Value: bson.M{"$ne": filterValue}}
	}

	return &storage.QueryOption{Key: name, Value: filterValue}
}

func optionsFromContext(ctx context.Context) []*storage.QueryOption {
	opts := make([]*storage.QueryOption, 0, 3)

	for _, filter := range AvailableFilters {
		v := ctx.Value(CtxFilterKey(filter))
		if v != nil {
			filVal, ok := v.(string)
			if !ok || filVal == "" {
				continue
			}
			opts = append(opts, parseFilterValue(filter, filVal))
		}
	}

	return opts
}
func (h *Heaper) Run(ctx context.Context, qProv queue.Provider, amqp *config.Amqp) {
	go watchStop(h, ctx)
	opts := optionsFromContext(ctx)

	channel, err := qProv.MakeChannel()
	if err != nil {
		log.Fatalf("failed connect to amqp: [%s]", err)
	}
	var channelErrCtn int
	row := &event_queue.EventQueue{}
	for h.working {
		h.loadLockList(opts)
		start := time.Now()
		iter := h.eqR.IterByStatus(event_queue.NewEvent, opts...)
		var n int
		for iter.Next(row) && h.working {
			n++
			startIter := time.Now()
			itemId := row.Id.Hex()
			log.Printf("item [%s] in heaper", itemId)

			if h.lockList.isLock(row) {
				log.Printf("item [%s] locked", itemId)
				h.metric.Incr("heaper.item.skip.locked")
				continue
			}
			if row.NextAttempt.After(time.Now()) {
				// Deferred row, lock
				h.lock(row)
				h.metric.Incr("heaper.item.skip.deferred")
				continue
			}

			if err := h.eqR.SetProcess(row); err != nil {
				log.Printf("failed setting 'Process' status [%s] for [%s]", err, itemId)
				h.metric.Incr("heaper.item.error.set_status_process")
				break
			}
			pipe, err := loadPipeline(row.Pipeline, h)
			if err != nil {
				err = fmt.Errorf("error on loading pipeline [%s] for [%s]: (%s)", row.Pipeline, itemId, err)
				h.eqR.SetError(row, err)
				log.Println(err)
				h.metric.Incr("heaper.item.error.unknown_pipeline")
				continue
			}

			if len(pipe.Stages) < 1 {
				h.eqR.SetProcessed(row)
				log.Printf("pipeline [%s] is empty. EQ: [%s]", row.Pipeline, itemId)
				h.metric.Incr("heaper.item.error.empty_pipeline")
				continue
			}
			nextStage, err := loadStage(pipe.Stages[0], h)
			if err != nil {
				err = fmt.Errorf("undefined stage [%s]: (%s)", pipe.Stages[0], err)
				h.eqR.SetError(row, err)
				log.Println(err)
				h.metric.Incr("heaper.item.error.undefined_stage")
				continue
			}

			message, err := exchange_message.NewJson(row, pipe)

			if err != nil {
				errorMsg := fmt.Errorf("failed marshaling [%s]", err)
				h.eqR.SetError(row, errorMsg)
				log.Printf("%s", errorMsg)
				h.metric.Incr("heaper.item.error.marshaling")
				continue
			}

			stageQueueConf := pipeline.ModuleConfigQueue(nextStage.Module, amqp)

			if err := channel.PublishTo(message, stageQueueConf.Exchange, stageQueueConf.RouteKey, 0); err != nil {
				log.Printf("item [%s] not sent [%s]", row.Id.Hex(), err)
				h.eqR.SetNew(row)
				h.metric.Incr("heaper.item.error.publishing")
				channelErrCtn++
			} else {
				channelErrCtn = 0
				log.Printf("item [%s] sent to [%s]", row.Id.Hex(), stageQueueConf.RouteKey)
				h.metric.Incr(fmt.Sprintf("heaper.item.success.%s", nextStage.Module))
				h.metric.Timing("heaper.time.item", time.Since(startIter))
			}

			h.lock(row)
		}
		iter.Close()
		if n > 0 {
			h.metric.Timing("heaper.time.collection", time.Since(start))
		}
		if channelErrCtn > 5 {
			log.Fatalf("receive great then 5 errors with amqp")
		}
		notSoFast(start)
	}
}

func notSoFast(start time.Time) {
	d := time.Since(start)
	minDuration := 500 * time.Millisecond
	if d < minDuration {
		time.Sleep(minDuration - d)
	}
}
