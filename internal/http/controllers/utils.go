package controllers

import (
	"gopkg.in/mgo.v2/bson"
	"net/url"
	"strconv"
	"strings"
)

func MakeHttpFilterQuery(f url.Values, filter map[string]string) (q bson.M, limit int, offset int) {
	q = make(bson.M)
	for qKey, fKey := range filter {
		if fVal := f.Get(fKey); fVal != "" {
			processValue(q, qKey, fVal)
		}
	}
	limit = 0
	if l := f.Get("limit"); l != "" {
		limit, _ = strconv.Atoi(l)
	}
	offset = 0
	if s := f.Get("offset"); s != "" {
		offset, _ = strconv.Atoi(s)
	}
	return
}

func processValue(q bson.M, qKey string, v string) {
	if isList(v) {
		q[qKey] = bson.M{"$in": strings.Split(v, ",")}
	} else {
		q[qKey] = v
	}
}

func isList(v string) bool {
	return strings.ContainsRune(v, ',')
}
