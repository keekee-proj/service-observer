package app

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
)

func (app *App) Cancel(err error) {
	app.working = false

	log.Printf("canceling application: %s", err)
	for _, fn := range app.cancelFns {
		fn()
	}
}

func CancelOnSignals(ctx context.Context, app *App, signals ...os.Signal) {
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, signals...)
	select {
	case sig := <-ch:
		app.Cancel(fmt.Errorf("os signal [%s:%d]", sig, sig))
		return
	case <-ctx.Done():
		app.Cancel(ctx.Err())
		return
	}
}
