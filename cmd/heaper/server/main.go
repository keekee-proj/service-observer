package main

import (
	application "bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/program/heaper"
	"context"
	"log"
	"syscall"
)

func fatalIfErr(format string, err error) {
	if err != nil {
		log.Fatalf(format, err)
	}
}

func main() {
	cfg, err := application.ConfigLoad()
	fatalIfErr("failed loading config: [%s]", err)

	if cfg.Pprof != nil && cfg.Pprof.Enabled {
		pprof, err := application.StartProfiling(cfg)
		fatalIfErr("failed starting profiling: [%s]", err)
		defer pprof.Stop()
	}

	app, err := application.Make(cfg)
	fatalIfErr("failed run app: [%s]", err)

	ctx := context.Background()
	go application.CancelOnSignals(ctx, app, syscall.SIGINT, syscall.SIGTERM)
	err = app.Run(heaper.Server, ctx)
	app.Close()
	log.Printf("application finish: [%v]", err)

	if err != nil {
		syscall.Exit(1)
	}
}
