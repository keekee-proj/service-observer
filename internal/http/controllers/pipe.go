package controllers

import (
	"bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/model/pipeline"
	"context"
	"errors"
	"io/ioutil"
	"net/http"
)

func UpdatePipe(ctx context.Context, response http.ResponseWriter, request *http.Request) error {
	sName := ctx.Value("pipe").(string)
	data, err := ioutil.ReadAll(request.Body)
	if err != nil {
		return err
	}
	request.Body.Close()

	p, err := pipeline.ReadJsonPipe(data)
	if err != nil {
		return err
	}
	application := app.MustFromContext(ctx)
	repo := application.PipeRepository()
	p.Name = sName
	if err = repo.ValidateEdit(p, application.StageRepository()); err != nil {
		return err
	}
	if err = repo.Update(sName, p); err != nil {
		return err
	}
	response.Write(success)
	return nil
}

func Pipe(ctx context.Context, response http.ResponseWriter, _ *http.Request) error {
	pName := ctx.Value("pipe").(string)

	p, err := app.MustFromContext(ctx).PipeRepository().OneCached(pName)
	if err != nil {
		return err
	}
	return WriteResponse(response, pipeline.PipeToHttpStruct(p))
}

func Pipes(ctx context.Context, response http.ResponseWriter, request *http.Request) error {
	if err := request.ParseForm(); err != nil {
		return err
	}
	q, limit, offset := MakeHttpFilterQuery(request.Form, pipeline.PipeFilterQuery)
	ps, err := app.MustFromContext(ctx).PipeRepository().Find(q, limit, offset)
	if err != nil {
		return err
	}

	return WriteResponse(response, pipeline.PipeListToHttpStruct(ps))
}

func AddPipe(ctx context.Context, response http.ResponseWriter, request *http.Request) error {
	data, err := ioutil.ReadAll(request.Body)
	if err != nil {
		return err
	}
	request.Body.Close()

	if len(data) == 0 {
		return errors.New("request empty")
	}
	p, err := pipeline.ReadJsonPipe(data)
	if err != nil {
		return err
	}
	application := app.MustFromContext(ctx)
	repo := application.PipeRepository()
	if err = repo.ValidateAdd(p, application.StageRepository()); err != nil {
		return err
	}
	if err = repo.Add(p); err != nil {
		return err
	}
	response.Write(success)
	return nil
}

func DeletePipe(ctx context.Context, response http.ResponseWriter, _ *http.Request) error {
	sName := ctx.Value("pipe").(string)
	err := app.MustFromContext(ctx).PipeRepository().Delete(sName)
	if err != nil {
		return err
	}
	response.Write(success)
	return nil
}
