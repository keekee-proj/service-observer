package plugin

type SchemeAttribute struct {
	Type        string
	Require     bool
	Default     interface{}
	Description string
}

type PipeExtension interface {
	Exec(entityType string, entityId string, event string, subject string,
		data []byte, params map[string]interface{}) ([]byte, error)

	Name() string
	Description() string
	Scheme() map[string]*SchemeAttribute

	PluginVersion() int
	MinAppVersion() int
	MaxAppVersion() int
	MinPluginApiVersion() int
	MaxPluginApiVersion() int
}
