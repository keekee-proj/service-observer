package empty

import (
	"net/http"
)

type EmptyFilter struct {
}

func NewService() *EmptyFilter {
	return &EmptyFilter{}
}

func (i *EmptyFilter) CheckRequest(r *http.Request) error {
	return nil
}
