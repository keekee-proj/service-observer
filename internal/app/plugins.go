package app

import (
	"bitbucket.org/keekee-proj/service-observer/internal/config"
	plg "bitbucket.org/keekee-proj/service-observer/pkg/plugin"
	"io/ioutil"
	"log"
	"plugin"
	"strings"
)

func setupPlugins(cfg *config.Plugins) (map[string]plg.PipeExtension, error) {
	res := make(map[string]plg.PipeExtension)
	files, err := ioutil.ReadDir(cfg.Path)
	if err != nil {
		return res, err
	}

	for _, fi := range files {
		if fi.IsDir() {
			continue
		}

		if !strings.HasSuffix(fi.Name(), ".so") {
			continue
		}

		plug, err := plugin.Open(cfg.Path + "/" + fi.Name())
		if err != nil {
			log.Printf("failed load module [%s]: %s", fi.Name(), err)
			continue
		}

		mod, err := plug.Lookup("Plugin")
		if err != nil {
			log.Printf("failed load module [%s]: %s", fi.Name(), err)
			continue
		}
		p, ok := mod.(plg.PipeExtension)
		if !ok {
			log.Printf("extensions [%s] is not a PipeExtension", fi.Name())
			continue
		}

		if AppVersion > p.MaxAppVersion() || AppVersion < p.MinAppVersion() {
			log.Printf("extensions [%s] expect app version beetwin %d - %d", fi.Name(), p.MinAppVersion(), p.MaxAppVersion())
			continue
		}
		if PluginApiVersion > p.MaxPluginApiVersion() || PluginApiVersion < p.MinPluginApiVersion() {
			log.Printf("extensions [%s] expect plugin api version beetwin %d - %d", fi.Name(), p.MinAppVersion(), p.MaxAppVersion())
			continue
		}
		_, ok = res[p.Name()]
		if ok {
			log.Printf("extensions name [%s] already registered", p.Name())
			continue
		}
		res[p.Name()] = p
	}

	return res, nil
}
