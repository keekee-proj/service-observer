package duplicate_filter

import (
	"bitbucket.org/keekee-proj/service-observer/internal/http/duplicate_filter/empty"
	"bitbucket.org/keekee-proj/service-observer/internal/http/duplicate_filter/idempotency_key"
	"bitbucket.org/keekee-proj/service-observer/internal/http/duplicate_filter/md5"
	"bitbucket.org/keekee-proj/service-observer/internal/model/unduplicate_lock"
	"errors"
)

const (
	md5DuplicateFilter = "md5"
	idempotencyKey     = "idempotency-key"
)

func DuplicateService(cfgValue string, r *unduplicate_lock.Repository) (DuplicateFilterService, error) {
	switch cfgValue {
	case md5DuplicateFilter:
		return md5.NewService(r), nil
	case idempotencyKey:
		return idempotency_key.NewService(r), nil
	case "":
		return empty.NewService(), nil
	default:
		return nil, errors.New("undefined service name")
	}
}
