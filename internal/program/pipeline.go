package program

import (
	"bitbucket.org/keekee-proj/service-observer/internal/metric"
	"context"
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/streadway/amqp"

	application "bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/config"
	"bitbucket.org/keekee-proj/service-observer/internal/model/event_queue"
	"bitbucket.org/keekee-proj/service-observer/internal/model/exchange_message"
	"bitbucket.org/keekee-proj/service-observer/internal/model/pipeline"
	"bitbucket.org/keekee-proj/service-observer/internal/queue"
	"bitbucket.org/keekee-proj/service-observer/pkg/plugin"
)

const ctxModuleNameKey = "moduleName"

func NewPipelineContext(parent context.Context, moduleName string) context.Context {
	return context.WithValue(parent, ctxModuleNameKey, moduleName)
}

func moduleNameFromContext(ctx context.Context) string {
	return ctx.Value(ctxModuleNameKey).(string)
}

func Pipeline(app *application.App, parentCtx context.Context) error {
	ctx, cnlFn := context.WithCancel(parentCtx)
	moduleName := moduleNameFromContext(ctx)
	app.RegisterCancelFn(cnlFn)
	eqR := app.EventQueueRepository()
	stR := app.StageRepository()
	module, ok := app.PipePlugin(moduleName)
	if !ok {
		return fmt.Errorf("module [%s] not found", moduleName)
	}

	cfg := app.Config()
	stageQueueConf := pipeline.ModuleConfigQueue(moduleName, cfg.Amqp)

	channel, err := app.QueueProvider().MakeChannel()
	if err != nil {
		return err
	}

	q, err := app.QueueProvider().MakeQueue(
		stageQueueConf.Exchange,
		stageQueueConf.RouteKey,
		stageQueueConf.QueueName,
		60,
	)
	if err != nil {
		return err
	}
	defer q.Close()

	// ToDo: move to cfg
	ch := q.Consume(20)
	log.Printf("start consuming [%s]\n", moduleName)

	wg := &sync.WaitGroup{}
	makePull(
		10, // ToDo: move to cfg
		ch,
		module,
		wg,
		q,
		channel,
		eqR,
		stR,
		cfg.Amqp,
		app.Metric(),
	)
	<-ctx.Done()
	wg.Wait()

	return nil
}

func findStageByModule(moduleName string, exMessage *exchange_message.ExchangeMessage, stR *pipeline.StageRepository) (stage *pipeline.Stage, err error) {
	stage, err = stR.OneByNamesAndModule(exMessage.Pipeline.Stages, moduleName)
	return
}

func makePull(n int, chDelivery <-chan amqp.Delivery, module plugin.PipeExtension, wg *sync.WaitGroup, amqpQ queue.Queue,
	ch queue.Channel, eqR *event_queue.Repository, stR *pipeline.StageRepository, confAmqp *config.Amqp, metric metric.Metric) {
	for i := 0; i < n; i++ {
		go asyncProcessMessage(
			chDelivery,
			module,
			wg,
			amqpQ,
			ch,
			eqR,
			stR,
			confAmqp,
			metric,
		)
	}
}
func asyncProcessMessage(chDelivery <-chan amqp.Delivery, module plugin.PipeExtension, wg *sync.WaitGroup, amqpQ queue.Queue,
	ch queue.Channel, eqR *event_queue.Repository, stR *pipeline.StageRepository, confAmqp *config.Amqp, metric metric.Metric) {
	for msg := range chDelivery {
		wg.Add(1)
		metricIncr(metric, module.Name(), "msg.incomming")
		moment := time.Now()
		processPipeMessage(
			module,
			wg,
			&msg,
			amqpQ,
			ch,
			eqR,
			stR,
			confAmqp,
			metric,
		)
		metricIncr(metric, module.Name(), "msg.processed")
		metric.Timing(fmt.Sprintf("pipline.module.%s.msg.procesTime", module.Name()), time.Since(moment))
	}
}

func metricIncr(m metric.Metric, moduleName, metricName string) {
	m.Incr(fmt.Sprintf("pipline.module.%s.%s", moduleName, metricName))
}

func processPipeMessage(module plugin.PipeExtension, wg *sync.WaitGroup, msg *amqp.Delivery, amqpQ queue.Queue,
	ch queue.Channel, eqR *event_queue.Repository, stR *pipeline.StageRepository, confAmqp *config.Amqp, metric metric.Metric) {
	defer wg.Done()

	if string(msg.Body) == "" {
		return
	}

	exMessage, err := exchange_message.MakeFromJson(msg.Body)
	if err != nil {
		log.Printf("failed unmarshaling message: [%s]", err)
		metricIncr(metric, module.Name(), "errors.unmarshal")
		nack(msg)
		return
	}
	eq, err := eqR.One(exMessage.EventQueueID)
	if err != nil {
		log.Printf("failed loading event queue [%s]: [%s]", exMessage.EventQueueID.Hex(), err)
		metricIncr(metric, module.Name(), "errors.eventQueueNotFound")
		nack(msg)
		return
	}

	log.Printf("successfull loading event queue: [%s]", eq.Id.Hex())

	stage, err := findStageByModule(module.Name(), exMessage, stR)

	if err != nil {
		log.Printf("filed loading stage: [%s]", err)
		metricIncr(metric, module.Name(), "errors.stageNotFound")
		deferMessage(amqpQ, msg, err, eq, eqR, metric, module.Name())
		return
	}

	originalPipe := exMessage.Pipeline.OriginalPipe()
	pipe := pipeline.NewProcessPipe(originalPipe, stage.Name)

	nextStageName, err := pipe.NextStageName()
	if err != nil {
		log.Printf("failed found next stage: [%s]", err)
		metricIncr(metric, module.Name(), "errors.nextStageNotFound")
		deferMessage(amqpQ, msg, err, eq, eqR, metric, module.Name())
		return
	}

	result, err := module.Exec(
		eq.EntityType,
		exMessage.EntityId,
		exMessage.Event,
		exMessage.Subject,
		exMessage.Data,
		stage.Attributes,
	)

	if err != nil {
		log.Printf("failed execute module [%s]: [%s]", module.Name(), err)
		metricIncr(metric, module.Name(), "errors.executing")
		deferMessage(amqpQ, msg, err, eq, eqR, metric, module.Name())
		return
	}

	if nextStageName == "" {
		log.Printf("finish item [%s]", exMessage.EventQueueID.Hex())
		err = eqR.SetProcessed(eq)
		if err != nil {
			log.Printf("failed save eq[%s]: [%s]", exMessage.EventQueueID.Hex(), err)
			deferMessage(amqpQ, msg, err, eq, eqR, metric, module.Name())
			return
		}
		ack(msg)
		return
	}

	nextStage, err := stR.OneCached(nextStageName)
	if err != nil {
		deferMessage(amqpQ, msg, err, eq, eqR, metric, module.Name())
		return
	}

	stageQueueConf := pipeline.ModuleConfigQueue(nextStage.Module, confAmqp)

	eq.Data = result
	message, err := exchange_message.NewJson(eq, originalPipe)
	if err != nil {
		deferMessage(amqpQ, msg, err, eq, eqR, metric, module.Name())
		return
	}

	if err := ch.PublishTo(message, stageQueueConf.Exchange, stageQueueConf.RouteKey, 0); err != nil {
		metricIncr(metric, module.Name(), "errors.publish")
		log.Printf("failed send amqp message: [%s]", err)
		deferMessage(amqpQ, msg, err, eq, eqR, metric, module.Name())
	} else {
		log.Printf("item [%s] sent to [%s]", exMessage.EventQueueID.Hex(), stageQueueConf.RouteKey)
		ack(msg)
	}
}

func nack(msg *amqp.Delivery) {
	err := msg.Nack(false, true)

	if err != nil {
		log.Printf("error on nack: [%s]", err)
	} else {
		log.Printf("nack message")
	}
}

func ack(msg *amqp.Delivery) {
	err := msg.Ack(false)

	if err != nil {
		log.Printf("error on ack: [%s]", err)
	} else {
		log.Printf("ack message")
	}
}

func deferMessage(amqp queue.Queue, msg *amqp.Delivery, lastErr error, eq *event_queue.EventQueue, eqR *event_queue.Repository,
	metric metric.Metric, moduleName string) {
	metricIncr(metric, moduleName, "msg.defer")
	err := amqp.Defer(msg)
	if err == queue.ErrMaxAttemptOut {
		fail := eqR.SetError(eq, lastErr)
		if fail != nil {
			log.Printf(
				"failed saving err: [%s]: [%s], previous err: [%s]",
				eq.Id.Hex(),
				fail,
				lastErr,
			)
			nack(msg)
			return
		}
	} else if err != nil {
		log.Printf("failed defer message: [%s]", err)
		err = eqR.Defer(eq, time.Minute, lastErr)
		if err != nil {
			log.Printf("failed defer eq: [%s]", err)
			nack(msg)
			return
		}
	}
	ack(msg)
}
