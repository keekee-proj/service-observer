package tests

import (
	"bitbucket.org/keekee-proj/service-observer/internal/model/subject"
	"encoding/json"
	"testing"
)

func BenchmarkFilter(b *testing.B) {
	filter := subject.Filter{
		"param1": subject.Filter{"$in": []float64{float64(1), float64(2)}},
		"$or": subject.Filter{"param3": "x", "param2": "x"},
	}
	expectedJson := `{"param1": 1, "param2": 2, "param3": "x"}`
	data := make(map[string]interface{})

	if err := json.Unmarshal([]byte(expectedJson), &data); err != nil {
		b.Fail()
	}
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			filter.IsSuited(data)
		}

	})
}
