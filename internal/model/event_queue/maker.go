package event_queue

import (
	"bitbucket.org/keekee-proj/service-observer/internal/model/entity"
	"bitbucket.org/keekee-proj/service-observer/internal/model/subject"
	"encoding/json"
	"fmt"
)

func MakeEvents(event string, e *entity.Entity, sR *subject.Repository, eqR *Repository, data []byte) error {

	dataMap, err := getDataMap(data)
	if err != nil {
		return err
	}

	evId, err := getId(dataMap)
	if err != nil {
		return err
	}

	subj := sR.IterByEntityEvent(e.Type, event)
	defer subj.Close()

	s := &subject.Subject{}
	result := make([]*EventQueue, 0)
	for subj.Next(s) {
		if s.Filter == nil || s.Filter.IsSuited(dataMap) {
			result = append(result, New(evId, event, e, s, data))
		}
	}

	if len(result) == 0 {
		return nil
	}

	if err := eqR.Add(result...); err != nil {
		return err
	}

	return nil
}

func getDataMap(jsonBody []byte) (map[string]interface{}, error) {
	var result map[string]interface{}
	err := json.Unmarshal(jsonBody, &result)
	return result, err
}

func getId(data map[string]interface{}) (string, error) {
	id, ok := data["id"]

	if !ok {
		return "", fmt.Errorf("id not set")
	}

	return fmt.Sprintf("%v", id), nil
}
