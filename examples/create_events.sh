#!/usr/bin/env bash
curl -X POST http://localhost:8884/entity/test_entity/event/add -H "authKey: J0h12oeuoII**yiu12" -d '{"id": 0, "param1": "a"}' | jq
curl -X POST http://localhost:8884/entity/test_entity/event/add -H "authKey: J0h12oeuoII**yiu12" -d '{"id": 1, "param1": "b"}' | jq
curl -X POST http://localhost:8884/entity/test_entity/event/add -H "authKey: J0h12oeuoII**yiu12" -d '{"id": "x2", "param1": "c"}' | jq
