package event_queue

import (
	"bitbucket.org/keekee-proj/service-observer/internal/model/entity"
	"bitbucket.org/keekee-proj/service-observer/internal/model/subject"
	"gopkg.in/mgo.v2/bson"
	"time"
)

const NewEvent = "new"
const ProcessEvent = "process"
const ProcessedEvent = "processed"
const ErrorEvent = "error"

const MaxAttempts = 10

type EventQueue struct {
	Id             bson.ObjectId `bson:"_id"`
	EntityType     string        `bson:"entity_type"`
	EntityId       string        `bson:"entity_id"`
	Event          string        `bson:"event"`
	Subject        string        `bson:"subject"`
	Pipeline       string        `bson:"pipeline"`
	Status         string        `bson:"status"`
	Data           []byte        `bson:"data"`
	Attempts       int           `bson:"attempts"`
	CreatedAt      time.Time     `bson:"created_at"`
	StartProcessAt time.Time     `bson:"start_process_at"`
	ProcessedAt    time.Time     `bson:"processed_at"`
	LastAttempt    time.Time     `bson:"last_attempt"`
	NextAttempt    time.Time     `bson:"next_attempt"`
	Error          string        `bson:"error"`
}

func New(id string, event string, e *entity.Entity, s *subject.Subject, data []byte) *EventQueue {
	return &EventQueue{
		Id:          bson.NewObjectId(),
		EntityId:    id,
		EntityType:  e.Type,
		Event:       event,
		Subject:     s.Name,
		Pipeline:    s.Pipeline,
		Status:      NewEvent,
		Data:        data,
		Attempts:    0,
		CreatedAt:   time.Now(),
		NextAttempt: time.Now(),
	}
}
