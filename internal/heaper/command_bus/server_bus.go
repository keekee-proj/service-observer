package command_bus

import (
	"context"
	"log"
	"net"
)

type ScriptFn func(*Bus, <-chan *Command)
type ServerBus struct {
	ctx      context.Context
	lnr      *net.TCPListener
	bs       []*Bus
	scriptFn ScriptFn
}

func NewServerBus(ctx context.Context, addr string, scFn ScriptFn) (*ServerBus, error) {
	address, err := net.ResolveTCPAddr("tcp", addr)
	if err != nil {
		return nil, err
	}

	ln, err := net.ListenTCP("tcp", address)
	if err != nil {
		return nil, err
	}

	return &ServerBus{
		ctx,
		ln,
		make([]*Bus, 0),
		scFn,
	}, nil
}

func (sb *ServerBus) Listen() {
	working := true
	go func(ctx context.Context, w *bool) {
		<-ctx.Done()
		working = false
		sb.closeCons()
		sb.lnr.Close()
	}(sb.ctx, &working)

	for working {
		con, err := sb.lnr.AcceptTCP()
		if err != nil {
			log.Println(err)
			continue
		}
		b := NewBus(con, 1024)
		sb.addBus(b)
	}
}
func (sb *ServerBus) addBus(bs *Bus) () {
	sb.bs = append(sb.bs, bs)
	go listenFunc(sb, bs)
}

func (sb *ServerBus) closeCons() {
	for _, bs := range sb.bs {
		bs.Close(nil)
	}
}

func (sb *ServerBus) script(b *Bus, ch <-chan *Command) {
	sb.scriptFn(b, ch)
}

func (sb *ServerBus) closeBus(b *Bus) {
	var i int
	var bs *Bus
	for i, bs = range sb.bs {
		if bs == b {
			break
		}
	}
	bs.Close(nil)
	l := len(sb.bs) - 1
	buses := make([]*Bus, 0, l)
	if i > 0 {
		buses = append(buses, sb.bs[:i]...)
	}
	if i < l && l > 0 {
		buses = append(buses, sb.bs[i+1:]...)
	}
	sb.bs = buses
}
