package program

import (
	application "bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/http/routes"
	"bitbucket.org/keekee-proj/service-observer/internal/http/server"
	"context"
	"log"
	"net/http"
	"time"
)

func Http(app *application.App, ctx context.Context) error {
	cfg := app.Config()
	appCtx, cnlFn := context.WithCancel(application.MakeContext(ctx, app))
	app.RegisterCancelFn(cnlFn)

	h := server.New(appCtx, routes.List)
	s := makeServer(cfg.App.ListenAddress, h)
	go s.ListenAndServe()
	log.Printf("start listen [%s]", cfg.App.ListenAddress)

	h.Wait()

	return nil
}

func makeServer(addr string, h *server.Server) *http.Server {
	return &http.Server{
		Addr:        addr,
		Handler:     h,
		ReadTimeout: time.Duration(time.Second),
	}
}
