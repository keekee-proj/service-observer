package duplicate_filter

import "net/http"

type DuplicateFilterService interface {
	CheckRequest(r *http.Request) error
}
