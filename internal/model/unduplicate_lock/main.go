package unduplicate_lock

import "time"

type UnduplicateLock struct {
	Sum       string    `bson:"_id"`
	CreatedAt time.Time `bson:"created_at"`
}

func New(sum string) *UnduplicateLock {
	return &UnduplicateLock{sum, time.Now()}
}
