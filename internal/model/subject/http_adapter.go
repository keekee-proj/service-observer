package subject

import (
	"encoding/json"
	"fmt"
)

var FilterQuery = map[string]string{"_id": "name", "entity": "entity", "events": "events"}

type httpAdapter struct {
	Name     string   `json:"name"`
	Entity   string   `json:"entity"`
	Events   []string `json:"events"`
	Filter   Filter   `json:"filter"`
	Pipeline string   `json:"pipeline"`
}

func ToHttpStruct(e *Subject) *httpAdapter {
	return convert(e)
}

func convert(e *Subject) *httpAdapter {
	return &httpAdapter{
		Name:     e.Name,
		Entity:   e.Entity,
		Events:   e.Events,
		Filter:   e.Filter,
		Pipeline: e.Pipeline,
	}
}

func ListToHttpStruct(es []*Subject) []*httpAdapter {
	result := make([]*httpAdapter, 0, len(es))
	for _, e := range es {
		result = append(result, convert(e))
	}
	return result
}

func ReadJson(rowBody []byte) (*Subject, error) {
	adapter := &httpAdapter{}
	if err := json.Unmarshal(rowBody, adapter); err != nil {
		if e, ok := err.(*json.SyntaxError); ok {
			return nil, fmt.Errorf("syntax error at byte offset %d", e.Offset)
		}
		return nil, err
	}
	s := &Subject{
		Name:   adapter.Name,
		Entity: adapter.Entity,
		Events: adapter.Events,
		Filter: adapter.Filter,
		Pipeline: adapter.Pipeline,
	}

	return s, nil
}
