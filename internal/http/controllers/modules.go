package controllers

import (
	"bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/pkg/plugin"
	"context"
	"encoding/json"
	"net/http"
)

type scheme struct {
	Type        string      `json:"type"`
	Require     bool        `json:"required"`
	Default     interface{} `json:"default"`
	Description string
}

type httpAdapter struct {
	Name        string             `json:"name"`
	Description string             `json:"description"`
	Scheme      map[string]*scheme `json:"scheme"`
}

func makeRow(p plugin.PipeExtension) *httpAdapter {
	modScheme := p.Scheme()
	sh := make(map[string]*scheme, len(modScheme))

	for chName := range modScheme {
		sh[chName] = &scheme{
			modScheme[chName].Type,
			modScheme[chName].Require,
			modScheme[chName].Default,
			modScheme[chName].Description,
		}
	}

	return &httpAdapter{
		Name:        p.Name(),
		Description: p.Description(),
		Scheme:      sh,
	}
}

func Modules(ctx context.Context, response http.ResponseWriter, _ *http.Request) error {
	application := app.MustFromContext(ctx)
	result := make([]*httpAdapter, 0, len(application.PipePlugins()))
	for plugName := range application.PipePlugins() {
		plg, _ := application.PipePlugin(plugName)
		result = append(result, makeRow(plg))
	}

	rowResult, err := json.Marshal(result)

	if err != nil {
		return err
	}

	response.Write(rowResult)
	return nil
}
