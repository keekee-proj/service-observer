package storage

type QueryOption struct {
	Key   string
	Value interface{}
}

type Storage interface {
	All(table string, result interface{}) error
	OneId(table string, id interface{}, result interface{}) error
	Create(table string, doc ...interface{}) error
	RemoveId(table string, id interface{}) error
	UpdateId(table string, id interface{}, doc interface{}) error
	Iter(table string, query interface{}, sortF ...string) Iterator
	Find(table string, query interface{}, limit int, offset int, result interface{}) error
	Count(table string, query interface{}) (int, error)
	Upsert(collection string, selector, update interface{}) (id interface{}, updated int, err error)
	Close()
}

type Iterator interface {
	Close() error
	Next(result interface{}) bool
	Err() error
	Timeout() bool
	Done() bool
}
