package app

import (
	"context"
)

const ctxKey = "app"

func MakeContext(parent context.Context, app *App) context.Context {
	return context.WithValue(parent, ctxKey, app)
}

func MustFromContext(ctx context.Context) *App {
	return ctx.Value(ctxKey).(*App)
}
