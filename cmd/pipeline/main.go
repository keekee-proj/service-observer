package main

import (
	application "bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/program"
	"context"
	"flag"
	"log"
	"syscall"
)

var module = flag.String("module", "", "module of pipeline")

func main() {
	flag.Parse()
	cfg, err := application.ConfigLoad()
	if err != nil {
		log.Fatalf("failed loading config: [%s]", err)
	}

	if cfg.Pprof != nil && cfg.Pprof.Enabled {
		pprof, err := application.StartProfiling(cfg)
		fatalIfErr("failed starting profiling: [%s]", err)
		defer pprof.Stop()
	}

	app, err := application.Make(cfg)
	if err != nil {
		log.Fatalf("failed run app: [%s]", err)
	}
	ctx := context.Background()
	go application.CancelOnSignals(ctx, app, syscall.SIGINT, syscall.SIGTERM)
	err = app.Run(program.Pipeline, program.NewPipelineContext(ctx, *module))
	app.Close()
	log.Printf("application finish: [%v]", err)

	if err != nil {
		syscall.Exit(1)
	}
}

func fatalIfErr(format string, err error) {
	if err != nil {
		log.Fatalf(format, err)
	}
}
