package provider

import (
	"bitbucket.org/keekee-proj/service-observer/internal/config"
	"bitbucket.org/keekee-proj/service-observer/internal/queue"
	"bitbucket.org/keekee-proj/service-observer/internal/queue/rabbit"
)

func NewRabbitProvider(cfg *config.Amqp) *RabbitProvider {
	return &RabbitProvider{cfg}
}

type RabbitProvider struct {
	cfg *config.Amqp
}

func (f *RabbitProvider) MakeQueue(exchange, routeKey, queueName string, dlx int) (queue.Queue, error) {
	return rabbit.New(f.cfg.Dsn, exchange, routeKey, queueName, true, dlx)
}

func (f *RabbitProvider) MakeChannel() (queue.Channel, error) {
	return rabbit.MakeChannel(f.cfg.Dsn, true)
}
