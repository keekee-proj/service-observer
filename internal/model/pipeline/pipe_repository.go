package pipeline

import (
	"bitbucket.org/keekee-proj/service-observer/internal/storage"
	"bitbucket.org/keekee-proj/service-observer/internal/utils"
	"fmt"
	"gopkg.in/mgo.v2/bson"
)

type PipeRepository struct {
	storage    storage.Storage
	collection string
	cache      *utils.Cache
}

func NewPipeRepository(s storage.Storage, collection string, cache *utils.Cache) *PipeRepository {
	return &PipeRepository{s, collection, cache}
}

func (r *PipeRepository) One(name string) (*Pipe, error) {
	p := &Pipe{}
	if err := r.storage.OneId(r.collection, name, p); err != nil {
		return nil, err
	}

	return p, nil
}

func (r *PipeRepository) OneCached(name string) (p *Pipe, err error) {
	p, _ = r.cache.Get(name, func() interface{} {
		var res *Pipe
		res, err = r.One(name)
		if err != nil {
			return err
		}
		return res
	}).(*Pipe)
	return
}

func (r *PipeRepository) All() ([]*Pipe, error) {
	var res []*Pipe
	if err := r.storage.All(r.collection, &res); err != nil {
		return nil, err
	}

	return res, nil
}

func (r *PipeRepository) Update(name string, p *Pipe) error {
	if err := r.storage.UpdateId(r.collection, name, p); err != nil {
		return err
	}

	return nil
}

func (r *PipeRepository) Delete(name string) error {
	if err := r.storage.RemoveId(r.collection, name); err != nil {
		return err
	}

	return nil
}

func (r *PipeRepository) Add(p *Pipe) error {
	if err := r.storage.Create(r.collection, p); err != nil {
		return err
	}

	return nil
}

func (r *PipeRepository) ValidateAdd(p *Pipe, stageR *StageRepository) error {
	if err := r.ValidateEdit(p, stageR); err != nil {
		return err
	}
	if _, err := r.One(p.Name); err == nil {
		return fmt.Errorf("alredy exists")
	}

	return nil
}

func (r *PipeRepository) ValidateEdit(p *Pipe, stageR *StageRepository) error {
	if p.Name == "" {
		return fmt.Errorf("name not set")
	}
	if p.Stages == nil || len(p.Stages) == 0 {
		return fmt.Errorf("stages not set")
	}

	if validSt, err := correctStages(stageR, p.Stages); err != nil {
		return err
	} else if !validSt {
		return fmt.Errorf("incorrect stages")
	}

	return nil
}

func correctStages(stageR *StageRepository, stages []string) (bool, error) {
	q := bson.M{"_id": bson.M{"$in": stages}}

	if stagesInR, err := stageR.Find(q, 0, 0); err != nil {
		return false, err
	} else {
		return len(stages) == len(stagesInR), nil
	}
}
func (r *PipeRepository) Find(q interface{}, limit int, offset int) ([]*Pipe, error) {
	var res []*Pipe
	if err := r.storage.Find(r.collection, q, limit, offset, &res); err != nil {
		return nil, err
	}

	return res, nil
}
