package pipeline

import (
	"bitbucket.org/keekee-proj/service-observer/internal/storage"
	"bitbucket.org/keekee-proj/service-observer/internal/utils"
	"fmt"
)

type StageRepository struct {
	storage    storage.Storage
	collection string
	cache      *utils.Cache
}

func NewStageRepository(s storage.Storage, collection string, cache *utils.Cache) *StageRepository {
	return &StageRepository{s, collection, cache}
}

func (r *StageRepository) One(name string) (*Stage, error) {
	s := &Stage{}
	if err := r.storage.OneId(r.collection, name, s); err != nil {
		return nil, err
	}

	return s, nil
}

func (r *StageRepository) OneByNamesAndModule(names []string, moduleName string) (*Stage, error) {
	q := map[string]interface{}{"_id": map[string][]string{"$in": names}, "module": moduleName}
	stages, err := r.Find(q, 1, 0)
	if err != nil {
		return nil, err
	}

	if len(stages) < 1 {
		return nil, fmt.Errorf("not found")
	}

	return stages[0], nil
}

func (r *StageRepository) OneCached(name string) (stage *Stage, err error) {
	stage, _ = r.cache.Get(name, func() interface{} {
		var res *Stage
		res, err = r.One(name)
		if err != nil {
			return err
		}
		return res
	}).(*Stage)
	return
}

func (r *StageRepository) All() ([]*Stage, error) {
	var res []*Stage
	if err := r.storage.All(r.collection, &res); err != nil {
		return nil, err
	}

	return res, nil
}

func (r *StageRepository) Update(name string, s *Stage) error {
	if err := r.storage.UpdateId(r.collection, name, s); err != nil {
		return err
	}

	return nil
}

func (r *StageRepository) Delete(name string) error {
	if err := r.storage.RemoveId(r.collection, name); err != nil {
		return err
	}

	return nil
}

func (r *StageRepository) Add(e *Stage) error {
	if err := r.storage.Create(r.collection, e); err != nil {
		return err
	}

	return nil
}

func (r *StageRepository) ValidateAdd(s *Stage) error {
	if err := r.ValidateEdit(s); err != nil {
		return err
	}
	if _, err := r.One(s.Name); err == nil {
		return fmt.Errorf("alredy exists")
	}
	return nil
}

func (r *StageRepository) ValidateEdit(s *Stage) error {
	if s.Name == "" {
		return fmt.Errorf("name not set")
	}
	if s.Module == "" {
		return fmt.Errorf("module not set")
	}
	return nil
}

func (r *StageRepository) Find(q interface{}, limit int, offset int) ([]*Stage, error) {
	var res []*Stage
	if err := r.storage.Find(r.collection, q, limit, offset, &res); err != nil {
		return nil, err
	}

	return res, nil
}
