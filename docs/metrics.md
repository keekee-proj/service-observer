##Collecting metrics
This application include `statsd` client for integration with graphite for collecting metrics.

Configure `metric` part of config file like this:  
```yaml
metric:
  udp: metric:8125
  prefix: esb.
```

Where `udp` - address of udp-connection of `statsd`, `prefix` - prefix of metric path.

You can test collecting metrics, if you start application via docker-compose, [read more](https://bitbucket.org/keekee-proj/service-observer/src/master/docs/building.md).

But, you should configure grafana to using it with graphite - in grafana web interface select graphite source and set graphite http address in docker-compose environment, default, `http://metric:8080` 

Examples of metrics interface:
![response time](https://bytebucket.org/keekee-proj/service-observer/raw/83d8217bb917480cc1ca6228271436a26c6fdca9/docs/resp_time.png)


####More documents:
 * [about graphite image](https://github.com/graphite-project/docker-graphite-statsd) 
 * [about grafana image](http://docs.grafana.org/installation/docker/)