package main

import (
	application "bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/program"
	"context"
	"log"
	"sync"
	"syscall"
)

func main() {
	cfg, err := application.ConfigLoad()
	if err != nil {
		log.Fatalf("failed loading config: [%s]", err)
	}
	app, err := application.Make(cfg)
	if err != nil {
		log.Fatalf("failed run app: [%s]", err)
	}
	ctx := context.Background()
	wg := &sync.WaitGroup{}
	wg.Add(2)
	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		err := app.Run(program.Heaper, ctx)
		log.Printf("heaper finish: [%v]", err)
	}(wg)

	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		err := app.Run(program.Http, ctx)
		log.Printf("http finish: [%v]", err)
	}(wg)

	for pl := range app.PipePlugins() {
		wg.Add(1)

		go func(mdl string, wg *sync.WaitGroup) {
			defer wg.Done()
			err := app.Run(program.Pipeline, program.NewPipelineContext(ctx, mdl))
			log.Printf("finish module [%s]: [%v]", mdl, err)
		}(pl, wg)
	}
	application.CancelOnSignals(ctx, app, syscall.SIGINT, syscall.SIGTERM)
	wg.Wait()
	app.Close()
}
