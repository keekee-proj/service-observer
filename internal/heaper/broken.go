package heaper

import (
	"fmt"
	"strings"
	"time"
)

type brokeRow struct {
	id            string
	entity        string
	entityId      string
	event         string
	timeInProcess time.Duration
}

func printBrokenTable(tbl []*brokeRow) {
	fmt.Println("Maybe next rows are broken")
	fmt.Println("|" + strings.Repeat("-", 110+14) + "|")
	fmt.Println(tblHead())
	fmt.Println("|" + strings.Repeat("-", 110+14) + "|")
	for _, row := range tbl {
		fmt.Println(tblRow(row))
	}
	fmt.Println("|" + strings.Repeat("-", 110+14) + "|")
}

func tblHead() string {
	return fmt.Sprintf(
		"| %s | %s | %s | %s | %s |",
		tmlField("Id", 30),
		tmlField("Entity", 20),
		tmlField("Entity Id", 20),
		tmlField("Event", 20),
		tmlField("Waiting time", 20),
	)
}
func tblRow(row *brokeRow) string {
	return fmt.Sprintf(
		"| %s | %s | %s | %s | %s |",
		tmlField(row.id, 30),
		tmlField(row.entity, 20),
		tmlField(row.entityId, 20),
		tmlField(row.event, 20),
		tmlField(row.timeInProcess.String(), 20),
	)
}

func tmlField(v string, l int) string {
	if len(v) >= l {
		return v[0:l]
	}
	sp := int((l - len(v)) / 2)
	return fmt.Sprintf(
		"%s%s%s",
		strings.Repeat(" ", sp),
		v,
		strings.Repeat(" ", l-len(v)-sp),
	)
}
