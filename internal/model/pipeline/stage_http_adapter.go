package pipeline

import (
	"encoding/json"
)

var StageFilterQuery = map[string]string{"_id": "name", "module": "module"}

type StageHttpAdapter struct {
	Name       string                 `json:"name"`
	Module     string                 `json:"module"`
	Attributes map[string]interface{} `json:"attributes"`
}

func StageToHttpStruct(s *Stage) *StageHttpAdapter {
	return convertStage(s)
}

func convertStage(p *Stage) *StageHttpAdapter {
	return &StageHttpAdapter{
		Name:       p.Name,
		Module:     p.Module,
		Attributes: p.Attributes,
	}
}

func StageListToHttpStruct(stages []*Stage) []*StageHttpAdapter {
	result := make([]*StageHttpAdapter, 0, len(stages))
	for _, s := range stages {
		result = append(result, convertStage(s))
	}
	return result
}

func ReadJsonStage(rowBody []byte) (*Stage, error) {
	adapter := &StageHttpAdapter{}
	if err := json.Unmarshal(rowBody, adapter); err != nil {
		return nil, err
	}
	return &Stage{
		Name:       adapter.Name,
		Module:     adapter.Module,
		Attributes: adapter.Attributes,
	}, nil
}
