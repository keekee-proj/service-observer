package heaper

import (
	"bitbucket.org/keekee-proj/service-observer/internal/storage"
	"context"
	"gopkg.in/mgo.v2/bson"
	"reflect"
	"testing"
)

type testCaseFilter struct {
	name           string
	ctxValues      map[string]string
	expectedResult []*storage.QueryOption
}

func testFilterDataProvider() []*testCaseFilter {
	return []*testCaseFilter{
		{
			name:           "no filters",
			ctxValues:      make(map[string]string),
			expectedResult: make([]*storage.QueryOption, 0),
		},
		{
			name:      "set one filter",
			ctxValues: map[string]string{FilterEntity: "xxx"},
			expectedResult: []*storage.QueryOption{
				{FilterEntity, "xxx"},
			},
		},
		{
			name:      "set 3 filters",
			ctxValues: map[string]string{FilterEntity: "xxx", FilterEvent: "yyy", FilterSubject: "zzz"},
			expectedResult: []*storage.QueryOption{
				{FilterEntity, "xxx"},
				{FilterEvent, "yyy"},
				{FilterSubject, "zzz"},
			},
		},
		{
			name:      "set list filter",
			ctxValues: map[string]string{FilterEntity: "xxx, zzz", FilterEvent: "yyy"},
			expectedResult: []*storage.QueryOption{
				{FilterEntity, bson.M{"$in": []string{"xxx", "zzz"}}},
				{FilterEvent, "yyy"},
			},
		},
		{
			name:      "set exclude values",
			ctxValues: map[string]string{FilterEntity: "- xxx, zzz", FilterEvent: "yyy"},
			expectedResult: []*storage.QueryOption{
				{FilterEntity, bson.M{"$nin": []string{"xxx", "zzz"}}},
				{FilterEvent, "yyy"},
			},
		},
		{
			name:      "set exclude value",
			ctxValues: map[string]string{FilterEntity: "- xxx, zzz", FilterEvent: "- yyy"},
			expectedResult: []*storage.QueryOption{
				{FilterEntity, bson.M{"$nin": []string{"xxx", "zzz"}}},
				{FilterEvent, bson.M{"$ne": "yyy"}},
			},
		},
		{
			name:      "undefined value",
			ctxValues: map[string]string{"some_key": "zzz", FilterEvent: "-yyy"},
			expectedResult: []*storage.QueryOption{
				{FilterEvent, bson.M{"$ne": "yyy"}},
			},
		},
		{
			name:      "empty value",
			ctxValues: map[string]string{FilterEntity: "", FilterEvent: "-yyy"},
			expectedResult: []*storage.QueryOption{
				{FilterEvent, bson.M{"$ne": "yyy"}},
			},
		},
	}
}

func TestFilters(t *testing.T) {
	for _, testCase := range testFilterDataProvider() {
		ctx := context.Background()
		for key := range testCase.ctxValues {
			ctx = context.WithValue(ctx, CtxFilterKey(key), testCase.ctxValues[key])
		}
		t.Run(testCase.name, func(t *testing.T) {
			result := optionsFromContext(ctx)
			eq := reflect.DeepEqual(testCase.expectedResult, result)
			if !eq {
				t.Errorf("expect: [%v], recive: [%v]", testCase.expectedResult, result)
			}
		})
	}
}
