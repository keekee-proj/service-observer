#!/usr/bin/env bash
curl -X POST http://localhost:8884/entity -H "authKey: J0h12oeuoII**yiu12" -d '{"type": "test_entity", "events": ["add"]}' | jq
curl -X POST http://localhost:8884/stage -H "authKey: J0h12oeuoII**yiu12" -d '{"name": "send_http_test", "module": "sender_http", "attributes": {"url": "http://testServ:8081","method": "POST"}}' | jq
curl -X POST http://localhost:8884/stage -H "authKey: J0h12oeuoII**yiu12" -d '{"name": "stage_tpl_test", "module": "templater", "attributes": {"templateRaw": "ID: \"{{ .id }}\", PARAM1: {{ .param1 }}"}}' | jq
curl -X POST http://localhost:8884/pipe -H "authKey: J0h12oeuoII**yiu12" -d '{"name": "pipe_test","stages": ["stage_tpl_test", "send_http_test"]}' | jq
curl -X POST http://localhost:8884/subject -H "authKey: J0h12oeuoII**yiu12" -d '{"name": "subj_0","entity": "test_entity","events": ["add"], "pipeline": "pipe_test"}' | jq

curl http://localhost:8884/entities -H "authKey: J0h12oeuoII**yiu12" | jq
curl http://localhost:8884/stages -H "authKey: J0h12oeuoII**yiu12" | jq
curl http://localhost:8884/pipes -H "authKey: J0h12oeuoII**yiu12" | jq
curl http://localhost:8884/subjects -H "authKey: J0h12oeuoII**yiu12" | jq
