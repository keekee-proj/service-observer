package tests

import (
	"bitbucket.org/keekee-proj/service-observer/internal/model/subject"
	"encoding/json"
	"testing"
)

func TestFilter(t *testing.T) {
	t.Run("Test: simple", func(t *testing.T) {
		filter := subject.Filter{
			"param1": float64(1),
		}
		expectedJson := `{"param1": 1, "param2": 2}`
		data := make(map[string]interface{})

		if err := json.Unmarshal([]byte(expectedJson), &data); err != nil {
			t.Fail()
		}

		eq := filter.IsSuited(data)
		if !eq {
			t.Fail()
		}
	})
	t.Run("Test: combo", func(t *testing.T) {
		filter := subject.Filter{
			"param1": float64(1),
			"param2": float64(2),
		}
		expectedJson := `{"param1": 1, "param2": 2}`
		data := make(map[string]interface{})

		if err := json.Unmarshal([]byte(expectedJson), &data); err != nil {
			t.Fail()
		}

		eq := filter.IsSuited(data)
		if !eq {
			t.Fail()
		}
	})
}

func TestFilterOr(t *testing.T) {
	t.Run("OR: success", func(t *testing.T) {
		filter := subject.Filter{
			"@or": subject.Filter{"param1": float64(1), "param2": 3},
		}
		expectedJson := `{"param1": 1, "param2": 2}`
		data := make(map[string]interface{})

		if err := json.Unmarshal([]byte(expectedJson), &data); err != nil {
			t.Fail()
		}

		eq := filter.IsSuited(data)
		if !eq {
			t.Fail()
		}
	})
	t.Run("OR: negative", func(t *testing.T) {
		filter := subject.Filter{
			"@or": subject.Filter{"param1": float64(2), "param2": 3},
		}
		expectedJson := `{"param1": 1, "param2": 2}`
		data := make(map[string]interface{})

		if err := json.Unmarshal([]byte(expectedJson), &data); err != nil {
			t.Fail()
		}

		eq := filter.IsSuited(data)
		if eq {
			t.Fail()
		}
	})
}

func TestFilterAnd(t *testing.T) {
	t.Run("AND: success", func(t *testing.T) {
		filter := subject.Filter{
			"@and": subject.Filter{"param1": float64(1), "param2": float64(2)},
		}
		expectedJson := `{"param1": 1, "param2": 2}`
		data := make(map[string]interface{})

		if err := json.Unmarshal([]byte(expectedJson), &data); err != nil {
			t.Fail()
		}

		eq := filter.IsSuited(data)
		if !eq {
			t.Fail()
		}
	})
	t.Run("AND: negative", func(t *testing.T) {
		filter := subject.Filter{
			"@and": subject.Filter{"param1": float64(1), "param2": 3},
		}
		expectedJson := `{"param1": 1, "param2": 2}`
		data := make(map[string]interface{})

		if err := json.Unmarshal([]byte(expectedJson), &data); err != nil {
			t.Fail()
		}

		eq := filter.IsSuited(data)
		if eq {
			t.Fail()
		}
	})
}
func TestFilterList(t *testing.T) {
	t.Run("ListNames: in list", func(t *testing.T) {
		filter := subject.Filter{
			"param1": subject.Filter{"@in": []float64{float64(1), float64(2)}},
		}
		expectedJson := `{"param1": 1, "param2": 2}`
		data := make(map[string]interface{})

		if err := json.Unmarshal([]byte(expectedJson), &data); err != nil {
			t.Fail()
		}

		eq := filter.IsSuited(data)
		if !eq {
			t.Fail()
		}
	})
	t.Run("ListNames: not in list", func(t *testing.T) {
		filter := subject.Filter{
			"param1": subject.Filter{"@nin": []float64{float64(2), float64(3)}},
		}
		expectedJson := `{"param1": 1, "param2": 2}`
		data := make(map[string]interface{})

		if err := json.Unmarshal([]byte(expectedJson), &data); err != nil {
			t.Fail()
		}

		eq := filter.IsSuited(data)
		if !eq {
			t.Fail()
		}
	})
}
func TestFilterNe(t *testing.T) {
	t.Run("Ne: success", func(t *testing.T) {
		filter := subject.Filter{
			"param1": subject.Filter{"@ne": float64(2)},
		}
		expectedJson := `{"param1": 1, "param2": 2}`
		data := make(map[string]interface{})

		if err := json.Unmarshal([]byte(expectedJson), &data); err != nil {
			t.Fail()
		}

		eq := filter.IsSuited(data)
		if !eq {
			t.Fail()
		}
	})
	t.Run("Ne: negative", func(t *testing.T) {
		filter := subject.Filter{
			"param1": subject.Filter{"@ne": float64(1)},
		}
		expectedJson := `{"param1": 1, "param2": 2}`
		data := make(map[string]interface{})

		if err := json.Unmarshal([]byte(expectedJson), &data); err != nil {
			t.Fail()
		}

		eq := filter.IsSuited(data)
		if eq {
			t.Fail()
		}
	})
}
func TestFilterExists(t *testing.T) {
	t.Run("Exists: success", func(t *testing.T) {
		filter := subject.Filter{
			"@exists": "param1",
		}
		expectedJson := `{"param1": 1, "param2": 2}`
		data := make(map[string]interface{})

		if err := json.Unmarshal([]byte(expectedJson), &data); err != nil {
			t.Fail()
		}

		eq := filter.IsSuited(data)
		if !eq {
			t.Fail()
		}
	})
	t.Run("Exists: negative", func(t *testing.T) {
		filter := subject.Filter{
			"@exists": "param0",
		}
		expectedJson := `{"param1": 1, "param2": 2}`
		data := make(map[string]interface{})

		if err := json.Unmarshal([]byte(expectedJson), &data); err != nil {
			t.Fail()
		}

		eq := filter.IsSuited(data)
		if eq {
			t.Fail()
		}
	})
}
func TestFilterNotExists(t *testing.T) {
	t.Run("NotExists: success", func(t *testing.T) {
		filter := subject.Filter{
			"@notExists": "param0",
		}
		expectedJson := `{"param1": 1, "param2": 2}`
		data := make(map[string]interface{})

		if err := json.Unmarshal([]byte(expectedJson), &data); err != nil {
			t.Fail()
		}

		eq := filter.IsSuited(data)
		if !eq {
			t.Fail()
		}
	})
	t.Run("NotExists: negative", func(t *testing.T) {
		filter := subject.Filter{
			"@notExists": "param1",
		}
		expectedJson := `{"param1": 1, "param2": 2}`
		data := make(map[string]interface{})

		if err := json.Unmarshal([]byte(expectedJson), &data); err != nil {
			t.Fail()
		}

		eq := filter.IsSuited(data)
		if eq {
			t.Fail()
		}
	})
	t.Run("NotExists: incorrect0", func(t *testing.T) {
		filter := subject.Filter{
			"@exists": 123,
		}
		expectedJson := `{"param1": 1, "param2": 2}`
		data := make(map[string]interface{})

		if err := json.Unmarshal([]byte(expectedJson), &data); err != nil {
			t.Fail()
		}

		eq := filter.IsSuited(data)
		if eq {
			t.Fail()
		}
	})
	t.Run("NotExists: incorrect1", func(t *testing.T) {
		filter := subject.Filter{
			"@exists": []string{"param1"},
		}
		expectedJson := `{"param1": 1, "param2": 2}`
		data := make(map[string]interface{})

		if err := json.Unmarshal([]byte(expectedJson), &data); err != nil {
			t.Fail()
		}

		eq := filter.IsSuited(data)
		if eq {
			t.Fail()
		}
	})
}
