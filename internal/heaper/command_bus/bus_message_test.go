package command_bus

import (
	"testing"
)

type testCaseMakeMessage struct {
	name           string
	cmdName        string
	cmdOpts        []string
	expectedResult []byte
}

func testMakeMessageDataProvider() []*testCaseMakeMessage {
	return []*testCaseMakeMessage{
		{
			"case 1",
			"xxx",
			[]string{},
			[]byte{3, byte('x'), byte('x'), byte('x')},
		},
		{
			"case 2",
			"xxx",
			[]string{"go", "php"},
			[]byte{3, byte('x'), byte('x'), byte('x'), 2, byte('g'), byte('o'), 3, byte('p'), byte('h'), byte('p')},
		},
	}
}
func TestBus_MakeMessage(t *testing.T) {
	for _, testCase := range testMakeMessageDataProvider() {
		t.Run(testCase.name, func(t *testing.T) {
			msgRaw := makeMessage(NewCmd(testCase.cmdName, testCase.cmdOpts...))

			if len(msgRaw) != len(testCase.expectedResult) {
				t.Errorf("incorrect result: [%s], expect: [%s]", msgRaw, testCase.expectedResult)
				t.FailNow()
			}

			for i := 0; i < len(msgRaw); i++ {
				if msgRaw[i] != testCase.expectedResult[i] {
					t.Errorf("incorrect result: [%s], expect: [%s]", msgRaw, testCase.expectedResult)
				}
			}

			msg, err := readMessage(msgRaw)
			if err != nil {
				t.Errorf("error on readin msg: [%s]", err)
				t.FailNow()
			}

			if msg.Name != testCase.cmdName {
				t.Errorf("incorrect result: [%s], expect: [%s]", msg.Name, testCase.cmdName)
				t.FailNow()
			}

			for i, row := range msg.Opts {
				if row != testCase.cmdOpts[i] {
					t.Errorf("[s]:incorrect option: [%s], expect: [%s]", row, testCase.cmdOpts[i])
					t.Errorf("[b]:incorrect option: [%v], expect: [%v]", []byte(row), []byte(testCase.cmdOpts[i]))
					t.FailNow()
				}
			}
		})
	}
}
