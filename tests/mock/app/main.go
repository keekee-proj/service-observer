package app

import (
	"bitbucket.org/keekee-proj/service-observer/internal/utils"
	"context"
	"time"

	"bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/config"
	"bitbucket.org/keekee-proj/service-observer/internal/http/duplicate_filter/md5"
	"bitbucket.org/keekee-proj/service-observer/internal/metric/stdout"
	"bitbucket.org/keekee-proj/service-observer/internal/model/entity"
	"bitbucket.org/keekee-proj/service-observer/internal/model/event_queue"
	"bitbucket.org/keekee-proj/service-observer/internal/model/pipeline"
	"bitbucket.org/keekee-proj/service-observer/internal/model/subject"
	"bitbucket.org/keekee-proj/service-observer/internal/model/unduplicate_lock"
	"bitbucket.org/keekee-proj/service-observer/internal/queue"
	"bitbucket.org/keekee-proj/service-observer/internal/storage"
	plg "bitbucket.org/keekee-proj/service-observer/pkg/plugin"
)

func NewMockApp(cfg *config.Config, s storage.Storage, q queue.Provider) *app.App {
	m := stdout.New("")
	is := md5.NewService(unduplicate_lock.NewRepository(s, app.DuplicateCollection))

	cache := utils.NewCache(time.Second, m)
	gc := utils.NewGC(time.Second, time.Second, m)
	return app.New(
		cfg,
		s,
		q,
		stdout.New(""),
		entity.NewRepository(s, app.EntityCollection, cache),
		subject.NewRepository(s, app.SubjectCollection, cache),
		pipeline.NewStageRepository(s, app.StageCollection, cache),
		pipeline.NewPipeRepository(s, app.PipelineCollection, cache),
		event_queue.NewRepository(s, app.EventQueueCollection),
		is,
		false,
		make([]context.CancelFunc, 0),
		0,
		0,
		make(map[string]plg.PipeExtension),
		gc,
		true,
	)
}
