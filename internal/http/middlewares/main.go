package middlewares

import (
	application "bitbucket.org/keekee-proj/service-observer/internal/app"
	"context"
	"errors"
	"net/http"
)

func CheckKey(ctx context.Context, r *http.Request) error {
	app := application.MustFromContext(ctx)
	key := app.Config().App.AuthKey

	if r.Header.Get("authKey") != key {
		return errors.New("auth failed")
	}
	return nil
}

func CheckEntity(ctx context.Context, _ *http.Request) error {
	app := application.MustFromContext(ctx)
	e := ctx.Value("entity").(string)
	if e == "" {
		return errors.New("task is empty")
	}
	if _, err := app.EntityRepository().OneCached(e); err != nil {
		return err
	}
	return nil
}

func CheckSubject(ctx context.Context, _ *http.Request) error {
	app := application.MustFromContext(ctx)
	s := ctx.Value("subject").(string)
	if s == "" {
		return errors.New("task is empty")
	}
	if _, err := app.SubjectRepository().OneCached(s); err != nil {
		return err
	}
	return nil
}

func CheckStage(ctx context.Context, _ *http.Request) error {
	app := application.MustFromContext(ctx)
	s := ctx.Value("stage").(string)
	if s == "" {
		return errors.New("task is empty")
	}
	if _, err := app.StageRepository().OneCached(s); err != nil {
		return err
	}
	return nil
}

func CheckPipe(ctx context.Context, _ *http.Request) error {
	app := application.MustFromContext(ctx)
	s := ctx.Value("pipe").(string)
	if s == "" {
		return errors.New("task is empty")
	}
	if _, err := app.PipeRepository().OneCached(s); err != nil {
		return err
	}
	return nil
}

func DuplicateFilter(ctx context.Context, r *http.Request) error {
	app := application.MustFromContext(ctx)
	if app.Config().App.DuplicateFilter == "" {
		return nil
	}

	err := app.DuplicateService().CheckRequest(r)
	if err != nil {
		return err
	}

	return nil
}
