package server

import (
	router "bitbucket.org/keekee-proj/router/v2"
	"bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/metric"
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"
)

type Server struct {
	wg      sync.WaitGroup
	ctx     context.Context
	routes  *router.RouteList
	stopped bool
}

var err404 = []byte("{\"error\": \"resource not found\"}")
var errServClosed = []byte("Server closed")

func (s *Server) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	t := time.Now()
	if s.stopped {
		res.WriteHeader(http.StatusServiceUnavailable)
		_, _ = res.Write(errServClosed)
		writeMetrics(app.MustFromContext(s.ctx).Metric(), req, http.StatusServiceUnavailable, time.Since(t))
		return
	}

	s.wg.Add(1)
	defer s.wg.Done()
	if req.GetBody == nil {
		req.GetBody = func() (io.ReadCloser, error) {
			buf, err := ioutil.ReadAll(req.Body)
			if err != nil {
				return nil, err
			}

			req.Body = ioutil.NopCloser(bytes.NewReader(buf))
			return ioutil.NopCloser(bytes.NewReader(buf)), nil
		}
	}
	err := s.routes.Execute(s.ctx, res, req)
	if err != nil {
		if err == router.NotFoundErr {
			res.WriteHeader(404)
			_, _ = res.Write(err404)
			writeMetrics(app.MustFromContext(s.ctx).Metric(), req, http.StatusNotFound, time.Since(t))
			return
		}
		res.WriteHeader(http.StatusBadRequest)
		_, _ = res.Write(makeErrorResponse(err))
		writeMetrics(app.MustFromContext(s.ctx).Metric(), req, http.StatusBadRequest, time.Since(t))
		return
	}

	writeMetrics(app.MustFromContext(s.ctx).Metric(), req, http.StatusOK, time.Since(t))
}

func metricName(req *http.Request, respCode int) string {
	return fmt.Sprintf(
		"http.handler.%d.%s.%s",
		respCode,
		req.Method,
		strings.Trim(
			strings.Replace(req.URL.Path, "/", ".", -1),
			".",
		),
	)
}

func writeMetrics(m metric.Metric, req *http.Request, code int, duration time.Duration) {
	path := metricName(req, code)
	if code == http.StatusOK {
		m.Timing(path+".time", duration)
		m.Timing("http.process.time", duration)
	}
	m.Incr(path + ".count")
	m.Incr("http.process.count")
}

func makeErrorResponse(err error) []byte {
	return []byte(
		fmt.Sprintf("{\"error\": \"%s\"}", strings.Replace(err.Error(), "\"", "'", -1)),
	)
}

func New(ctx context.Context, routes *router.RouteList) *Server {
	return &Server{
		ctx:    ctx,
		routes: routes,
	}
}

func (s *Server) Wait() error {
	<-s.ctx.Done()
	s.stopped = true
	s.wg.Wait()
	return s.ctx.Err()
}
