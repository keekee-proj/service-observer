package event_queue

import (
	"bitbucket.org/keekee-proj/service-observer/internal/storage"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"time"
)

var ErrorMaxAttempts = fmt.Errorf("exceeded attempts")

type Repository struct {
	storage    storage.Storage
	collection string
}

func NewRepository(s storage.Storage, collection string) *Repository {
	return &Repository{s, collection}
}

func (r *Repository) One(id bson.ObjectId) (*EventQueue, error) {
	e := &EventQueue{}
	if err := r.storage.OneId(r.collection, id, e); err != nil {
		return nil, err
	}

	return e, nil
}

func (r *Repository) SetProcessed(e *EventQueue) error {
	e.Status = ProcessedEvent
	e.ProcessedAt = time.Now()
	if err := r.storage.UpdateId(r.collection, e.Id, e); err != nil {
		return err
	}

	return nil
}

func (r *Repository) SetProcess(e *EventQueue) error {
	e.Status = ProcessEvent
	e.StartProcessAt = time.Now()
	if err := r.storage.UpdateId(r.collection, e.Id, e); err != nil {
		return err
	}

	return nil
}

func (r *Repository) SetNew(e *EventQueue) error {
	e.Status = NewEvent
	if err := r.storage.UpdateId(r.collection, e.Id, e); err != nil {
		return err
	}

	return nil
}

func (r *Repository) SetError(e *EventQueue, err error) error {
	e.Status = ErrorEvent
	e.Error = err.Error()
	if err := r.storage.UpdateId(r.collection, e.Id, e); err != nil {
		return err
	}

	return nil
}

func (r *Repository) Defer(e *EventQueue, timeout time.Duration, err error) error {
	if e.Attempts >= MaxAttempts {
		return ErrorMaxAttempts
	}

	e.Attempts++
	e.LastAttempt = time.Now()
	e.NextAttempt = time.Now().Add(timeout)
	e.Error = ""
	if err != nil {
		e.Error = err.Error()
	}
	e.Status = NewEvent

	if err := r.storage.UpdateId(r.collection, e.Id, e); err != nil {
		return err
	}

	return nil
}

func (r *Repository) Add(e ...*EventQueue) error {
	docs := make([]interface{}, 0, len(e))
	for _, i := range e {
		docs = append(docs, i)
	}
	if err := r.storage.Create(r.collection, docs...); err != nil {
		return err
	}

	return nil
}

func (r *Repository) IterByStatus(status string, opts ...*storage.QueryOption) storage.Iterator {
	q := bson.M{
		"status": status,
	}
	for _, opt := range opts {
		q[opt.Key] = opt.Value
	}
	return r.storage.Iter(r.collection, q)
}

func (r *Repository) Count(opts ...*storage.QueryOption) (int, error) {
	q := bson.M{}
	for _, opt := range opts {
		q[opt.Key] = opt.Value
	}
	return r.storage.Count(r.collection, q)
}
