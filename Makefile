IMAGE=service-observer
PORT=8884
CONFIG=etc/conf.yaml
WORKDIR=$(shell pwd)
MODULE=sender

RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
$(eval $(RUN_ARGS):;@:)

COMP_FILES :=-f docker-compose.yaml -f docker-compose-testServ.yaml

ifdef COMPOSE_FILES
  COMP_FILES := ${COMPOSE_FILES}
endif

help:
	@echo
	@echo "  build		- build Docker images (base, build, app images)"
	@echo "  build.test	- build Docker images (with image of http-server for test)"
	@echo "  rebuild	- rebuild main images (only build and app images)"
	@echo "  run.http 	- start http service"
	@echo "  run.heaper 	- start heaper service"
	@echo "  run.pipeline 	- start pipeline service"
	@echo "  run.single 	- start all services in single instance"
	@echo "  build.attach	- attach to build container"
	@echo "  clean	        - remove main docker images (build and app)"
	@echo "  cleanAll      - remove all docker images"
	@echo "  mock.gen      - generate mock-files"
	@echo ""
	@echo "  ----------- docker-compose commands -----------"
	@echo "  up            - up all services (docker-compose up -d)"
	@echo "  down          - down all services"
	@echo "  start         - start a one service"
	@echo "  stop          - stop a one service"
	@echo "  ps            - list containers (with watching)"
	@echo "  log           - get log from container"
	@echo "  mongo         - attach to mongodb client"
	@echo

test:
	docker run --rm -it -v `pwd`:/go/src/bitbucket.org/keekee-proj/service-observer ${IMAGE}:base go test ./src/bitbucket.org/keekee-proj/service-observer/internal/...

build.base:
	docker build -t ${IMAGE}:base -f Dockerfile_base .

build: build.base
	docker build -t ${IMAGE}:build -f Dockerfile_build .
	docker build -t ${IMAGE} -f Dockerfile .

deps:
	GO111MODULE=on go mod vendor

build.test: build
	docker build -t ${IMAGE}:testServer -f Dockerfile_testServer .

rebuild: clean
	docker build -t ${IMAGE}:build -f Dockerfile_build .
	docker build -t ${IMAGE} -f Dockerfile .

fixrebuild:
	docker build -t ${IMAGE}:build -f Dockerfile_build .
	docker build -t ${IMAGE} -f Dockerfile .

build.attach:
	docker run --rm -it -v `pwd`/etc:/app/etc  ${IMAGE}:build sh

run.http:
	docker run --rm -it -e config=/app/$(CONFIG) -e app=http -p $(PORT):8883 -v `pwd`/etc:/app/etc ${IMAGE}
run.heaper:
	docker run --rm -it -e config=/app/$(CONFIG) -e app=heaper -v `pwd`/etc:/app/etc $(IMAGE)
run.pipeline:
	docker run --rm -it -v `pwd`/etc:/app/etc $(IMAGE) ./pipeline --config=/app/$(CONFIG) --module=${MODULE}
run.single:
	docker run --rm -it -e config=/app/$(CONFIG) -e app=single -p $(PORT):8883 -v `pwd`/etc:/app/etc ${IMAGE}

clean:
	docker rmi ${IMAGE}:build
	docker rmi ${IMAGE}

cleanAll:
	docker rmi ${IMAGE}:base
	docker rmi ${IMAGE}:build
	docker rmi ${IMAGE}
	docker rmi ${IMAGE}:testServer

up:
	docker-compose $(COMP_FILES) up -d
down:
	docker-compose $(COMP_FILES) down
	docker volume prune -f
ps:
	watch docker-compose $(COMP_FILES) ps
log:
	docker-compose $(COMP_FILES) logs -f $(RUN_ARGS)
attach:
	docker-compose $(COMP_FILES) exec $(RUN_ARGS) sh
start:
	docker-compose $(COMP_FILES) start $(RUN_ARGS)
stop:
	docker-compose $(COMP_FILES) stop $(RUN_ARGS)
mongo:
	docker-compose $(COMP_FILES) exec testServ mongo -host mongodb

mock.gen:
	mockgen -source=./internal/queue/interface.go -package=queue -destination=./tests/mock/queue/main.go
	mockgen -source=./vendor/github.com/streadway/amqp/delivery.go -package=amqp -destination=./tests/mock/amqp/main.go
	mockgen -source=./pkg/plugin/main.go -package=plugin -destination=./tests/mock/plugin/main.go
	mockgen -source=./internal/storage/interface.go -package=storage -destination=./tests/mock/storage/main.go

lint:
	golangci-lint run --config=lint_cfg.yaml