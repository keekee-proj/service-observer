package rabbit

import (
	i "bitbucket.org/keekee-proj/service-observer/internal/queue"
	"fmt"
	"github.com/streadway/amqp"
	"log"
)

type Queue struct {
	channel   *Channel
	exchange  string
	routeKey  string
	queueName string
	dlx       int
	closing   bool
}

func New(dsn string, exchange string, routeKey string, queueName string, keepAlive bool, dlx int) (i.Queue, error) {
	return makeQueue(dsn, exchange, routeKey, queueName, keepAlive, dlx)
}

func makeQueue(dsn string, exchange string, routeKey string, queueName string, keepAlive bool, dlx int) (*Queue, error) {
	ich, err := MakeChannel(dsn, keepAlive)
	if err != nil {
		return nil, err
	}
	ch := ich.(*Channel)

	log.Printf("initQueues for [%s]\n", queueName)

	err = initQueues(ch, exchange, routeKey, queueName, dlx)
	if err != nil {
		return nil, fmt.Errorf("amqp error: [%s]", err)
	}
	q := &Queue{
		ch,
		exchange,
		routeKey,
		queueName,
		dlx,
		false,
	}
	return q, nil
}

func (q *Queue) Close() error {
	q.closing = true
	return q.channel.Close()
}

func (q *Queue) Defer(msg *amqp.Delivery) error {
	if q.dlx < 1 {
		return fmt.Errorf("deferred queue not available")
	}
	attempt := int16(0)
	hAttempt, ok := msg.Headers["attempt"]

	if ok {
		attempt, _ = hAttempt.(int16)
		attempt++
	}

	if attempt > i.MaxAttemptValue {
		return i.ErrMaxAttemptOut

	}
	return q.channel.PublishTo(msg.Body, q.exchange, deferredRouteKey(q.routeKey), attempt)
}

func initQueues(ch *Channel, exchange string, routeKey string, queueName string, dlx int) error {
	err := ch.ExchangeDeclare(
		exchange,
		amqp.ExchangeDirect,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}
	log.Printf("declare exchange [%s]", exchange)
	_, err = ch.QueueDeclare(
		queueName,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}
	log.Printf("declare queue [%s]", queueName)
	err = ch.QueueBind(
		queueName,
		routeKey,
		exchange,
		false,
		nil,
	)
	if err != nil {
		return err
	}
	log.Printf("bind ex[%s] r[%s] to q[%s]", exchange, routeKey, queueName)

	if dlx < 1 {
		return nil
	}

	routeDlx := deferredRouteKey(routeKey)
	queueDlx := deferredQueueName(queueName)

	args := map[string]interface{}{
		"x-dead-letter-exchange":    exchange,
		"x-dead-letter-routing-key": routeKey,
		"x-message-ttl":             int32(dlx * 1000),
	}
	_, err = ch.QueueDeclare(
		queueDlx,
		true,
		false,
		false,
		false,
		args,
	)
	if err != nil {
		return err
	}
	log.Printf("declare queue [%s]", queueDlx)

	err = ch.QueueBind(
		queueDlx,
		routeDlx,
		exchange,
		false,
		nil,
	)
	if err != nil {
		return err
	}
	log.Printf("bind ex[%s] r[%s] to q[%s]", exchange, routeDlx, queueDlx)

	return nil
}

func deferredRouteKey(routeKey string) string {
	return fmt.Sprintf("%s_deferred", routeKey)
}

func deferredQueueName(queueName string) string {
	return fmt.Sprintf("%s_deferred", queueName)
}

func (q *Queue) Consume(prefetchCount int) <-chan amqp.Delivery {
	ch := make(chan amqp.Delivery, prefetchCount)
	go watchConsumeQueue(q, ch)
	return ch
}

func watchConsumeQueue(q *Queue, targetCh chan<- amqp.Delivery) {
	queueCh, err := makeConsumeQueue(q, cap(targetCh))
	if err != nil {
		log.Fatalf("consume error: [%s]", err)
	}

	for !q.closing {
		msg, ok := <-queueCh
		if !ok {
			queueCh = waitMakingConsumeQueue(q, cap(targetCh))
			continue
		}
		targetCh <- msg
	}
	close(targetCh)
	log.Println("stop consume watching")
}

func waitMakingConsumeQueue(q *Queue, prefetchCount int) <-chan amqp.Delivery {
	for !q.closing {
		ch, err := makeConsumeQueue(q, prefetchCount)
		if err == nil {
			log.Println("created new consume channel")
			return ch
		}
	}
	return nil
}

func makeConsumeQueue(q *Queue, prefetchCount int) (<-chan amqp.Delivery, error) {
	err := q.channel.Qos(prefetchCount, 0, false)
	if err != nil {
		return nil, err
	}

	ch, err := q.channel.Consume(
		q.queueName,
		"",
		false,
		false,
		false,
		false,
		make(amqp.Table),
	)
	if err != nil {
		return nil, err
	}
	return ch, nil
}
