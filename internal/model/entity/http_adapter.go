package entity

import (
	"encoding/json"
)

var FilterQuery = map[string]string{"_id": "type", "events": "events"}

type httpAdapter struct {
	Type   string   `json:"type"`
	Events []string `json:"events"`
}

func ToHttpStruct(e *Entity) *httpAdapter {
	return convert(e)
}

func convert(e *Entity) *httpAdapter {
	return &httpAdapter{
		Type:   e.Type,
		Events: e.Events,
	}
}

func ListToHttpStruct(es []*Entity) []*httpAdapter {
	result := make([]*httpAdapter, 0, len(es))
	for _, e := range es {
		result = append(result, convert(e))
	}
	return result
}

func ReadJson(rowBody []byte) (*Entity, error) {
	adapter := &httpAdapter{}
	if err := json.Unmarshal(rowBody, adapter); err != nil {
		return nil, err
	}
	return &Entity{
		Type:   adapter.Type,
		Events: adapter.Events,
	}, nil
}
