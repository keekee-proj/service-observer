FROM alpine:latest

WORKDIR /app

COPY --from=service-observer:build /app /app

EXPOSE 8883

CMD ./${app} --config=${config}