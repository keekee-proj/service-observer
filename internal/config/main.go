package config

import (
	"gopkg.in/yaml.v2"
	"time"
)

type App struct {
	ListenAddress      string `yaml:"listen_address"`
	AuthKey            string `yaml:"authKey"`
	DuplicateFilter    string `yaml:"duplicateFilter"`
	Name               string `yaml:"-"` // From ENV
	CreatingEventsType string `yaml:"creating_events_type"`
}

type Storage struct {
	Dsn  string `yaml:"dsn"`
	Name string `yaml:"name"`
}

type Cache struct {
	Ttl time.Duration `yaml:"ttl"`
}

type Amqp struct {
	Dsn       string `yaml:"dsn"`
	Exchange  string `yaml:"exchange"`
	RouteKey  string `yaml:"route_key"`
	QueueName string `yaml:"queue_name"`
}

type Metric struct {
	Address string `yaml:"udp"`
	Prefix  string `yaml:"prefix"`
}

type HeaperCluster struct {
	Address  string `yaml:"address"`
	Strategy string `yaml:"strategy"`
}

type Plugins struct {
	Path string `yaml:"path"`
}

type Pprof struct {
	Enabled bool   `yaml:"enabled"`
	Path    string `yaml:"out_dir"`
	Mode    string `yaml:"mode"`
}

type Config struct {
	App      *App           `yaml:"app"`
	Db       *Storage       `yaml:"db"`
	Amqp     *Amqp          `yaml:"exchange_amqp"`
	Cache    *Cache         `yaml:"cache"`
	Metric   *Metric        `yaml:"metric"`
	HCluster *HeaperCluster `yaml:"heaper_cluster"`
	Plugins  *Plugins       `yaml:"plugins"`
	Pprof    *Pprof         `yaml:"pprof"`
}

func ReadFromFile(file []byte) (cfg *Config, err error) {
	cfg = &Config{}
	err = yaml.Unmarshal(file, cfg)
	return
}
