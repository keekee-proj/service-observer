package heaper

import (
	"bitbucket.org/keekee-proj/service-observer/internal/model/subject"
	"fmt"
	"time"
)

type subjectReshaper struct {
	sR *subject.Repository
}

func NewSubjectReshaper(sR *subject.Repository) *subjectReshaper {
	return &subjectReshaper{sR: sR}
}

func (sr *subjectReshaper) needReshape(sls *slList) bool {
	if len(sls.lst) == 0 {
		return false
	}

	subjectCtn, err := sr.sR.Count()
	if err != nil || subjectCtn == 0 {
		return false
	}

	var cliSubjCtn int

	for _, ctn := range sls.wt {
		cliSubjCtn += ctn
	}

	return cliSubjCtn != subjectCtn
}

func (sr *subjectReshaper) reshape(sls *slList) error {
	sls.Lock()
	defer sls.Unlock()

	argIndex := 2

	if time.Since(sls.shapeTime) < time.Minute {
		return sls.shapeResult
	}

	if len(sls.lst) == 0 {
		sls.shapeResult = fmt.Errorf("no slaves")
		return sls.shapeResult
	}

	subjectCtn, err := sr.sR.Count()
	if err != nil {
		sls.shapeResult = err
		return sls.shapeResult
	}
	if subjectCtn == 0 {
		sls.shapeResult = fmt.Errorf("no subjects")
		return sls.shapeResult
	}

	subByCli := float64(len(sls.lst) / subjectCtn)
	if subByCli == 0 {
		subByCli = 1
	}

	for _, slv := range sls.lst {
		slv.weight = 0
		slv.commandArgs = make([]string, 3)
	}

	var (
		lastSlave *slave
		subj      subject.Subject
	)
	iter := sr.sR.IterAll()
	wt := make(weightTable)
	for iter.Next(&subj) {
		if lastSlave == nil {
			lastSlave = sls.lst[0]
		}

		if lastSlave.weight >= subByCli {
			lastSlave = getMinimalSlave(sls)
		}

		lastSlave.weight += 1
		format := "%s,%s"
		if lastSlave.commandArgs[argIndex] == "" {
			format = "%s%s"
		}
		lastSlave.commandArgs[argIndex] = fmt.Sprintf(format, lastSlave.commandArgs[argIndex], subj.Name)
		wt[lastSlave.String()] = int(lastSlave.weight)
	}

	sls.wt = wt
	sls.dirty = false
	return nil
}
