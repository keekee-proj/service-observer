package main

import (
	"flag"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"sync"
	"sync/atomic"
	"time"
)

type testServer struct {
	sync.RWMutex
	firstMsg time.Time
	LastMsg  time.Time
	cnt      int64
}

func (s *testServer) reset() {
	s.Lock()
	defer s.Unlock()

	dt := s.LastMsg.Sub(s.firstMsg)
	sec := dt / time.Second
	var rps int64
	if sec > 0 {
		rps = s.cnt / int64(sec)
	}
	log.Println("reset counter")
	log.Printf("message count: %d", s.cnt)
	log.Printf("duration: %d sec + [%s]", sec, (dt % time.Second).String())
	log.Printf("rps: %d", rps)
	s.cnt = 0
}

func (s *testServer) incr() {
	s.RLock()
	defer s.RUnlock()
	i := atomic.AddInt64(&s.cnt, 1)
	s.LastMsg = time.Now()
	if i == 1 {
		s.firstMsg = time.Now()
		go func(s *testServer) {
			for {
				time.Sleep(time.Second)
				if time.Since(s.LastMsg) > time.Second*10 {
					break
				}
			}
			s.reset()
		}(s)
	}
}
func (s *testServer) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	s.incr()
	log.Printf("url: %s\n", req.URL.Path)
	log.Printf("headers:")
	for key, h := range req.Header {
		log.Printf("[%s]: %v", key, h)
	}
	result, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Printf("error: [%s]", err)
		res.WriteHeader(422)
		res.Write([]byte(err.Error()))
		return
	}
	log.Printf("body:\n%s", result)
	if *withErr && rand.Intn(2) != 1 {
		log.Println("return err")
		res.WriteHeader(400)
		res.Write([]byte("some error"))
		return
	}

	res.WriteHeader(200)
	res.Write([]byte("ok"))
	log.Println("return ok")
}

var (
	addr    = flag.String("addr", "", "listen http address")
	withErr = flag.Bool("we", false, "may return an error")
)

func main() {
	flag.Parse()
	if *addr == "" {
		log.Fatal("address not set")
	}

	http.ListenAndServe(*addr, &testServer{})
}
