package utils

import (
	"bitbucket.org/keekee-proj/service-observer/internal/metric"
	"time"
)

type GC struct {
	metric        metric.Metric
	cc            []*Cache
	sleepTime     time.Duration
	stopWorldTime time.Duration
	enabled       bool
}

func NewGC(stopWorldTime, sleepTime time.Duration, metric metric.Metric) *GC {
	return &GC{stopWorldTime: stopWorldTime, sleepTime: sleepTime, metric: metric}
}

func (gc *GC) processCache(c *Cache, moment time.Time) {
	c.Lock()
	var n, skip, i int
	for key, v := range c.data {
		i++
		if i%10 == 0 {
			if time.Since(moment) > gc.stopWorldTime {
				break
			}
		}

		if v.expire.After(moment) {
			skip++
			continue
		}

		delete(c.data, key)
		n++
	}
	c.Unlock()

	if n > 0 {
		gc.metric.Add("cache_gc.items.remove", n)
	}
	if skip > 0 {
		gc.metric.Add("cache_gc.items.skip", skip)
	}
}

func (gc *GC) AddCache(cs ...*Cache) {
	gc.cc = append(gc.cc, cs...)
}

func (gc *GC) Disable() {
	gc.enabled = false
}

func (gc *GC) Run() {
	gc.enabled = true
	for gc.enabled {
		time.Sleep(gc.sleepTime)
		if !gc.enabled {
			break
		}

		moment := time.Now()
		for _, c := range gc.cc {
			gc.processCache(c, moment)
		}
	}
}
