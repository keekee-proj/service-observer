package heaper

import (
	application "bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/heaper/command_bus"
	"context"
	"errors"
	"fmt"
	"log"
	"math"
	"sync"
	"time"
)

var ErrNoEntities = errors.New("no entities")

type weightTable map[string]int

type slave struct {
	commandArgs []string
	weight      float64
	bus         *command_bus.Bus
	index       int
}

type slList struct {
	*sync.Mutex
	lst         []*slave
	wt          weightTable
	eqCnt       int
	shapeTime   time.Time
	shapeResult error
	dirty       bool
	iseq        int
	rsh         reshaper
}

func (sls *slList) reshape() error {
	return sls.rsh.reshape(sls)
}
func (sls *slList) needReshape() bool {
	return sls.rsh.needReshape(sls)
}

func (sls *slList) actualWeight() float64 {
	sls.Lock()
	defer sls.Unlock()
	s := float64(0)
	for _, sl := range sls.lst {
		s += sl.weight
	}

	return s
}
func (s *slave) String() string {
	return fmt.Sprintf("client [%s][%d]", s.bus.String(), s.index)
}

func (sls *slList) registerSlave(s *slave) {
	sls.Lock()
	defer sls.Unlock()
	sls.lst = append(sls.lst, s)
	sls.dirty = true
	s.index = sls.iseq
	sls.iseq++
}

func (sls *slList) unregisterSlave(s *slave) {
	sls.Lock()
	defer sls.Unlock()
	for i := range sls.lst {
		if sls.lst[i] == s {
			sls.lst = append(sls.lst[:i], sls.lst[i+1:]...)
			break
		}
	}
	sls.dirty = true
}

func newSlList(rsh reshaper) *slList {
	return &slList{
		&sync.Mutex{},
		make([]*slave, 0),
		nil,
		0,
		time.Time{},
		nil,
		false,
		0,
		rsh,
	}
}

func newSlave(b *command_bus.Bus) *slave {
	return &slave{weight: 0, bus: b, index: -1}
}

func getMinimalSlave(sls *slList) *slave {
	minI := -1
	minV := math.MaxFloat64
	for i, s := range sls.lst {
		if s.weight < minV {
			minI = i
			minV = s.weight
		}
	}

	if minI == -1 {
		return nil
	}

	return sls.lst[minI]
}

func Server(app *application.App, ctx context.Context) error {
	sll := newSlList(reshaperByType(app.Config().HCluster.Strategy, app))
	iterRunning := new(bool)

	script := func(bus *command_bus.Bus, commands <-chan *command_bus.Command) {
		log.Println("start server script")
		client := newSlave(bus)
		sll.registerSlave(client)
		defer func() {
			sll.unregisterSlave(client)
		}()

		<-commands
		if err := bus.Call(command_bus.OkCmd); err != nil {
			log.Println(err)
			return
		}

		if !bus.Alive() {
			log.Println("bus is died")
			return
		}

	waitJob:
		for *iterRunning {
			time.Sleep(time.Second)
		}
		for !*iterRunning {
			time.Sleep(time.Second)
		}

		if !bus.Alive() {
			log.Println("bus is died after wait")
			return
		}

		if client.weight == 0 {
			log.Println("client have zero weight")
			if err := bus.Call(command_bus.WaitCmd); err != nil {
				log.Printf("receive err on call cmd wait: [%s]\n", err)
				return
			}
			log.Printf("[%s] have to wait a job\n", client)
			goto waitJob
		}

		log.Printf("call run cmd: [%v]\n", client.commandArgs)
		if err := bus.Call(command_bus.NewRunCmd(client.commandArgs...)); err != nil {
			return
		}

		lastPing := new(time.Time)
		pingTik := time.NewTicker(time.Second)
		go watchPing(bus, pingTik.C, time.Second*5, lastPing)
		for cmd := range commands {
			if cmd.Name == command_bus.PingCmd.Name {
				*lastPing = time.Now()
				bus.Call(command_bus.PongCmd)
			}
		}
		pingTik.Stop()
	}
	servCtx, cnlFn := context.WithCancel(ctx)
	app.RegisterCancelFn(cnlFn)
	serv, _ := command_bus.NewServerBus(servCtx, app.Config().HCluster.Address, script)

	*iterRunning = false

	go asyncReshape(app, sll, iterRunning)
	serv.Listen()
	log.Println("finish")
	return nil
}

func watchPing(bus *command_bus.Bus, ch <-chan time.Time, d time.Duration, lastPing *time.Time) {
	for range ch {
		if time.Since(*lastPing) > d {
			bus.Close(nil)
			break
		}
	}
}

func asyncReshape(app *application.App, slaveList *slList, iterRunning *bool) {
	for app.IsWorking() {
		if !slaveList.needReshape() {
			time.Sleep(time.Second * 10)
			continue
		}

		if *iterRunning {
			*iterRunning = false
			for _, slv := range slaveList.lst {
				if slv.weight > 0 {
					slv.bus.Close(errors.New("need to close the bus for reshaping"))
				}
			}
		}

		for slaveList.actualWeight() != 0 {
			time.Sleep(time.Second)
		}
		time.Sleep(time.Second * 5)
		log.Println("reshaping")
		err := slaveList.reshape()
		if err != nil {
			log.Printf("error on reshaping: [%s]", err)
		} else {
			log.Println("successful reshaping")
			*iterRunning = true
		}
	}
}
