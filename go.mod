module bitbucket.org/keekee-proj/service-observer

require (
	bitbucket.org/keekee-proj/router/v2 v2.3.0
	bitbucket.org/keekee-proj/service-observer/pkg/plugin v0.0.0
	github.com/alexcesaro/statsd v2.0.0+incompatible
	github.com/golang/mock v1.2.0
	github.com/pkg/profile v1.3.0
	github.com/streadway/amqp v0.0.0-20180315184602-8e4aba63da9f
	gopkg.in/alexcesaro/statsd.v2 v2.0.0 // indirect
	gopkg.in/mgo.v2 v2.0.0-20160818020120-3f83fa500528
	gopkg.in/yaml.v2 v2.1.1
)

replace bitbucket.org/keekee-proj/service-observer/pkg/plugin => ./pkg/plugin
