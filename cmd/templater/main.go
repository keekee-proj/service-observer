package main

import (
	"bitbucket.org/keekee-proj/service-observer/pkg/plugin"
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"text/template"
)

const paramTemplateRaw = "templateRaw"

type templater struct{}

func (T *templater) Exec(entityType string, entityId string, event string, subject string,
	data []byte, params map[string]interface{}) ([]byte, error) {
	var tpl string

	if tplRaw, ok := params[paramTemplateRaw]; ok {
		tpl, ok = tplRaw.(string)
		if !ok {
			return data, fmt.Errorf("template is not string")
		}
	}

	if tpl == "" {
		return data, fmt.Errorf("template is empty")
	}
	var dataParams map[string]interface{}

	if err := json.Unmarshal(data, &dataParams); err != nil {
		return data, err
	}

	parsedtpl, err := template.New("").Parse(tpl)

	if err != nil {
		return data, err
	}

	var b bytes.Buffer
	writer := bufio.NewWriter(&b)
	parsedtpl.Execute(writer, dataParams)

	writer.Flush()

	return b.Bytes(), nil
}

func (T *templater) Name() string {
	return "templater"
}
func (T *templater) Description() string {
	return "make result using GO templates"
}

func (T *templater) Scheme() map[string]*plugin.SchemeAttribute {
	return map[string]*plugin.SchemeAttribute{
		paramTemplateRaw: {
			Type:    "string",
			Require: true,
			Default: nil,
			Description: `template body. For example:
Id: "{{ .id }}"
ParamX: {{ .params.X }}`,
		},
	}
}
func (T *templater) PluginVersion() int {
	return 0
}
func (T *templater) MinAppVersion() int {
	return 0
}
func (T *templater) MaxAppVersion() int {
	return 0
}
func (T *templater) MinPluginApiVersion() int {
	return 0
}

func (T *templater) MaxPluginApiVersion() int {
	return 0
}

var Plugin templater
