package stdout

import (
	"bitbucket.org/keekee-proj/service-observer/internal/metric"
	"fmt"
	"os"
	"sync"
	"time"
)

const bufSize = 2048

type Metric struct {
	prefix string
	mx     *sync.Mutex
	buf    []byte
}

func New(prefix string) metric.Metric {
	return &Metric{
		prefix,
		new(sync.Mutex),
		make([]byte, 0, bufSize),
	}
}

func (s *Metric) Close() {
	s.mx.Lock()
	defer s.mx.Unlock()
	s.flush()
}

func (s *Metric) Incr(metricName string) {
	s.Add(metricName, 1)
}

func (s *Metric) Add(metricName string, count int) {
	s.write(fmt.Sprintf("metric: [%s] - [+%d]", metricName, count))
}

func (s *Metric) Value(metricName string, val int) {
	s.write(fmt.Sprintf("metric: [%s] - [%d]", metricName, val))
}

func (s *Metric) Timing(metricName string, duration time.Duration) {
	s.write(fmt.Sprintf("metric: [%s] - [%d]ms", metricName, durationToMills(duration)))
}
func durationToMills(duration time.Duration) int {
	return int(duration / time.Millisecond)
}

func (s *Metric) write(str string) {
	s.mx.Lock()
	defer s.mx.Unlock()

	v := []byte(str + "\n")
	if len(v)+len(s.buf) > bufSize {
		s.flush()
	}
	s.buf = append(s.buf, v...)
}

func (s *Metric) flush() {
	os.Stdout.Write(s.buf)
	s.buf = make([]byte, 0, bufSize)
}
