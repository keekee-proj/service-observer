package idempotency_key

import (
	"errors"
	"fmt"
	"net/http"

	"bitbucket.org/keekee-proj/service-observer/internal/model/unduplicate_lock"
)

const IdempotentHeaderKey = "idempotency-key"

type IdempotentFilter struct {
	r *unduplicate_lock.Repository
}

func NewService(r *unduplicate_lock.Repository) *IdempotentFilter {
	return &IdempotentFilter{r}
}

func (i *IdempotentFilter) checkKey(key string) error {
	model := unduplicate_lock.New(key)
	op, err := i.r.AddOrTouch(model)
	if err != nil {
		return err
	}

	if op == unduplicate_lock.OpUpdate {
		return fmt.Errorf("key [%s] already exists", key)
	}

	return nil
}

func (i *IdempotentFilter) CheckRequest(r *http.Request) error {
	key := r.Header.Get(IdempotentHeaderKey)
	if key == "" {
		return errors.New("idempotency key not set")
	}

	return i.checkKey(key)
}
