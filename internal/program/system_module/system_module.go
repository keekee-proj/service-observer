package system_module

import (
	"bitbucket.org/keekee-proj/service-observer/internal/metric"
	"bitbucket.org/keekee-proj/service-observer/internal/modules"
	"bitbucket.org/keekee-proj/service-observer/internal/queue"
	"context"
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"sync"
	"time"

	application "bitbucket.org/keekee-proj/service-observer/internal/app"
)

const ctxModuleNameKey = "sysModuleName"

func NewSystemModuleContext(parent context.Context, moduleName string) context.Context {
	return context.WithValue(parent, ctxModuleNameKey, moduleName)
}

func AsyncSystemModule(app *application.App, parentCtx context.Context) error {
	ctx, cnlFn := context.WithCancel(parentCtx)
	moduleName := ctx.Value(ctxModuleNameKey).(string)
	app.RegisterCancelFn(cnlFn)

	cfg := app.Config()
	m, err := modules.ModuleByName(moduleName)
	if err != nil {
		return err
	}

	q, err := app.QueueProvider().MakeQueue(
		cfg.Amqp.Exchange,
		m.RouteKey(cfg.Amqp),
		m.QueueName(cfg.Amqp),
		60,
	)
	if err != nil {
		return err
	}
	defer q.Close()

	ch := q.Consume(20)
	log.Printf("start consuming [%s]\n", moduleName)

	wg := &sync.WaitGroup{}
	makeSystemPull(
		10, // ToDo: move to cfg
		ch,
		m,
		wg,
		q,
		app,
	)
	<-ctx.Done()
	wg.Wait()

	return nil
}

func makeSystemPull(n int, chDelivery <-chan amqp.Delivery, module modules.Module,
	wg *sync.WaitGroup, amqpQ queue.Queue, app *application.App) {
	for i := 0; i < n; i++ {
		go asyncProcessSysMessages(
			chDelivery,
			module,
			wg,
			amqpQ,
			app,
		)
	}
}

func sysMetricIncr(m metric.Metric, moduleName, metricName string) {
	m.Incr(fmt.Sprintf("system_module.%s.%s", moduleName, metricName))
}

func asyncProcessSysMessages(chDelivery <-chan amqp.Delivery, module modules.Module, wg *sync.WaitGroup,
	amqpQ queue.Queue, app *application.App) {
	for msg := range chDelivery {
		wg.Add(1)
		sysMetricIncr(app.Metric(), module.Name(), "msg.incomming")
		moment := time.Now()
		processSysMessage(
			module,
			wg,
			&msg,
			amqpQ,
			app,
		)
		sysMetricIncr(app.Metric(), module.Name(), "msg.processed")
		app.Metric().Timing(fmt.Sprintf("system_module.%s.msg.procesTime", module.Name()), time.Since(moment))
	}
}

func processSysMessage(m modules.Module, wg *sync.WaitGroup, msg *amqp.Delivery, amqpQ queue.Queue, app *application.App) {
	defer wg.Done()
	if len(msg.Body) == 0 {
		return
	}

	err := m.Exec(app, msg.Body)
	if err == nil {
		msg.Ack(false)
		return
	}
	log.Printf("failed executin module [%s]: %s", m.Name(), err)
	err = amqpQ.Defer(msg)
	if err == nil {
		sysMetricIncr(app.Metric(), m.Name(), "msg.defer")
		return
	}

	if err == queue.ErrMaxAttemptOut {
		sysMetricIncr(app.Metric(), m.Name(), "msg.lostMaxAttempt")
		log.Printf("message lost in module [%s] after all attempts", m.Name())
	} else {
		sysMetricIncr(app.Metric(), m.Name(), "msg.lost")
		log.Printf("message lost in module [%s]", m.Name())
	}
}
