##Examples
This instruction intended only for local tests. 

**For executing example you should have `docker`, `docker-compose`, `watch`, `jq`**

**Step 1.** Build docker images
```
make build.test
```

If you execute `docker images`, then you will see next images:
```
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
service-observer    testServer          000aaabbb111        16 seconds ago      11MB
service-observer    latest              000aaabbb112        18 seconds ago      48.7MB
service-observer    build               000aaabbb113        21 seconds ago      552MB
service-observer    base                000aaabbb114        57 seconds ago      480MB
```    
**Step 2.** Start services via docker-compose
```
make up
```

Check services status by command:
```
make ps
```
All states of containers (excluded 'init') should be `Up`
    
**Step 3.** Execute
```
./examples/fill_by_test_data.sh
```
This script will create main entities

**Step 4.** Execute
```
./examples/create_events.sh
```

This script will create 3 events.
If you call `make log testServ`, you will see next log:
```
testServ_1            | 2100/07/31 23:59:59 url: /
testServ_1            | 2100/07/31 23:59:59 headers:
testServ_1            | 2100/07/31 23:59:59 [User-Agent]: [Go-http-client/1.1]
testServ_1            | 2100/07/31 23:59:59 [Content-Length]: [18]
testServ_1            | 2100/07/31 23:59:59 [Accept-Encoding]: [gzip]
testServ_1            | 2100/07/31 23:59:59 body:
testServ_1            | ID: "0", PARAM1: a
testServ_1            | 2100/07/31 23:59:59 url: /
testServ_1            | 2100/07/31 23:59:59 headers:
testServ_1            | 2100/07/31 23:59:59 [Content-Length]: [18]
testServ_1            | 2100/07/31 23:59:59 [Accept-Encoding]: [gzip]
testServ_1            | 2100/07/31 23:59:59 [User-Agent]: [Go-http-client/1.1]
testServ_1            | 2100/07/31 23:59:59 body:
testServ_1            | ID: "1", PARAM1: b
testServ_1            | 2100/07/31 23:59:59 url: /
testServ_1            | 2100/07/31 23:59:59 headers:
testServ_1            | 2100/07/31 23:59:59 [User-Agent]: [Go-http-client/1.1]
testServ_1            | 2100/07/31 23:59:59 [Content-Length]: [19]
testServ_1            | 2100/07/31 23:59:59 [Accept-Encoding]: [gzip]
testServ_1            | 2100/07/31 23:59:59 body:
testServ_1            | ID: "x2", PARAM1: c
```
    
You can see in body some data like: `ID: "x2", PARAM1: c`, because we setted stage `stage_tpl_test` with template:
```
ID: "{{ .id }}", PARAM1: {{ .param1 }}
```

and throwed event with data:
```json
{
 "id": "x2",
 "param1": "c"
}
```

**Step 5.** Execute
```
./examples/clean.sh
```
to cleanup testing data

**Step 6.** Execute
```
make down
```
to stop test environment.
And
```
make cleanAll
```
for remove docker images