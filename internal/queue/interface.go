package queue

import (
	"fmt"
	"github.com/streadway/amqp"
)

var ErrMaxAttemptOut = fmt.Errorf("max attempt")
var MaxAttemptValue = int16(10)

type Queue interface {
	Close() error
	Defer(*amqp.Delivery) error
	Consume(prefetchCount int) <-chan amqp.Delivery
}

type Channel interface {
	Close() error
	PublishTo(body []byte, exchange, routeKey string, attempt int16) error
}
type Provider interface {
	MakeChannel() (Channel, error)
	MakeQueue(exchange string, routeKey string, queueName string, dlx int) (Queue, error)
}
