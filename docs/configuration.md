##Configuration
Example of configuration file:
```yaml
#etc/conf.yaml
app:
  listen_address: :8883             # Application port (don't change on using docker image)
  authKey: J0h12oeuoII**yiu12       # Authorization key, should be setted in the headers of the http-request
  duplicateFilter: md5              # Prevents duplicates of events (use strategy md5 or idempotency-key) 
  creating_events_type: async       # method of creating event for each subscriber (default sync)
db:
  dsn: 192.168.99.100:32019         # Database dsn (MongoDB)
  name: service_observer            # Database name
rabbit:
  dsn: amqp://guest:guest@192.168.99.100:30425/ # RabbitMQ dsn
  exchange: serv_observer                       # params for using rabbitMQ
  route_key: serv_observer
  queue_name: serv_observer
cache:
  ttl: 30s                          # timeout for in-memory cache for models
metric:                             # Part for metrics configuration (used statsd client)
  udp: metric:8125                  # statsd address
  prefix: esb.                      # metric prifix
heaper_cluster:                     # configuration fo heaper cluster 
  address: heaper-server:8119       # address of server module 
  strategy: subject                 # strategy of spreading jobs, may be entity
```

If you use docker compose for testing this application - you can use (and modify) next config file `./etc/compose.yaml`.

###Parameter app.creating_events_type
Set this parameter to async if you have a lot of subscribers and you don't need 
to save the correct order of events.