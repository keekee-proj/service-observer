package controllers

import (
	"bitbucket.org/keekee-proj/service-observer/internal/app"
	"bitbucket.org/keekee-proj/service-observer/internal/model/entity"
	"context"
	"errors"
	"io/ioutil"
	"net/http"
)

func UpdateEntity(ctx context.Context, response http.ResponseWriter, request *http.Request) error {
	eType := ctx.Value("entity").(string)
	data, err := ioutil.ReadAll(request.Body)
	if err != nil {
		return err
	}
	request.Body.Close()
	e, err := entity.ReadJson(data)
	if err != nil {
		return err
	}
	e.Type = eType
	repo := app.MustFromContext(ctx).EntityRepository()
	if err = repo.ValidateAdd(e); err != nil {
		return err
	}
	if err = repo.Update(eType, e); err != nil {
		return err
	}
	response.Write(success)
	return nil
}

func Entity(ctx context.Context, response http.ResponseWriter, _ *http.Request) error {
	eName := ctx.Value("entity").(string)

	e, err := app.MustFromContext(ctx).EntityRepository().OneCached(eName)
	if err != nil {
		return err
	}
	return WriteResponse(response, entity.ToHttpStruct(e))
}

func Entities(ctx context.Context, response http.ResponseWriter, request *http.Request) error {
	if err := request.ParseForm(); err != nil {
		return err
	}

	q, limit, offset := MakeHttpFilterQuery(request.Form, entity.FilterQuery)
	e, err := app.MustFromContext(ctx).EntityRepository().Find(q, limit, offset)
	if err != nil {
		return err
	}

	return WriteResponse(response, entity.ListToHttpStruct(e))
}

func AddEntity(ctx context.Context, response http.ResponseWriter, request *http.Request) error {
	data, err := ioutil.ReadAll(request.Body)
	if err != nil {
		return err
	}
	request.Body.Close()

	if len(data) == 0 {
		return errors.New("request empty")
	}
	repo := app.MustFromContext(ctx).EntityRepository()
	e, err := entity.ReadJson(data)
	if err != nil {
		return err
	}
	if err = repo.ValidateAdd(e); err != nil {
		return err
	}
	if err = repo.Add(e); err != nil {
		return err
	}
	response.Write(success)
	return nil
}

func DeleteEntity(ctx context.Context, response http.ResponseWriter, _ *http.Request) error {
	eName := ctx.Value("entity").(string)
	err := app.MustFromContext(ctx).EntityRepository().Delete(eName)
	if err != nil {
		return err
	}
	response.Write(success)
	return nil
}
