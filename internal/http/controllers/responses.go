package controllers

import (
	"encoding/json"
	"net/http"
)

var success = []byte("{\"result\": \"ok\"}")

func WriteResponse(response http.ResponseWriter, result interface{}) error {
	rowResult, err := json.Marshal(&struct {
		R interface{} `json:"result"`
	}{result})
	if err != nil {
		return err
	}
	_, err = response.Write(rowResult)
	return err
}
