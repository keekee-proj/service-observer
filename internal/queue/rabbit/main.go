package rabbit

import (
	i "bitbucket.org/keekee-proj/service-observer/internal/queue"
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"time"
)

type Channel struct {
	*amqp.Channel
	amqp *rabbitConnection
}

func MakeChannel(dsn string, keepAlive bool) (i.Channel, error) {
	rabbit, err := makeConnection(dsn, keepAlive)
	if err != nil {
		return nil, fmt.Errorf("amqp error: [%s]", err)
	}

	ch, err := rabbit.amqp.Channel()
	if err != nil {
		return nil, fmt.Errorf("amqp error: [%s]", err)
	}
	log.Println("created chanel")

	channel := &Channel{
		ch,
		rabbit,
	}
	recCh := make(chan bool)
	rabbit.NotifyReconnect(recCh)
	go channelReconnect(channel, recCh)

	return channel, nil
}

func channelReconnect(channel *Channel, ch chan bool) {
	<-ch
	var (
		newCan i.Channel
		err    error
	)
	for {
		time.Sleep(time.Second)
		newCan, err = MakeChannel(channel.amqp.dsn, true)
		if err == nil {
			channel.Channel = newCan.(*Channel).Channel
			break
		}
	}
}

func (ch *Channel) PublishTo(body []byte, exchange, routeKey string, attempt int16) error {
	msg := amqp.Publishing{
		ContentType:  "application/json",
		DeliveryMode: amqp.Persistent,
		Body:         body,
		Headers: map[string]interface{}{
			"attempt": attempt,
		},
	}
	return ch.Publish(
		exchange,
		routeKey,
		false,
		false,
		msg,
	)
}

func (ch *Channel) Close() error {
	err := ch.Channel.Close()
	if err != nil {
		log.Printf("error on closing channel [%s]", err)
		return err
	}
	log.Println("channel successful closed")

	return ch.amqp.Close()
}
