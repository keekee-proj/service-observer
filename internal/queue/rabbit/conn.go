package rabbit

import (
	"github.com/streadway/amqp"
	"log"
	"sync"
	"time"
)

var rabbitCon *rabbitConnection
var lock = sync.Mutex{}

type rabbitConnection struct {
	*sync.RWMutex
	amqp        *amqp.Connection
	dsn         string
	keepAlive   bool
	notifyRecon []chan bool
}

func makeConnection(dsn string, keepAlive bool) (*rabbitConnection, error) {
	lock.Lock()
	defer lock.Unlock()

	if rabbitCon != nil {
		log.Println("connection returned from cache")
		return rabbitCon, nil
	}

	con, err := amqp.Dial(dsn)
	if err != nil {
		return nil, err
	}
	rabbitCon = &rabbitConnection{
		&sync.RWMutex{},
		con,
		dsn,
		keepAlive,
		make([]chan bool, 0),
	}

	if keepAlive {
		go keepAliveRabbit(rabbitCon)
	}

	return rabbitCon, nil
}

func (q *rabbitConnection) Close() error {
	q.keepAlive = false
	return q.amqp.Close()
}

func keepAliveRabbit(amCon *rabbitConnection) {
	for {
		ch := make(chan *amqp.Error)
		amCon.amqp.NotifyClose(ch)
		err := <-ch

		if !amCon.keepAlive {
			break
		}

		if err != nil {
			log.Printf("amqp error: [%s, code: %d]", err.Reason, err.Code)
		}
		amCon.Lock()

		if !amCon.keepAlive {
			log.Fatal("amqp connection closed")
		}
		newCon, e := amqp.Dial(amCon.dsn)
		if e != nil {
			tik := time.NewTicker(10 * time.Millisecond)
			for range tik.C {
				newCon, e = amqp.Dial(amCon.dsn)
				if e != nil {
					continue
				}
				break
			}
			tik.Stop()
		}
		amCon.amqp = newCon
		amCon.Unlock()

		for _, recCh := range amCon.notifyRecon {
			go func(ch chan bool) {
				ch <- true
			}(recCh)
		}
		log.Printf("amqp successful reconnect")
	}
	log.Printf("finish watching connection")
}

func (q *rabbitConnection) NotifyReconnect(ch chan bool) {
	q.notifyRecon = append(q.notifyRecon, ch)
}
